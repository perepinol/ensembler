package com.gitlab.perepinol.ensembler.testing;

import com.gitlab.perepinol.ensembler.db.DatabaseManagementService;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;

@IntegrationTestSuite
public class IntegrationTests {
    @Autowired
    private DatabaseManagementService databaseManagementService;

    @BeforeEach
    void setup() {
        databaseManagementService.wipe();
    }
}
