package com.gitlab.perepinol.ensembler.testing;


import com.gitlab.perepinol.ensembler.EnsemblerApplication;
import org.springframework.boot.test.context.SpringBootTest;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@SpringBootTest(classes = EnsemblerApplication.class, properties = {
        "spring.profiles.active=integration-tests"
})
public @interface IntegrationTestSuite {

}
