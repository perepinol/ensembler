package com.gitlab.perepinol.ensembler.musician;

import com.gitlab.perepinol.ensembler.testing.IntegrationTestSuite;
import com.gitlab.perepinol.ensembler.testing.IntegrationTests;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MusicianRepositoryIntegrationTests extends IntegrationTests {
    @Autowired
    private MusicianRepository musicianRepository;

    // e2e tests already take care of database reads and writes.
    // This acts as a fail-early check for DBMS compatibility.
    @Test
    void test() {
        List<Musician> musicians = musicianRepository.findAll();
        assertEquals(0, musicians.size());
    }
}
