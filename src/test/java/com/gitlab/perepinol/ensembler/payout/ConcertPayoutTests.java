package com.gitlab.perepinol.ensembler.payout;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ConcertPayoutTests {
    @Test
    public void payoutIsZeroWhenNotAttended() {
        ConcertPayout payout = ConcertPayout.builder()
                .didAttend(false)
                .pricePerConcert(5.5)
                .build();

        assertEquals(0, payout.getTotal());
    }

    @Test
    public void payoutIsConcertPriceWhenAttended() {
        ConcertPayout payout = ConcertPayout.builder()
                .didAttend(true)
                .pricePerConcert(5.5)
                .build();

        assertEquals(5.5, payout.getTotal());
    }
}
