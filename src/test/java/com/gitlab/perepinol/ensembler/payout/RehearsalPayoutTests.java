package com.gitlab.perepinol.ensembler.payout;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class RehearsalPayoutTests {
    @Test
    public void payoutIsZeroWhenNoHoursAttended() {
        RehearsalPayout payout = RehearsalPayout.builder()
                .durationSeconds(0)
                .pricePerHour(3.5)
                .build();

        assertEquals(0, payout.getTotal());
    }

    @Test
    public void rehearsalPayoutIsPricePerHourPerDurationWhenDurationIsLessThanAnHour() {
        RehearsalPayout payout = RehearsalPayout.builder()
                .durationSeconds(120)
                .pricePerHour(3.5)
                .build();

        assertEquals(0.116, payout.getTotal(), 0.001);
    }

    @Test
    public void rehearsalPayoutIsPricePerHourPerDurationWhenDurationIsMoreThanAnHour() {
        RehearsalPayout payout = RehearsalPayout.builder()
                .durationSeconds(7200)
                .pricePerHour(3.5)
                .build();

        assertEquals(7, payout.getTotal());
    }
}
