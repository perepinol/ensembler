package com.gitlab.perepinol.ensembler.payout;

import com.gitlab.perepinol.ensembler.concert.Concert;
import com.gitlab.perepinol.ensembler.historicalValues.HistoricalValue;
import com.gitlab.perepinol.ensembler.historicalValues.History;
import com.gitlab.perepinol.ensembler.location.Location;
import com.gitlab.perepinol.ensembler.musician.Musician;
import com.gitlab.perepinol.ensembler.project.Project;
import com.gitlab.perepinol.ensembler.rehearsal.Rehearsal;
import com.gitlab.perepinol.ensembler.rehearsal.RehearsalAttendance;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Predicate;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ProjectPayoutCalculatorTests {
    History rehearsalHistory = new History(List.of(
            HistoricalValue.builder()
                    .since(LocalDate.of(2024, 1, 1))
                    .value(1)
                    .build(),
            HistoricalValue.builder()
                    .since(LocalDate.of(2024, 3, 1))
                    .value(3)
                    .build()
    ));
    History concertHistory = new History(List.of(
            HistoricalValue.builder()
                    .since(LocalDate.of(2024, 1, 1))
                    .value(1)
                    .build(),
            HistoricalValue.builder()
                    .since(LocalDate.of(2024, 2, 1))
                    .value(0.5)
                    .build()
    ));
    History mileageHistory = new History(List.of(
            HistoricalValue.builder()
                    .since(LocalDate.of(2024, 1, 1))
                    .value(1)
                    .build(),
            HistoricalValue.builder()
                    .since(LocalDate.of(2024, 1, 15))
                    .value(1.5)
                    .build()
    ));

    Function<Project, ProjectPayout> calculate = (project) -> PayoutCalculator.calculate(project,
            rehearsalHistory,
            concertHistory,
            mileageHistory
    );

    @Test
    public void returnsNoPayoutsWhenProjectIsEmpty() {
        Project project = Project.builder()
                .build();

        ProjectPayout projectPayout = calculate.apply(project);

        assertEquals(0, projectPayout.getPayouts().size());
        assertEquals(0, projectPayout.getPayouts().size());
    }

    @Test
    public void returnsZeroedPayoutsWhenNoRehearsalsOrConcerts() {
        Project project = Project.builder()
                .participants(Set.of(
                        Musician.builder()
                                .id(1L)
                                .build(),
                        Musician.builder()
                                .id(2L)
                                .build()
                ))
                .build();

        ProjectPayout projectPayout = calculate.apply(project);

        assertEquals(2, projectPayout.getPayouts().size());

        assertEquals(0, projectPayout.getPayouts().get(1L).getRehearsals().getPayouts().size());
        assertEquals(0, projectPayout.getPayouts().get(1L).getConcerts().getPayouts().size());

        assertEquals(0, projectPayout.getPayouts().get(2L).getRehearsals().getPayouts().size());
        assertEquals(0, projectPayout.getPayouts().get(2L).getConcerts().getPayouts().size());
    }

    @Test
    public void returnsCorrectPayouts() {
        GettableSet<Musician> participants = GettableSet.of(
                Musician.builder()
                        .id(1L)
                        .location(Location.builder().distance(150).build())
                        .build(),
                Musician.builder()
                        .id(2L)
                        .location(Location.builder().distance(10).build())
                        .build(),
                Musician.builder()
                        .id(3L)
                        .location(Location.builder().distance(0).build())
                        .build()
        );
        Function<Long, Musician> participantById = (id) -> participants.get((musician) -> musician.getId().equals(id));
        Set<Rehearsal> rehearsals = Set.of(
                Rehearsal.builder()
                        .date(LocalDate.of(2024, 1, 1))
                        .attendances(Set.of(
                                RehearsalAttendance.builder()
                                        .musician(participantById.apply(1L))
                                        .durationSeconds(3600)
                                        .build(),
                                RehearsalAttendance.builder()
                                        .musician(participantById.apply(2L))
                                        .durationSeconds(1800)
                                        .build(),
                                RehearsalAttendance.builder()
                                        .musician(participantById.apply(3L))
                                        .durationSeconds(7200)
                                        .build()
                        ))
                        .build(),
                Rehearsal.builder()
                        .date(LocalDate.of(2024, 2, 5))
                        .attendances(Set.of(
                                RehearsalAttendance.builder()
                                        .musician(participantById.apply(2L))
                                        .durationSeconds(3600)
                                        .build()
                        ))
                        .build(),
                Rehearsal.builder()
                        .date(LocalDate.of(2024, 3, 1))
                        .attendances(Set.of(
                                RehearsalAttendance.builder()
                                        .musician(participantById.apply(1L))
                                        .durationSeconds(5400)
                                        .build(),
                                RehearsalAttendance.builder()
                                        .musician(participantById.apply(2L))
                                        .durationSeconds(3600)
                                        .build(),
                                RehearsalAttendance.builder()
                                        .musician(participantById.apply(3L))
                                        .durationSeconds(3600)
                                        .build()
                        ))
                        .build()
        );
        Set<Concert> concerts = Set.of(
                Concert.builder()
                        .date(LocalDate.of(2024, 1, 31))
                        .attendances(Set.of(
                                participantById.apply(1L),
                                participantById.apply(3L)
                        ))
                        .build(),
                Concert.builder()
                        .date(LocalDate.of(2024, 2, 1))
                        .attendances(Set.of(
                                participantById.apply(1L),
                                participantById.apply(2L)
                        ))
                        .build()
        );
        Project project = Project.builder()
                .participants(participants)
                .rehearsals(rehearsals)
                .concerts(concerts)
                .build();

        ProjectPayout projectPayout = calculate.apply(project);

        // ID 1
        assertEquals(3, projectPayout.getPayouts().get(1L).getRehearsals().getPayouts().size());
        assertEquals(1, projectPayout.getPayouts().get(1L).getRehearsals().getPayouts().get(0).getTotal());
        assertEquals(0, projectPayout.getPayouts().get(1L).getRehearsals().getPayouts().get(1).getTotal());
        assertEquals(4.5, projectPayout.getPayouts().get(1L).getRehearsals().getPayouts().get(2).getTotal());
        assertEquals(5.5, projectPayout.getPayouts().get(1L).getRehearsals().getTotal());

        assertEquals(3, projectPayout.getPayouts().get(1L).getMileages().getPayouts().size());
        assertEquals(150, projectPayout.getPayouts().get(1L).getMileages().getPayouts().get(0).getTotal());
        assertEquals(0, projectPayout.getPayouts().get(1L).getMileages().getPayouts().get(1).getTotal());
        assertEquals(225, projectPayout.getPayouts().get(1L).getMileages().getPayouts().get(2).getTotal());
        assertEquals(375, projectPayout.getPayouts().get(1L).getMileages().getTotal());

        assertEquals(2, projectPayout.getPayouts().get(1L).getConcerts().getPayouts().size());
        assertEquals(1, projectPayout.getPayouts().get(1L).getConcerts().getPayouts().get(0).getTotal());
        assertEquals(0.5, projectPayout.getPayouts().get(1L).getConcerts().getPayouts().get(1).getTotal());
        assertEquals(1.5, projectPayout.getPayouts().get(1L).getConcerts().getTotal());

        assertEquals(382, projectPayout.getPayouts().get(1L).getTotal());

        // ID 2
        assertEquals(3, projectPayout.getPayouts().get(2L).getRehearsals().getPayouts().size());
        assertEquals(0.5, projectPayout.getPayouts().get(2L).getRehearsals().getPayouts().get(0).getTotal());
        assertEquals(1, projectPayout.getPayouts().get(2L).getRehearsals().getPayouts().get(1).getTotal());
        assertEquals(3, projectPayout.getPayouts().get(2L).getRehearsals().getPayouts().get(2).getTotal());
        assertEquals(4.5, projectPayout.getPayouts().get(2L).getRehearsals().getTotal());

        assertEquals(3, projectPayout.getPayouts().get(2L).getMileages().getPayouts().size());
        assertEquals(10, projectPayout.getPayouts().get(2L).getMileages().getPayouts().get(0).getTotal());
        assertEquals(15, projectPayout.getPayouts().get(2L).getMileages().getPayouts().get(1).getTotal());
        assertEquals(15, projectPayout.getPayouts().get(2L).getMileages().getPayouts().get(2).getTotal());
        assertEquals(40, projectPayout.getPayouts().get(2L).getMileages().getTotal());

        assertEquals(2, projectPayout.getPayouts().get(2L).getConcerts().getPayouts().size());
        assertEquals(0, projectPayout.getPayouts().get(2L).getConcerts().getPayouts().get(0).getTotal());
        assertEquals(0.5, projectPayout.getPayouts().get(2L).getConcerts().getPayouts().get(1).getTotal());
        assertEquals(0.5, projectPayout.getPayouts().get(2L).getConcerts().getTotal());

        assertEquals(45, projectPayout.getPayouts().get(2L).getTotal());

        // ID 3
        assertEquals(3, projectPayout.getPayouts().get(3L).getRehearsals().getPayouts().size());
        assertEquals(2, projectPayout.getPayouts().get(3L).getRehearsals().getPayouts().get(0).getTotal());
        assertEquals(0, projectPayout.getPayouts().get(3L).getRehearsals().getPayouts().get(1).getTotal());
        assertEquals(3, projectPayout.getPayouts().get(3L).getRehearsals().getPayouts().get(2).getTotal());
        assertEquals(5, projectPayout.getPayouts().get(3L).getRehearsals().getTotal());

        assertEquals(3, projectPayout.getPayouts().get(3L).getMileages().getPayouts().size());
        assertEquals(0, projectPayout.getPayouts().get(3L).getMileages().getPayouts().get(0).getTotal());
        assertEquals(0, projectPayout.getPayouts().get(3L).getMileages().getPayouts().get(1).getTotal());
        assertEquals(0, projectPayout.getPayouts().get(3L).getMileages().getPayouts().get(2).getTotal());
        assertEquals(0, projectPayout.getPayouts().get(3L).getMileages().getTotal());

        assertEquals(2, projectPayout.getPayouts().get(3L).getConcerts().getPayouts().size());
        assertEquals(1, projectPayout.getPayouts().get(3L).getConcerts().getPayouts().get(0).getTotal());
        assertEquals(0, projectPayout.getPayouts().get(3L).getConcerts().getPayouts().get(1).getTotal());
        assertEquals(1, projectPayout.getPayouts().get(3L).getConcerts().getTotal());

        assertEquals(6, projectPayout.getPayouts().get(3L).getTotal());
    }

    private static class GettableSet<T> extends HashSet<T> {
        public GettableSet(Collection<? extends T> c) {
            super(c);
        }

        public T get(Predicate<T> predicate) {
            return this.stream().filter(predicate).findFirst().orElseThrow(AssertionError::new);
        }

        @SafeVarargs
        public static <T> GettableSet<T> of(T... elements) {
            return new GettableSet<>(Set.of(elements));
        }
    }
}
