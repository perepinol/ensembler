package com.gitlab.perepinol.ensembler.payout;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MileagePayoutTests {
    @Test
    public void payoutIsZeroWhenNotAttended() {
        MileagePayout payout = MileagePayout.builder()
                .distance(200)
                .pricePerKm(2.2)
                .attended(false)
                .build();

        assertEquals(0, payout.getTotal());
    }

    @Test
    public void travelPayoutIsDistancePerPrice() {
        MileagePayout payout = MileagePayout.builder()
                .attended(true)
                .distance(200)
                .pricePerKm(2.2)
                .build();

        assertEquals(440, payout.getTotal(), 0.001);
    }
}
