<?xml version="1.0" encoding="UTF-8"?>
<databaseChangeLog
        xmlns="http://www.liquibase.org/xml/ns/dbchangelog"
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xmlns:ext="http://www.liquibase.org/xml/ns/dbchangelog-ext"
        xmlns:pro="http://www.liquibase.org/xml/ns/pro"
        xsi:schemaLocation="http://www.liquibase.org/xml/ns/dbchangelog
        http://www.liquibase.org/xml/ns/dbchangelog/dbchangelog-latest.xsd
        http://www.liquibase.org/xml/ns/dbchangelog-ext http://www.liquibase.org/xml/ns/dbchangelog/dbchangelog-ext.xsd
        http://www.liquibase.org/xml/ns/pro http://www.liquibase.org/xml/ns/pro/liquibase-pro-latest.xsd">
    <changeSet id="1" author="Pere Piñol">
        <createTable tableName="location">
            <column name="id" type="bigint" autoIncrement="true">
                <constraints primaryKey="true" nullable="false"/>
            </column>
            <column name="name" type="varchar(255)">
                <constraints nullable="false" unique="true"/>
            </column>
            <column name="distance" type="int">
                <constraints nullable="false"/>
            </column>
        </createTable>
        <createTable tableName="musician">
            <column name="id" type="bigint" autoIncrement="true">
                <constraints primaryKey="true" unique="true" nullable="false"/>
            </column>
            <column name="given_name" type="varchar(255)">
                <constraints nullable="false"/>
            </column>
            <column name="family_name" type="varchar(255)">
                <constraints nullable="false"/>
            </column>
            <column name="instrument" type="varchar(255)">
                <constraints nullable="false"/>
            </column>
            <column name="location_id" type="bigint">
                <constraints
                        nullable="false"
                        foreignKeyName="fk_musician_location_id"
                        references="location(id)"
                />
            </column>
        </createTable>
        <createTable tableName="project">
            <column name="id" type="bigint" autoIncrement="true">
                <constraints primaryKey="true" unique="true" nullable="false"/>
            </column>
            <column name="name" type="varchar(255)">
                <constraints nullable="false"/>
            </column>
            <column name="season" type="int">
                <constraints nullable="false"/>
            </column>
            <column name="conductor" type="varchar(255)">
                <constraints nullable="true"/>
            </column>
        </createTable>
        <addUniqueConstraint tableName="project" columnNames="name,season"/>
        <createTable tableName="participation">
            <column name="project_id" type="bigint">
                <constraints
                        primaryKey="true"
                        nullable="false"
                        foreignKeyName="fk_participation_project_id"
                        references="project(id)"
                />
            </column>
            <column name="musician_id" type="bigint">
                <constraints
                        primaryKey="true"
                        nullable="false"
                        foreignKeyName="fk_participation_musician_id"
                        references="musician(id)"
                />
            </column>
        </createTable>
        <createTable tableName="rehearsal">
            <column name="id" type="bigint" autoIncrement="true">
                <constraints primaryKey="true" unique="true" nullable="false"/>
            </column>
            <column name="rehearsal_date" type="timestamp">
                <constraints nullable="false"/>
            </column>
            <column name="duration" type="int">
                <constraints nullable="false"/>
            </column>
            <column name="project_id" type="bigint">
                <constraints
                        nullable="false"
                        foreignKeyName="fk_rehearsal_project_id"
                        references="project(id)"
                />
            </column>
        </createTable>
        <createTable tableName="rehearsal_attendance">
            <column name="rehearsal_id" type="bigint">
                <constraints
                        primaryKey="true"
                        nullable="false"
                        foreignKeyName="fk_rehearsal_attendance_rehearsal_id"
                        references="rehearsal(id)"
                />
            </column>
            <column name="musician_id" type="bigint">
                <constraints
                        primaryKey="true"
                        nullable="false"
                        foreignKeyName="fk_rehearsal_attendance_musician_id"
                        references="musician(id)"
                />
            </column>
        </createTable>
    </changeSet>
    <changeSet id="2" author="Pere Piñol">
        <createTable tableName="concert">
            <column name="id" type="bigint" autoIncrement="true">
                <constraints primaryKey="true" unique="true" nullable="false"/>
            </column>
            <column name="concert_date" type="timestamp">
                <constraints nullable="false"/>
            </column>
            <column name="location" type="varchar(255)">
                <constraints nullable="false"/>
            </column>
            <column name="project_id" type="bigint">
                <constraints
                        nullable="false"
                        foreignKeyName="fk_concert_project_id"
                        references="project(id)"
                />
            </column>
        </createTable>
        <createTable tableName="concert_attendance">
            <column name="concert_id" type="bigint">
                <constraints
                        primaryKey="true"
                        nullable="false"
                        foreignKeyName="fk_concert_attendance_concert_id"
                        references="concert(id)"
                />
            </column>
            <column name="musician_id" type="bigint">
                <constraints
                        primaryKey="true"
                        nullable="false"
                        foreignKeyName="fk_concert_attendance_musician_id"
                        references="musician(id)"
                />
            </column>
        </createTable>
    </changeSet>
    <changeSet id="3" author="Pere Piñol">
        <createTable tableName="historical_value">
            <column name="type" type="varchar(50)">
                <constraints
                        primaryKey="true"
                        nullable="false"
                />
            </column>
            <column name="since" type="timestamp">
                <constraints
                        primaryKey="true"
                        nullable="false"
                />
            </column>
            <column name="value" type="float">
                <constraints
                        nullable="false"
                />
            </column>
        </createTable>
    </changeSet>
    <changeSet id="4" author="Pere Piñol">
        <createTable tableName="instrument">
            <column name="id" type="bigint">
                <constraints
                        primaryKey="true"
                        nullable="false"
                />
            </column>
            <column name="name" type="varchar(255)">
                <constraints
                        nullable="false"
                        unique="true"
                />
            </column>
        </createTable>
        <addColumn tableName="musician">
            <column name="instrument_id" type="bigint" defaultValue="-1">
                <constraints
                        nullable="false"
                />
            </column>
        </addColumn>
        <sql>
            INSERT INTO instrument (id, name) VALUES
            (100, 'soprano'),
            (105, 'mezzo-soprano'),
            (110, 'contralto'),
            (115, 'countertenor'),
            (120, 'tenor'),
            (125, 'baritone'),
            (130, 'bass');
        </sql>
    </changeSet>
    <!--This changeset will fail at first if there are musicians with instruments that do not match the available ones.
    Add other instruments as necessary first.-->
    <changeSet id="5" author="Pere Piñol">
        <sql>
            UPDATE musician m SET instrument_id = (SELECT id FROM instrument i WHERE LOWER(i.name) =
            LOWER(m.instrument))
        </sql>
        <dropDefaultValue tableName="musician" columnName="instrument_id"/>
        <addForeignKeyConstraint
                baseTableName="musician"
                baseColumnNames="instrument_id"
                constraintName="fk_musician_instrument_id"
                referencedTableName="instrument"
                referencedColumnNames="id"
        />
        <dropColumn tableName="musician" columnName="instrument"/>
    </changeSet>
    <changeSet id="6" author="Pere Piñol">
        <createTable tableName="rehearsal_attendance_2">
            <column name="rehearsal_id" type="bigint">
                <constraints
                        primaryKey="true"
                        nullable="false"
                />
            </column>
            <column name="musician_id" type="bigint">
                <constraints
                        primaryKey="true"
                        nullable="false"
                />
            </column>
            <column name="duration" type="int">
                <constraints nullable="false"/>
            </column>
        </createTable>
        <sql>
            INSERT INTO rehearsal_attendance_2 (rehearsal_id, musician_id, duration)
            SELECT ra.rehearsal_id, ra.musician_id, r.duration
            FROM rehearsal_attendance ra
            LEFT JOIN rehearsal r ON r.id = ra.rehearsal_id;
        </sql>
        <dropTable tableName="rehearsal_attendance"/>
        <createTable tableName="rehearsal_attendance">
            <column name="id" type="bigint" autoIncrement="true">
                <constraints
                        primaryKey="true"
                        nullable="false"
                        unique="true"
                />
            </column>
            <column name="rehearsal_id" type="bigint">
                <constraints
                        nullable="false"
                        foreignKeyName="fk_rehearsal_attendance_rehearsal_id"
                        references="rehearsal(id)"
                />
            </column>
            <column name="musician_id" type="bigint">
                <constraints
                        nullable="false"
                        foreignKeyName="fk_rehearsal_attendance_musician_id"
                        references="musician(id)"
                />
            </column>
            <column name="duration" type="int">
                <constraints nullable="false"/>
            </column>
        </createTable>
        <sql>
            INSERT INTO rehearsal_attendance (rehearsal_id, musician_id, duration)
            SELECT rehearsal_id, musician_id, duration
            FROM rehearsal_attendance_2;
        </sql>
        <dropTable tableName="rehearsal_attendance_2"/>
    </changeSet>
    <changeSet id="7" author="Pere Piñol">
        <renameColumn
            tableName="historical_value"
            oldColumnName="value"
            newColumnName="hist_value"
            columnDataType="float"
        />
    </changeSet>
    <changeSet id="8" author="Pere Piñol">
        <addColumn tableName="musician">
            <!-- Could be int, but the numbers will typically be big, and they just represent an external ID -->
            <column name="account_number" type="varchar(7)">
                <!-- Eventually this should be not nullable -->
                <constraints nullable="true" unique="true" />
            </column>
        </addColumn>
    </changeSet>
</databaseChangeLog>
