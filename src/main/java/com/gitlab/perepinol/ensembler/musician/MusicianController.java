package com.gitlab.perepinol.ensembler.musician;

import com.gitlab.perepinol.ensembler.boundary.NotFoundException;
import com.gitlab.perepinol.ensembler.location.Location;
import com.gitlab.perepinol.ensembler.location.LocationRepository;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/api/musician")
class MusicianController {
    private final MusicianRepository musicianRepository;
    private final InstrumentRepository instrumentRepository;
    private final LocationRepository locationRepository;

    public MusicianController(
            MusicianRepository musicianRepository,
            InstrumentRepository instrumentRepository,
            LocationRepository locationRepository
    ) {
        this.musicianRepository = musicianRepository;
        this.instrumentRepository = instrumentRepository;
        this.locationRepository = locationRepository;
    }

    @GetMapping
    public List<Musician> getMusicians() {
        return musicianRepository.findAll();
    }

    @GetMapping("/{id}")
    public Musician getMusician(@PathVariable("id") long id) {
        return musicianRepository.findById(id)
                .orElseThrow(NotFoundException::new);
    }

    @PostMapping
    public Musician createMusician(@RequestBody IncomingMusicianDto musician) {
        return musicianRepository.save(fromDto(musician));
    }

    @PutMapping("/{id}")
    public Musician updateMusician(@PathVariable("id") long id, @RequestBody IncomingMusicianDto musician) {
        if (musician.getId() != null && id != musician.getId()) {
            throw new IllegalArgumentException("Path and body IDs do not match");
        }

        return musicianRepository.save(fromDto(musician).toBuilder().id(id).build());
    }

    @DeleteMapping("/{id}")
    public Musician deleteMusician(@PathVariable("id") long id) {
        Musician musician = musicianRepository.findById(id)
                .orElseThrow(NotFoundException::new);
        musicianRepository.delete(musician);

        return musician;
    }

    @GetMapping("/instrument")
    public List<Instrument> getInstruments() {
        return instrumentRepository.findAll();
    }

    private Musician fromDto(IncomingMusicianDto musician) {
        Location location = locationRepository.findById(musician.getLocationId())
                .orElseThrow(() -> new IllegalArgumentException("Location not found"));
        Instrument instrument = instrumentRepository.findById(musician.getInstrumentId())
                .orElseThrow(() -> new IllegalArgumentException("Instrument not found"));

        return musician.toEntity(location, instrument);
    }
}
