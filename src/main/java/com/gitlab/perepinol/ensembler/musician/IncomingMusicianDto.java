package com.gitlab.perepinol.ensembler.musician;

import com.gitlab.perepinol.ensembler.location.Location;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.extern.jackson.Jacksonized;

@Jacksonized
@Builder(access = AccessLevel.PACKAGE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class IncomingMusicianDto {
    @Getter(AccessLevel.PACKAGE)
    private final Long id;
    private final String givenName;
    private final String familyName;
    @Getter(AccessLevel.PACKAGE)
    private final Long instrumentId;
    @Getter(AccessLevel.PACKAGE)
    private final long locationId;
    private final String accountNumber;

    Musician toEntity(Location location, Instrument instrument) {
        if (!location.getId().equals(locationId)) {
            throw new IllegalArgumentException("Location IDs do not match");
        }

        if (!instrument.getId().equals(instrumentId)) {
            throw new IllegalArgumentException("Instrument IDs do not match");
        }

        return Musician.builder()
                .id(id)
                .givenName(givenName)
                .familyName(familyName)
                .instrument(instrument)
                .location(location)
                .accountNumber(accountNumber)
                .build();
    }
}
