package com.gitlab.perepinol.ensembler.musician;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.gitlab.perepinol.ensembler.location.Location;
import jakarta.annotation.Nullable;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Objects;

@Entity
@Getter
@Builder(toBuilder = true)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Setter(AccessLevel.PRIVATE)
public class Musician {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private Long id;

    @Column(nullable = false)
    private String givenName;

    @Column(nullable = false)
    private String familyName;

    @ManyToOne
    @JoinColumn(name = "instrument_id", nullable = false)
    private Instrument instrument;

    @ManyToOne
    @JoinColumn(name = "location_id", nullable = false)
    private Location location;

    @Column(nullable = true, unique = true)
    @Nullable
    private String accountNumber;

    @JsonIgnore
    public String getFullName() {
        return givenName + " " + familyName;
    }

    @Override
    public String toString() {
        return String.format("Musician{id=%d,name=%s,instrument=%s", id, getFullName(), instrument);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Musician musician = (Musician) o;
        return Objects.equals(id, musician.id)
                && Objects.equals(givenName, musician.givenName)
                && Objects.equals(familyName, musician.familyName)
                && Objects.equals(instrument, musician.instrument)
                && Objects.equals(location, musician.location)
                && Objects.equals(accountNumber, musician.accountNumber);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, givenName, familyName, instrument, location, accountNumber);
    }
}
