package com.gitlab.perepinol.ensembler.musician;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PRIVATE)
class Instrument {
    @Id
    @Column
    private Long id;
    @Column
    private String name;
}
