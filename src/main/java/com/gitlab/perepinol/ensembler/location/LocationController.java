package com.gitlab.perepinol.ensembler.location;

import com.gitlab.perepinol.ensembler.boundary.NotFoundException;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/location")
public class LocationController {
    private final LocationRepository locationRepository;

    public LocationController(LocationRepository locationRepository) {
        this.locationRepository = locationRepository;
    }

    @GetMapping
    public List<Location> getLocations() {
        return locationRepository.findAll();
    }

    @GetMapping("/{id}")
    public Location getLocationById(@PathVariable("id") long id) {
        return locationRepository.findById(id)
                .orElseThrow(NotFoundException::new);
    }

    @PostMapping
    public Location createLocation(@RequestBody Location location) {
        return locationRepository.save(location);
    }

    @PutMapping("/{id}")
    public Location updateLocation(@PathVariable("id") long id, @RequestBody Location location) {
        if (location.getId() != null && !location.getId().equals(id)) {
            throw new IllegalArgumentException("Path and body IDs do not match");
        }

        return locationRepository.save(location.toBuilder().id(id).build());
    }

    @DeleteMapping("/{id}")
    public Location deleteLocation(@PathVariable("id") long id) {
        Location location = locationRepository.findById(id).orElseThrow(NotFoundException::new);
        locationRepository.delete(location);

        return location;
    }
}
