package com.gitlab.perepinol.ensembler.concert;

import com.gitlab.perepinol.ensembler.boundary.NotFoundException;
import com.gitlab.perepinol.ensembler.project.Project;
import com.gitlab.perepinol.ensembler.project.ProjectRepository;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/concert")
public class ConcertController {
    private final ConcertRepository concertRepository;
    private final ProjectRepository projectRepository;

    public ConcertController(
            ConcertRepository concertRepository,
            ProjectRepository projectRepository
    ) {
        this.concertRepository = concertRepository;
        this.projectRepository = projectRepository;
    }

    @GetMapping
    List<Concert> getConcerts() {
        return concertRepository.findAll();
    }

    @GetMapping("/{id}")
    Concert getConcert(@PathVariable("id") long id) {
        return concertRepository.findById(id)
                .orElseThrow(NotFoundException::new);
    }

    @PostMapping
    Concert createConcert(@RequestBody IncomingConcertDto concertDto) {
        Project project = getProjectForConcert(concertDto);

        return concertRepository.save(concertDto.toEntity(project));
    }

    @PutMapping("/{id}")
    Concert updateConcert(@PathVariable("id") long id, @RequestBody IncomingConcertDto concertDto) {
        if (concertDto.getId() != null && concertDto.getId() != id) {
            throw new IllegalArgumentException("Path and body IDs do not match");
        }

        Concert concert = concertDto.toEntity(getProjectForConcert(concertDto))
                .toBuilder()
                .id(id)
                .build();

        return concertRepository.save(concert);
    }

    @DeleteMapping("/{id}")
    Concert deleteConcert(@PathVariable("id") long id) {
        Concert concert = concertRepository.findById(id).orElseThrow(NotFoundException::new);
        concertRepository.delete(concert);

        return concert;
    }

    private Project getProjectForConcert(IncomingConcertDto concertDto) {
        return projectRepository.findById(concertDto.getProjectId())
                .orElseThrow(NotFoundException::new);
    }
}
