package com.gitlab.perepinol.ensembler.concert;

import com.gitlab.perepinol.ensembler.project.Project;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.extern.jackson.Jacksonized;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@Jacksonized
@Getter
@Builder(access = AccessLevel.PACKAGE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class IncomingConcertDto {
    private final Long id;

    private final String date;

    private final String location;

    private final long projectId;

    private final List<Long> attendeeIds;

    public Concert toEntity(Project project) {
        if (project.getId() != projectId) {
            throw new IllegalArgumentException("Project IDs do not match");
        }

        if (attendeeIds.size() != attendeeIds.stream().distinct().count()) {
            throw new IllegalArgumentException("There are duplicated attendees");
        }

        return Concert.builder()
                .id(id)
                .date(LocalDate.parse(date))
                .location(location)
                .project(project)
                .attendances(this.attendeeIds.stream()
                        .map(attendeeId -> project.getParticipants().stream()
                                .filter(participant -> participant.getId().equals(attendeeId))
                                .findFirst()
                                .orElseThrow(IllegalArgumentException::new)
                        )
                        .collect(Collectors.toSet())
                )
                .build();
    }
}
