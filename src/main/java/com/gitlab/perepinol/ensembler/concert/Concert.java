package com.gitlab.perepinol.ensembler.concert;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.gitlab.perepinol.ensembler.musician.Musician;
import com.gitlab.perepinol.ensembler.project.Project;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.ManyToOne;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.Set;

@Entity
@Getter(AccessLevel.PUBLIC)
@Builder(access = AccessLevel.PUBLIC, toBuilder = true)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Concert {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private Long id;

    @Column(name = "concert_date", nullable = false)
    private LocalDate date;

    @Column(name = "location", nullable = false)
    private String location;

    @ManyToOne
    @JoinColumn(name = "project_id", referencedColumnName = "id", nullable = false)
    private Project project;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "concert_attendance",
            joinColumns = @JoinColumn(name = "concert_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "musician_id", referencedColumnName = "id")
    )
    @JsonProperty("attendees")
    private Set<Musician> attendances;
}
