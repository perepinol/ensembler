package com.gitlab.perepinol.ensembler.date;

import java.time.LocalDate;

public class DateProvider {
    private static final DateProvider INSTANCE = new DateProvider();

    private LocalDate fakeDate;

    private DateProvider() {

    }

    public static LocalDate get() {
        if (INSTANCE.fakeDate != null) {
            return INSTANCE.fakeDate;
        }

        return LocalDate.now();
    }

    public static void set(LocalDate date) {
        INSTANCE.fakeDate = date;
    }
}
