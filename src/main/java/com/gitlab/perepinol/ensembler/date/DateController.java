package com.gitlab.perepinol.ensembler.date;

import org.springframework.context.annotation.Profile;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.time.LocalDate;
import java.time.format.DateTimeParseException;

/**
 * Used in tests to set the system date.
 */
@Controller
@RequestMapping(value = "/api/date")
@Profile({ "test", "integration-tests" })
public class DateController {
    @PutMapping
    public ResponseEntity<Object> setDate(@RequestParam("date") String date) {
        try {
            DateProvider.set(LocalDate.parse(date)); // Maybe change format?
            return ResponseEntity.ok().build();
        } catch (DateTimeParseException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
