package com.gitlab.perepinol.ensembler.db;

import org.springframework.context.annotation.Profile;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "/api/database")
@Profile({ "test", "integration-tests" })
class DatabaseController {
    private final DatabaseManagementService databaseManagementService;

    public DatabaseController(DatabaseManagementService databaseManagementService) {
        this.databaseManagementService = databaseManagementService;
    }

    @GetMapping
    public ResponseEntity<String> get() {
        return ResponseEntity.ok().build();
    }

    @DeleteMapping
    public ResponseEntity<Object> wipe() {
        databaseManagementService.wipe();

        return ResponseEntity.ok().build();
    }
}
