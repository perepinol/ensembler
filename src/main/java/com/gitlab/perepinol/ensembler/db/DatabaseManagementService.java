package com.gitlab.perepinol.ensembler.db;

import org.aspectj.weaver.PersistenceSupport;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class DatabaseManagementService {
    private static final String CHANGELOG_PATTERN = "DATABASECHANGELOG.*";
    private static final List<String> EXCLUDED_TABLES = List.of(
            "instrument"
    );

    private final DataSource dataSource;

    public DatabaseManagementService(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public void wipe() {
        sortTableNamesByLeastDependants().stream()
                .filter(tableName -> EXCLUDED_TABLES.stream().noneMatch(table -> table.equalsIgnoreCase(tableName)))
                .map(tableName -> String.format("DELETE FROM %s", tableName))
                .forEach(sql -> {
                    try (
                            Connection connection = dataSource.getConnection();
                            PreparedStatement statement = connection.prepareStatement(sql);
                    ) {
                        statement.execute();
                    } catch (SQLException e) {
                        throw new RuntimeException(e);
                    }
                });
    }


    private List<String> sortTableNamesByLeastDependants() {
        Map<String, List<String>> dependantCount;

        try (Connection connection = dataSource.getConnection()) {
            dependantCount = new CaseInsensitiveMetadata(connection).getDependantsForTables(
                    "ensembler",
                    "public"
            );
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return new StringGraph(dependantCount).getNodesByDependencies();
    }

    private static class CaseInsensitiveMetadata {
        private final DatabaseMetaData metaData;

        private CaseInsensitiveMetadata(Connection connection) throws SQLException {
            this.metaData = connection.getMetaData();
        }

        // Rough, but since it's only for testing, problems will be hard to miss
        public Map<String, List<String>> getDependantsForTables(
                String catalog,
                String schema
        ) throws SQLException {
            return doOperationCaseInsensitive(
                    (c, s) -> {
                        Map<String, List<String>> dependantMap = new HashMap<>();
                        for (String table : getTableNamesCaseSensitive(c, s)) {
                            List<String> dependants = new LinkedList<>();
                            ResultSet resultSet = metaData.getExportedKeys(c, s, table);
                            while (resultSet.next()) {
                                dependants.add(resultSet.getString(7));
                            }
                            dependantMap.put(table, dependants);
                        }
                        return dependantMap;
                    },
                    (map1, map2) -> {
                        map2.forEach((table, dependants) -> {
                            if (!map1.containsKey(table.toLowerCase())) {
                                map1.put(table.toLowerCase(), dependants);
                            } else {
                                List<String> mergedDependants = Stream.concat(
                                        dependants.stream(),
                                        map1.get(table.toLowerCase()).stream()
                                ).toList();
                                map1.put(table, mergedDependants);
                            }
                        });

                        return map1;
                    },
                    catalog,
                    schema
            );
        }

        private Set<String> getTableNamesCaseSensitive(String catalog, String schema) throws SQLException {
            Set<String> tableNames = new HashSet<>();

            ResultSet resultSet = metaData.getTables(
                    catalog,
                    schema,
                    "%",
                    null
            );

            while (resultSet.next()) {
                String tableName = resultSet.getString(3);
                if (tableName.matches(CHANGELOG_PATTERN)) {
                    continue;
                }

                tableNames.add(tableName);
            }

            return tableNames;
        }

        private <T> T doOperationCaseInsensitive(
                ThrowingBiFunction<String, String, T, SQLException> operation,
                BiFunction<T, T, T> merger,
                String catalog,
                String schema
        ) throws SQLException {
            Map<String, String> combinations = new HashMap<>() {{
                put(catalog.toLowerCase(), schema.toLowerCase());
                put(catalog.toUpperCase(), schema.toUpperCase());
            }};

            T accumulatedResult = null;
            for (Map.Entry<String, String> entry : combinations.entrySet()) {
                T result = operation.apply(entry.getKey(), entry.getValue());
                accumulatedResult = accumulatedResult != null
                        ? merger.apply(accumulatedResult, result)
                        : result;
            }

            return accumulatedResult;
        }
    }

    private interface ThrowingBiFunction<T, U, V, ExceptionType extends Throwable> {
        V apply(T t, U u) throws ExceptionType;
    }
}
