package com.gitlab.perepinol.ensembler.db;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class StringGraph {
    private final Map<String, List<String>> dependantMap;

    public StringGraph(Map<String, List<String>> dependantMap) {
        this.dependantMap = dependantMap;
    }

    /**
     * Returns the node values in an order where a later node never depends on an earlier one.
     * @return A list of string node values.
     */
    public List<String> getNodesByDependencies() {
        List<String> result = new LinkedList<>();
        Map<String, List<String>> map = StringGraph.copyMap(dependantMap);
        while (!map.isEmpty()) {
            String noDependantsNode = map.entrySet().stream()
                    .filter(entry -> entry.getValue().size() == 0)
                    .findFirst()
                    .orElseThrow(() -> new UnsupportedOperationException("Graph is cyclical"))
                    .getKey();

            result.add(noDependantsNode);

            map.remove(noDependantsNode);
            map.forEach((nodeName, dependants) -> map.put(
                    nodeName,
                    dependants.stream()
                            .filter(node -> !node.equals(noDependantsNode))
                            .toList()
            ));
        }

        return result;
    }

    private static Map<String, List<String>> copyMap(Map<String, List<String>> map) {
        Map<String, List<String>> copy = new HashMap<>();
        map.forEach((key, value) -> copy.put(key, List.copyOf(value)));
        return copy;
    }
}
