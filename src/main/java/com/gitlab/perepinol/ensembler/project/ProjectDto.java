package com.gitlab.perepinol.ensembler.project;

import com.gitlab.perepinol.ensembler.musician.Musician;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.extern.jackson.Jacksonized;

import java.util.List;
import java.util.stream.Collectors;

@Jacksonized
@Getter(AccessLevel.PUBLIC)
@Builder(access = AccessLevel.PACKAGE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class ProjectDto {
    private final Long id;

    private final String name;

    private final int year;

    private final String conductor;

    private final List<Long> participantIds;

    public Project toEntity(List<Musician> musicians) {
        if (participantIds.size() != participantIds.stream().distinct().count()) {
            throw new IllegalArgumentException("There are duplicated participants");
        }

        return Project.builder()
                .id(id)
                .name(name)
                .year(year)
                .conductor(conductor)
                .participants(participantIds.stream()
                        .map(id -> musicians.stream()
                                .filter(musician -> musician.getId().equals(id))
                                .findFirst()
                                .orElseThrow(IllegalArgumentException::new)
                        )
                        .collect(Collectors.toSet())
                )
                .build();
    }

    public static ProjectDto fromEntity(Project project) {
        return ProjectDto.builder()
                .id(project.getId())
                .name(project.getName())
                .year(project.getYear())
                .conductor(project.getConductor())
                .participantIds(project.getParticipants()
                        .stream()
                        .map(Musician::getId)
                        .toList()
                )
                .build();
    }
}
