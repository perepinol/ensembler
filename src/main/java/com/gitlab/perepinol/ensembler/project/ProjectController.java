package com.gitlab.perepinol.ensembler.project;

import com.gitlab.perepinol.ensembler.boundary.NotFoundException;
import com.gitlab.perepinol.ensembler.musician.Musician;
import com.gitlab.perepinol.ensembler.musician.MusicianRepository;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/project")
public class ProjectController {
    private final ProjectRepository projectRepository;
    private final MusicianRepository musicianRepository;

    public ProjectController(
            ProjectRepository projectRepository,
            MusicianRepository musicianRepository
    ) {
        this.projectRepository = projectRepository;
        this.musicianRepository = musicianRepository;
    }

    @GetMapping
    public List<Project> getProjects() {
        return projectRepository.findAll();
    }

    @GetMapping("/{id}")
    public Project getProject(@PathVariable("id") long id) {
        return projectRepository.findById(id)
                .orElseThrow(NotFoundException::new);
    }

    @PostMapping
    public Project createProject(@RequestBody ProjectDto projectDto) {
        List<Musician> musicians = getMusiciansForProject(projectDto);

        return projectRepository.save(projectDto.toEntity(musicians));
    }

    @PutMapping("/{id}")
    public Project updateProject(@PathVariable("id") long id, @RequestBody ProjectDto projectDto) {
        if (projectDto.getId() != null && projectDto.getId() != id) {
            throw new IllegalArgumentException("Path and body IDs do not match");
        }

        Project project = projectDto.toEntity(getMusiciansForProject(projectDto))
                .toBuilder()
                .id(id)
                .build();

        return projectRepository.save(project);
    }

    @DeleteMapping("/{id}")
    public Project deleteProject(@PathVariable("id") long id) {
        Project project = projectRepository.findById(id).orElseThrow(NotFoundException::new);
        projectRepository.delete(project);

        return project;
    }

    private List<Musician> getMusiciansForProject(ProjectDto projectDto) {
        return musicianRepository.findAllById(projectDto.getParticipantIds());
    }
}
