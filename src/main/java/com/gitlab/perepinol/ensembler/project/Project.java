package com.gitlab.perepinol.ensembler.project;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.gitlab.perepinol.ensembler.musician.Musician;
import com.gitlab.perepinol.ensembler.concert.Concert;
import com.gitlab.perepinol.ensembler.rehearsal.Rehearsal;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import jakarta.persistence.UniqueConstraint;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.HashSet;
import java.util.Set;

@Entity
@Table(uniqueConstraints = {@UniqueConstraint(columnNames = {"name", "season"})})
@Getter
@Builder(access = AccessLevel.PUBLIC, toBuilder = true)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Setter(AccessLevel.PRIVATE)
public class Project {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private Long id;

    @Column(nullable = false)
    private String name;

    @Column(name = "season", nullable = false)
    private int year;

    private String conductor;

    @OneToMany(mappedBy = "project")
    @Builder.Default
    @JsonIgnore
    private Set<Rehearsal> rehearsals = new HashSet<>();

    @OneToMany(mappedBy = "project")
    @Builder.Default
    @JsonIgnore
    private Set<Concert> concerts = new HashSet<>();

    @ManyToMany(fetch = FetchType.EAGER)
    // @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinTable(
            name = "participation",
            joinColumns = @JoinColumn(name = "project_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "musician_id", referencedColumnName = "id")
    )
    @Builder.Default
    private Set<Musician> participants = new HashSet<>();

    @Override
    public String toString() {
        return String.format("Project{id=%d,name=%s,year=%s,conductor=%s", id, name, year, conductor);
    }
}
