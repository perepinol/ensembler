package com.gitlab.perepinol.ensembler.rehearsal;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.gitlab.perepinol.ensembler.project.Project;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.extern.jackson.Jacksonized;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@Jacksonized
@Builder(access = AccessLevel.PACKAGE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
class IncomingRehearsalDto {
    @Getter(AccessLevel.PACKAGE)
    private final Long id;

    private final String date;

    private final int durationSeconds;

    @Getter(AccessLevel.PACKAGE)
    private final long projectId;

    @JsonProperty("attendees")
    private final List<IncomingAttendanceDto> attendances;

    public Rehearsal toEntity(Project project) {
        if (project.getId() != projectId) {
            throw new IllegalArgumentException("Project IDs do not match");
        }

        if (attendances.size() != attendances.stream().map(IncomingAttendanceDto::id).distinct().count()) {
            throw new IllegalArgumentException("There are duplicated attendees");
        }

        return Rehearsal.builder()
                .id(id)
                .project(project)
                .durationSeconds(durationSeconds)
                .date(LocalDate.parse(date))
                .attendances(this.attendances.stream()
                        .map(attendee -> project.getParticipants().stream()
                                .filter(participant -> participant.getId().equals(attendee.id()))
                                .findFirst()
                                .map(musician -> RehearsalAttendance.builder()
                                        // We have to set the Rehearsal ID later because it may be null
                                        .musician(musician)
                                        .durationSeconds(attendee.durationSeconds())
                                        .build()
                                )
                                .orElseThrow(IllegalArgumentException::new)
                        )
                        .collect(Collectors.toSet())
                )
                .build();
    }

    @Jacksonized
    @Builder(access = AccessLevel.PACKAGE)
    private record IncomingAttendanceDto(Long id, int durationSeconds) {
    }
}
