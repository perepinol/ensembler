package com.gitlab.perepinol.ensembler.rehearsal;

import com.gitlab.perepinol.ensembler.boundary.NotFoundException;
import com.gitlab.perepinol.ensembler.project.Project;
import com.gitlab.perepinol.ensembler.project.ProjectRepository;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/rehearsal")
public class RehearsalController {
    private final RehearsalRepository rehearsalRepository;
    private final ProjectRepository projectRepository;

    RehearsalController(
            RehearsalRepository rehearsalRepository,
            ProjectRepository projectRepository
    ) {
        this.rehearsalRepository = rehearsalRepository;
        this.projectRepository = projectRepository;
    }

    @GetMapping
    List<Rehearsal> getRehearsals() {
        return rehearsalRepository.findAll();
    }

    @GetMapping("/{id}")
    Rehearsal getRehearsal(@PathVariable("id") long id) {
        return rehearsalRepository.findById(id)
                .orElseThrow(NotFoundException::new);
    }

    @PostMapping
    Rehearsal createRehearsal(@RequestBody IncomingRehearsalDto rehearsalDto) {
        Project project = getProjectForRehearsal(rehearsalDto);

        return rehearsalRepository.save(rehearsalDto.toEntity(project));
    }

    @PutMapping("/{id}")
    Rehearsal updateRehearsal(@PathVariable("id") long id, @RequestBody IncomingRehearsalDto rehearsalDto) {
        if (rehearsalDto.getId() != null && rehearsalDto.getId() != id) {
            throw new IllegalArgumentException("Path and body IDs do not match");
        }

        Rehearsal rehearsal = rehearsalDto.toEntity(getProjectForRehearsal(rehearsalDto))
                .toBuilder()
                .id(id)
                .build();

        return rehearsalRepository.save(rehearsal);
    }

    @DeleteMapping("/{id}")
    Rehearsal deleteRehearsal(@PathVariable("id") long id) {
        Rehearsal rehearsal = rehearsalRepository.findById(id).orElseThrow(NotFoundException::new);
        rehearsalRepository.delete(rehearsal);

        return rehearsal;
    }

    private Project getProjectForRehearsal(IncomingRehearsalDto rehearsalDto) {
        return projectRepository.findById(rehearsalDto.getProjectId())
                .orElseThrow(NotFoundException::new);
    }
}
