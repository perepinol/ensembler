package com.gitlab.perepinol.ensembler.rehearsal;

import com.gitlab.perepinol.ensembler.musician.Musician;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Entity
@Builder(access = AccessLevel.PUBLIC, toBuilder = true)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class RehearsalAttendance {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "rehearsal_id", referencedColumnName = "id")
    private Rehearsal rehearsal;

    @ManyToOne
    @JoinColumn(name = "musician_id", referencedColumnName = "id")
    @Getter
    private Musician musician;

    @Column(name = "duration")
    @Getter
    private int durationSeconds;
}
