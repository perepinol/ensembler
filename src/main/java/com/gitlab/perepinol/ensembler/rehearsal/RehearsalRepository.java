package com.gitlab.perepinol.ensembler.rehearsal;

import org.springframework.data.jpa.repository.JpaRepository;

interface RehearsalRepository extends JpaRepository<Rehearsal, Long> {
}
