package com.gitlab.perepinol.ensembler.rehearsal;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.gitlab.perepinol.ensembler.project.Project;
import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDate;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
@Getter(AccessLevel.PUBLIC)
@Builder(access = AccessLevel.PUBLIC, toBuilder = true)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Rehearsal {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private Long id;

    @Column(name = "rehearsal_date", nullable = false)
    private LocalDate date;

    @Column(name = "duration", nullable = false)
    private int durationSeconds;

    @ManyToOne
    @JoinColumn(name = "project_id", referencedColumnName = "id", nullable = false)
    private Project project;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "rehearsal")
    @JsonProperty("attendees")
    private Set<RehearsalAttendance> attendances;

    private Rehearsal(Long id, LocalDate date, int durationSeconds, Project project, Set<RehearsalAttendance> attendances) {
        this.id = id;
        this.date = date;
        this.durationSeconds = durationSeconds;
        this.project = project;
        this.attendances = attendances.stream()
                .map(attendee -> attendee.toBuilder().rehearsal(this).build())
                .collect(Collectors.toSet());
    }
}
