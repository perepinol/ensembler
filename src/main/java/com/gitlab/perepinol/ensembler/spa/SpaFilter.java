package com.gitlab.perepinol.ensembler.spa;

import jakarta.servlet.*;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;
import java.util.stream.Stream;

@Component
public class SpaFilter implements Filter {
    private static final String API_PREFIX = "/api";
    private static final List<String> ALLOWED_STATIC_FILES = List.of(
            "index.html"
    );

    private static final List<String> ALLOWED_STATIC_FORMATS = List.of(
            "js",
            "png",
            "jpg",
            "ico",
            "css",
            "woff",
            "woff2"
    );

    @Override
    public void doFilter(
            ServletRequest request,
            ServletResponse response,
            FilterChain chain
    ) throws IOException, ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest) request;

        if (
                !httpRequest.getRequestURI().startsWith(API_PREFIX)
                && Stream.concat(
                        ALLOWED_STATIC_FILES.stream().map(file -> file + "$"),
                        ALLOWED_STATIC_FORMATS.stream().map(format -> ".*\\." + format + "$")
                ).noneMatch(fileRegex -> httpRequest.getRequestURI().matches(fileRegex))
        ) {
            httpRequest.getRequestDispatcher("/index.html").forward(request, response);
            return;
        }

        chain.doFilter(request, response);
    }
}
