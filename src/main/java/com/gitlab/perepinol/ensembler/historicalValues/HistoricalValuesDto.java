package com.gitlab.perepinol.ensembler.historicalValues;

import lombok.Getter;
import lombok.extern.jackson.Jacksonized;

import java.util.List;
import java.util.stream.Collectors;

@Getter
public class HistoricalValuesDto {
    private final HistoricalValueType type;
    private final List<HistoricalValueDto> values;

    private HistoricalValuesDto(HistoricalValueType type, List<HistoricalValueDto> values) {
        this.type = type;
        this.values = values;
    }

    public static List<HistoricalValuesDto> from(List<HistoricalValue> historicalValues) {
        return historicalValues.stream()
                .collect(Collectors.groupingBy(HistoricalValue::getType))
                .entrySet()
                .stream()
                .map(kvp -> {
                    List<HistoricalValueDto> values = kvp.getValue().stream()
                            .map(HistoricalValueDto::fromEntity)
                            .toList();

                    return new HistoricalValuesDto(kvp.getKey(), values);
                })
                .toList();
    }
}
