package com.gitlab.perepinol.ensembler.historicalValues;

import com.gitlab.perepinol.ensembler.boundary.NotFoundException;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Comparator;
import java.util.List;

@RestController
@RequestMapping("/api/historical")
class HistoricalValueController {
    private final HistoricalValueRepository historicalValueRepository;

    public HistoricalValueController(HistoricalValueRepository historicalValueRepository) {
        this.historicalValueRepository = historicalValueRepository;
    }

    @GetMapping
    public List<HistoricalValuesDto> getAll() {
        List<HistoricalValue> values = historicalValueRepository.findAll();
        return HistoricalValuesDto.from(values);
    }

    @GetMapping("/{type}")
    public List<HistoricalValueDto> getAllByType(@PathVariable("type") String typeString) {
        HistoricalValueType type = HistoricalValueType.from(typeString);
        return historicalValueRepository.findByType(type)
                .getHistoricalValues()
                .stream()
                .map(HistoricalValueDto::fromEntity)
                .toList();
    }

    @PostMapping("/{type}")
    public HistoricalValueDto create(
            @PathVariable("type") String typeString,
            @RequestBody HistoricalValueDto historicalValueDto
    ) {
        HistoricalValueType type = HistoricalValueType.from(typeString);
        return HistoricalValueDto.fromEntity(
                historicalValueRepository.save(historicalValueDto.toEntity(type))
        );
    }

    @DeleteMapping("/{type}")
    public HistoricalValueDto deleteLast(
            @PathVariable("type") String typeString
    ) {
        HistoricalValueType type = HistoricalValueType.from(typeString);
        History history = historicalValueRepository.findByType(type);
        if (history.isEmpty()) {
            throw new NotFoundException();
        }
        HistoricalValue last = history.getCurrent();
        historicalValueRepository.delete(last);
        return HistoricalValueDto.fromEntity(last);
    }
}
