package com.gitlab.perepinol.ensembler.historicalValues;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.IdClass;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Entity
@IdClass(HistoricalValue.HistoricalValueId.class)
@Getter(AccessLevel.PACKAGE)
@Builder(access = AccessLevel.PUBLIC)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class HistoricalValue {
    @Id
    @Column
    private LocalDate since;

    @Id
    @Column
    private HistoricalValueType type;

    @Column(name = "hist_value")
    private double value;

    @NoArgsConstructor(access = AccessLevel.PRIVATE)
    public static class HistoricalValueId {
        private LocalDate since;
        private HistoricalValueType type;
    }
}
