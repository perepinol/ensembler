package com.gitlab.perepinol.ensembler.historicalValues;

import lombok.Getter;

import java.time.LocalDate;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

@Getter
public class History {
    private final List<HistoricalValue> historicalValues;

    public History(List<HistoricalValue> historicalValues) {
        this.historicalValues = historicalValues.stream()
                .sorted(Comparator.comparing(HistoricalValue::getSince))
                .toList();
    }

    public boolean isEmpty() {
        return this.historicalValues.isEmpty();
    }

    public HistoricalValue getCurrent() {
        if (this.isEmpty()) {
            throw new UnsupportedOperationException("Empty history");
        }
        return this.historicalValues.get(this.historicalValues.size() - 1);
    }

    public Optional<Double> getValueAt(LocalDate date) {
        for (int i = historicalValues.size() - 1; i >= 0; i--) {
            LocalDate since = historicalValues.get(i).getSince();
            if (since.isBefore(date) || since.isEqual(date)) {
                return Optional.of(historicalValues.get(i).getValue());
            }
        }
        return Optional.empty();
    }
}
