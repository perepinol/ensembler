package com.gitlab.perepinol.ensembler.historicalValues;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface HistoricalValueRepository extends JpaRepository<HistoricalValue, HistoricalValue.HistoricalValueId> {
    default History findByType(HistoricalValueType type) {
        return new History(findListByType(type));
    }

    List<HistoricalValue> findListByType(HistoricalValueType type);
}
