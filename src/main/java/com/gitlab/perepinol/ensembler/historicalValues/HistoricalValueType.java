package com.gitlab.perepinol.ensembler.historicalValues;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

import java.util.Arrays;

public enum HistoricalValueType {
    REHEARSAL("rehearsal"),
    CONCERT("concert"),
    MILEAGE("mileage");

    private final String label;

    HistoricalValueType(String label) {
        this.label = label;
    }

    @JsonCreator
    public static HistoricalValueType from(String label) {
        return Arrays.stream(HistoricalValueType.values())
                .filter(value -> value.label.equals(label))
                .findFirst()
                .orElseThrow(IllegalArgumentException::new);
    }

    @Override
    @JsonValue
    public String toString() {
        return label;
    }
}
