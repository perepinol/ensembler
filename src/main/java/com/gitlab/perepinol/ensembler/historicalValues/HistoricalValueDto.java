package com.gitlab.perepinol.ensembler.historicalValues;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.extern.jackson.Jacksonized;

import java.time.LocalDate;

@Jacksonized
@Builder(access = AccessLevel.PACKAGE)
@Getter
public class HistoricalValueDto {
    private LocalDate since;
    private double value;

    public static HistoricalValueDto fromEntity(HistoricalValue historicalValue) {
        return HistoricalValueDto.builder()
                .since(historicalValue.getSince())
                .value(historicalValue.getValue())
                .build();
    }

    public HistoricalValue toEntity(HistoricalValueType type) {
        return HistoricalValue.builder()
                .since(since)
                .type(type)
                .value(value)
                .build();
    }
}
