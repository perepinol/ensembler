package com.gitlab.perepinol.ensembler.payout;

public interface Payout {
    double getTotal();
}
