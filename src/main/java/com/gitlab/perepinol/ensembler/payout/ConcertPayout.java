package com.gitlab.perepinol.ensembler.payout;

import com.gitlab.perepinol.ensembler.concert.Concert;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

@Getter
@Builder(access = AccessLevel.PROTECTED, toBuilder = true)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
class ConcertPayout implements Payout {
    @Getter(AccessLevel.PROTECTED)
    private Concert concert;
    private boolean didAttend;
    private double pricePerConcert;

    public long getConcertId() {
        return concert.getId();
    }

    public double getTotal() {
        return didAttend ? pricePerConcert : 0;
    }
}
