package com.gitlab.perepinol.ensembler.payout;

import com.gitlab.perepinol.ensembler.concert.Concert;
import com.gitlab.perepinol.ensembler.date.DateProvider;
import com.gitlab.perepinol.ensembler.musician.Musician;
import com.gitlab.perepinol.ensembler.payout.csv.CsvBuilder;
import com.gitlab.perepinol.ensembler.project.Project;
import com.gitlab.perepinol.ensembler.rehearsal.Rehearsal;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Getter
@Builder(access = AccessLevel.PROTECTED)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
class ProjectPayout implements Payout {
    private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("dd/MM/yyyy");

    private final Project project;

    // Rehearsals and concerts are not included in Project because that
    // would cause infinite recursion when JSON-serialising. So we add them here
    // They still contain unnecessary references to the project though.
    private final Set<Rehearsal> rehearsals;
    private final Set<Concert> concerts;

    // Maps of musician ID -> payouts
    private final Map<Long, MusicianPayout> payouts;

    @Override
    public double getTotal() {
        return payouts.values().stream().map(Payout::getTotal).reduce(0.0, Double::sum);
    }

    public String toCsv() {
        CsvBuilder csv = new CsvBuilder(";", System.lineSeparator());
        csv.appendLine(
                CsvFields.DATE,
                CsvFields.DESCRIPTION,
                CsvFields.RECORD,
                CsvFields.ACCOUNT,
                CsvFields.LEFT,
                CsvFields.RIGHT
        );

        csv.appendLine(
                DateProvider.get().format(FORMATTER),
                project.getName(),
                "1",
                "4100020",
                "",
                getTotal()
        );

        payouts.forEach((id, payout) -> {
            if (payout.getTotal() == 0) {
                return;
            }

            Musician musician = project.getParticipants().stream()
                    .filter(p -> id.equals(p.getId()))
                    .findFirst()
                    .orElseThrow(IllegalStateException::new);

            if (musician.getAccountNumber() == null) {
                throw new IllegalStateException(String.format(
                        "Account number for %s not available",
                        musician.getFullName()
                ));
            }

            csv.appendLine(
                    "",
                    "",
                    "1",
                    musician.getAccountNumber(),
                    payout.getTotal(),
                    ""
            );
        });

        return csv.build();
    }

    @Getter
    @AllArgsConstructor(access = AccessLevel.PRIVATE)
    static class MusicianPayout implements Payout {
        private final AggregatedPayout<RehearsalPayout> rehearsals;
        private final AggregatedPayout<MileagePayout> mileages;
        private final AggregatedPayout<ConcertPayout> concerts;

        @Builder(access = AccessLevel.PROTECTED)
        private MusicianPayout(
                List<RehearsalPayout> rehearsals,
                List<MileagePayout> mileages,
                List<ConcertPayout> concerts
        ) {
            this.rehearsals = new AggregatedPayout<>(rehearsals);
            this.mileages = new AggregatedPayout<>(mileages);
            this.concerts = new AggregatedPayout<>(concerts);
        }

        public double getTotal() {
            return rehearsals.getTotal() + mileages.getTotal() + concerts.getTotal();
        }
    }

    @Getter
    static class AggregatedPayout<T extends Payout> implements Payout {
        private final List<T> payouts;

        public AggregatedPayout(List<T> payouts) {
            this.payouts = payouts;
        }

        @Override
        public double getTotal() {
            return payouts.stream().map(Payout::getTotal).reduce(0.0, Double::sum);
        }
    }

    private enum CsvFields {
        DATE("FECHA"),
        DESCRIPTION("CONCEPTO"),
        ACCOUNT("CUENTA"),
        LEFT("DEBE"),
        RIGHT("HABER"),
        RECORD("REGISTRO");

        private final String fieldName;

        CsvFields(String fieldName) {
            this.fieldName = fieldName;
        }

        @Override
        public String toString() {
            return fieldName;
        }
    }
}
