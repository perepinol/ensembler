package com.gitlab.perepinol.ensembler.payout;

import com.gitlab.perepinol.ensembler.rehearsal.Rehearsal;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

@Getter
@Builder(access = AccessLevel.PROTECTED, toBuilder = true)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
class RehearsalPayout implements Payout {
    @Getter(AccessLevel.PROTECTED)
    private Rehearsal rehearsal;
    private int durationSeconds;
    private double pricePerHour;

    public long getRehearsalId() {
        return rehearsal.getId();
    }

    public double getTotal() {
        return durationSeconds * pricePerHour / 3600;
    }
}
