package com.gitlab.perepinol.ensembler.payout;

import com.gitlab.perepinol.ensembler.rehearsal.Rehearsal;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

@Getter
@Builder(access = AccessLevel.PROTECTED, toBuilder = true)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class MileagePayout implements Payout {
    @Getter(AccessLevel.PROTECTED)
    private Rehearsal rehearsal;
    private int distance;
    private boolean attended;
    private double pricePerKm;

    public long getRehearsalId() {
        return rehearsal.getId();
    }

    public double getTotal() {
        return attended ? distance * pricePerKm : 0;
    }
}
