package com.gitlab.perepinol.ensembler.payout;

import com.gitlab.perepinol.ensembler.concert.Concert;
import com.gitlab.perepinol.ensembler.historicalValues.History;
import com.gitlab.perepinol.ensembler.musician.Musician;
import com.gitlab.perepinol.ensembler.project.Project;
import com.gitlab.perepinol.ensembler.rehearsal.Rehearsal;
import com.gitlab.perepinol.ensembler.rehearsal.RehearsalAttendance;

import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class PayoutCalculator {
    public static ProjectPayout calculate(
            Project project,
            History rehearsalHistory,
            History concertHistory,
            History mileageHistory
    ) {
        // We inject the historical prices into the payouts now
        // Then when iterating the musicians we only have to add their information
        List<RehearsalPayout> rehearsals = project.getRehearsals().stream()
                .sorted(Comparator.comparing(Rehearsal::getDate))
                .map(rehearsal -> RehearsalPayout.builder()
                        .rehearsal(rehearsal)
                        .pricePerHour(rehearsalHistory.getValueAt(rehearsal.getDate())
                                .orElseThrow(() -> new IllegalStateException(
                                        String.format("Rehearsal price at %s does not exist", rehearsal.getDate())
                                ))
                        )
                        .build()
                )
                .toList();
        List<MileagePayout> mileages = project.getRehearsals().stream()
                .sorted(Comparator.comparing(Rehearsal::getDate))
                .map(rehearsal -> MileagePayout.builder()
                        .rehearsal(rehearsal)
                        .pricePerKm(mileageHistory.getValueAt(rehearsal.getDate())
                                .orElseThrow(() -> new IllegalStateException(
                                        String.format("Mileage price at %s does not exist", rehearsal.getDate())
                                ))
                        )
                        .build()
                )
                .toList();
        List<ConcertPayout> concerts = project.getConcerts().stream()
                .sorted(Comparator.comparing(Concert::getDate))
                .map(concert -> ConcertPayout.builder()
                        .concert(concert)
                        .pricePerConcert(concertHistory.getValueAt(concert.getDate())
                                .orElseThrow(() -> new IllegalStateException(
                                        String.format("Concert price at %s does not exist", concert.getDate())
                                ))
                        )
                        .build()
                )
                .toList();

        Map<Long, ProjectPayout.MusicianPayout> payouts = new HashMap<>();
        for (Musician musician : project.getParticipants()) {
            payouts.put(
                    musician.getId(),
                    ProjectPayout.MusicianPayout.builder()
                            .rehearsals(
                                    rehearsals.stream().map(rehearsal -> rehearsal.toBuilder()
                                            .durationSeconds(rehearsal.getRehearsal().getAttendances()
                                                    .stream()
                                                    .filter(attendance -> attendance.getMusician().equals(musician))
                                                    .findFirst()
                                                    .map(RehearsalAttendance::getDurationSeconds)
                                                    .orElse(0)
                                            )
                                            .build()
                                    ).toList()
                            )
                            .mileages(
                                    mileages.stream().map(mileage -> mileage.toBuilder()
                                            .distance(musician.getLocation().getDistance())
                                            .attended(mileage.getRehearsal().getAttendances()
                                                    .stream()
                                                    .filter(attendance -> attendance.getMusician().equals(musician))
                                                    .findFirst()
                                                    .map(RehearsalAttendance::getDurationSeconds)
                                                    .orElse(0) > 0
                                            )
                                            .build()
                                    ).toList()
                            )
                            .concerts(
                                    concerts.stream().map(concert -> concert.toBuilder()
                                            .didAttend(concert.getConcert().getAttendances()
                                                    .stream()
                                                    .anyMatch(attendance -> attendance.equals(musician))
                                            )
                                            .build()
                                    ).toList()
                            )
                            .build()
            );
        }

        return ProjectPayout.builder()
                .project(project)
                .payouts(payouts)
                .rehearsals(project.getRehearsals())
                .concerts(project.getConcerts())
                .build();
    }
}
