package com.gitlab.perepinol.ensembler.payout.csv;

public class CsvBuilder {
    private final String fieldSeparator;
    private final String lineSeparator;

    private final StringBuilder stringBuilder;

    private boolean insertedFirst;

    public CsvBuilder(String fieldSeparator, String lineSeparator) {
        this.fieldSeparator = fieldSeparator;
        this.lineSeparator = lineSeparator;

        this.stringBuilder = new StringBuilder();

        this.insertedFirst = false;
    }

    public void appendLine(Object... items) {
        if (items.length == 0) {
            return;
        }

        if (insertedFirst) {
            stringBuilder.append(lineSeparator);
        }

        insertedFirst = true;

        stringBuilder.append(items[0].toString());

        for (int i = 1; i < items.length; i++) {
            stringBuilder.append(fieldSeparator);
            stringBuilder.append(items[i].toString());
        }
    }

    public String build() {
        return stringBuilder.toString();
    }
}
