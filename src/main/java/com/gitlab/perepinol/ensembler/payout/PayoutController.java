package com.gitlab.perepinol.ensembler.payout;

import com.gitlab.perepinol.ensembler.boundary.NotFoundException;
import com.gitlab.perepinol.ensembler.historicalValues.HistoricalValueRepository;
import com.gitlab.perepinol.ensembler.historicalValues.HistoricalValueType;
import com.gitlab.perepinol.ensembler.historicalValues.History;
import com.gitlab.perepinol.ensembler.project.Project;
import com.gitlab.perepinol.ensembler.project.ProjectRepository;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@RestController
@RequestMapping("/api/payout")
class PayoutController {
    private final ProjectRepository projectRepository;
    private final HistoricalValueRepository historicalValueRepository;

    public PayoutController(ProjectRepository projectRepository, HistoricalValueRepository historicalValueRepository) {
        this.projectRepository = projectRepository;
        this.historicalValueRepository = historicalValueRepository;
    }

    @GetMapping("/{projectId}")
    public ProjectPayout getPayoutForProject(@PathVariable("projectId") long id) {
        Project project = projectRepository.findById(id)
                .orElseThrow(NotFoundException::new);

        History rehearsalHistory = historicalValueRepository.findByType(HistoricalValueType.REHEARSAL);
        History concertHistory = historicalValueRepository.findByType(HistoricalValueType.CONCERT);
        History mileageHistory = historicalValueRepository.findByType(HistoricalValueType.MILEAGE);

        try {
            return PayoutCalculator.calculate(project, rehearsalHistory, concertHistory, mileageHistory);
        } catch (IllegalStateException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @GetMapping("/{projectId}/export")
    public void exportPayoutForProjectAsCsv(
            HttpServletResponse response,
            @PathVariable("projectId") long id
    ) throws IOException {
        ProjectPayout payout = getPayoutForProject(id);
        response.setContentType("text/csv");
        response.setHeader(
                "Content-Disposition",
                String.format(
                        "attachment; filename=%s_payouts.csv",
                        payout.getProject().getName().toLowerCase().replace(" ", "_")
                )
        );

        response.getWriter().print(payout.toCsv());
    }
}
