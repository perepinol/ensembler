package com.gitlab.perepinol.ensembler;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;

import java.util.Arrays;

@SpringBootApplication
public class EnsemblerApplication {
	public static void main(String[] args) {
		SpringApplication.run(EnsemblerApplication.class, args);
	}

	@Bean
	@Profile("debug")
	CommandLineRunner loggingRunner(ApplicationContext context) {
		return args -> Arrays.stream(context.getBeanDefinitionNames()).sorted().forEach(System.out::println);
	}
}
