package com.gitlab.perepinol.ensembler;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import liquibase.integration.spring.SpringLiquibase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.concurrent.locks.LockSupport;

@Configuration
public class EnsemblerApplicationConfiguration {
    private static final Logger LOGGER = LoggerFactory.getLogger(EnsemblerApplicationConfiguration.class.getName());

    @Autowired
    Environment environment;

    @Bean
    DataSource dataSource() {
        String databaseUrl = environment.getRequiredProperty("database.url");
        String driverName = getFirstAvailableDriver(databaseUrl).getClass().getName();

        LOGGER.info("Using database driver {} for {}", driverName, databaseUrl);

        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(driverName);
        dataSource.setUrl(databaseUrl);
        dataSource.setUsername(environment.getRequiredProperty("database.username"));
        dataSource.setPassword(environment.getRequiredProperty("database.password"));

        checkDatabaseConnectivity(dataSource);

        return dataSource;
    }

    @Bean
    SpringLiquibase liquibase() {
        SpringLiquibase liquibase = new SpringLiquibase();
        liquibase.setDataSource(dataSource());
        liquibase.setChangeLog("classpath:liquibase/migrations.xml");

        return liquibase;
    }

    @Bean
    ObjectMapper objectMapper() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new JavaTimeModule());
        mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        return mapper;
    }

    private Driver getFirstAvailableDriver(String url) {
        return DriverManager.drivers()
                .filter(driver -> {
                    try {
                        return driver.acceptsURL(url);
                    } catch (SQLException e) {
                        return false;
                    }
                })
                .findFirst()
                .orElseThrow(() -> new RuntimeException(String.format(
                        "No available drivers for %s",
                        url
                )));
    }

    private void checkDatabaseConnectivity(DataSource dataSource) {
        int retries = environment.getRequiredProperty("database.retries", Integer.class);
        long retryWaitSeconds = environment.getRequiredProperty(
                "database.retry_wait_seconds",
                Long.class
        );

        SQLException e = null;
        for (int i = 0; i < retries; i++) {
            if (i > 0) {
                LockSupport.parkNanos(retryWaitSeconds * 1000000000);
            }

            try (Connection ignored = dataSource.getConnection()) {
                e = null;
                break;
            } catch (SQLException sqlException) {
                e = sqlException;
            }

            LOGGER.info(
                    "Could not connect to the database ({}/{}). Retrying in {} seconds",
                    i + 1,
                    retries,
                    retryWaitSeconds
            );
        }

        if (e != null) {
            throw new RuntimeException(e);
        }
    }
}
