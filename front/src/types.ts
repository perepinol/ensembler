import { AcademicYear } from './app/utils/academic-year/academic-year';
import { Duration } from './app/utils/duration/duration';

export interface Model {
    id: unknown;
}

export type New<T extends Model> = Omit<T, 'id'>

export const isExistingModel = <T extends Model>(model: T | New<T>): model is T => {
    return Object.keys(model).includes('id');
};

export interface Location extends Model {
    id: number;
    name: string;
    distance: number;
}

export interface Instrument extends Model {
    id: number;
    name: string;
}

export interface Musician extends Model {
    id: number;
    givenName: string;
    familyName: string;
    instrument: Instrument;
    location: Location;
    accountNumber?: string;
}

export interface Project extends Model {
    id: number;
    name: string;
    year: AcademicYear;
    conductor: string;
    participants: Musician[];
}

export interface RehearsalAttendance {
    musician: Musician;
    duration: Duration;
}

export interface Rehearsal extends Model {
    id: number;
    date: Date;
    project: Project;
    duration: Duration;
    attendees: RehearsalAttendance[];
}

export interface Concert extends Model {
    id: number;
    date: Date;
    project: Project;
    location: string;
    attendees: Musician[];
}

export enum HistoricalType {
    REHEARSAL = 'rehearsal',
    CONCERT = 'concert',
    MILEAGE = 'mileage'
}

export interface Historical {
    since: Date;
    value: number;
}

export interface PayoutAggregation<T> {
    payouts: T[];
    total: number;
}

export interface RehearsalPayout {
    rehearsal: Rehearsal;
    total: number;
}

export interface MileagePayout {
    rehearsal: Rehearsal;
    total: number;
}

export interface ConcertPayout {
    concert: Concert;
    total: number;
}

export interface MusicianPayout {
    rehearsals: PayoutAggregation<RehearsalPayout>;
    mileages: PayoutAggregation<MileagePayout>;
    concerts: PayoutAggregation<ConcertPayout>;
    total: number;
}

export interface ProjectPayout {
    project: Project;
    payouts: Record<Musician['id'], MusicianPayout>;
    total: number;
}