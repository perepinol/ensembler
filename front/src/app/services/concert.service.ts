import { Concert, Musician, New, Project } from 'src/types';
import { CrudService } from './crud.service';
import { dateToString, stringToDate } from './formatters';

export interface ConcertIncomingDto extends Omit<Concert, 'date'> {
    date: string;
}

interface ConcertOutgoingDto extends Omit<ConcertIncomingDto, 'project' | 'attendees'> {
    projectId: Project['id'];
    attendeeIds: Musician['id'][];
}

export class ConcertService extends CrudService<Concert, ConcertIncomingDto, ConcertOutgoingDto> {
    constructor() {
        super('/concert');
    }

    protected override toJson(body: Concert | New<Concert>): ConcertOutgoingDto | New<ConcertOutgoingDto> {
        const { date, project, attendees, ...other } = body;
        return {
            ...other,
            date: dateToString(date),
            projectId: project.id,
            attendeeIds: attendees.map(attendee => attendee.id)
        };
    }

    public override fromJson(body: ConcertIncomingDto): Concert {
        return {
            ...body,
            date: stringToDate(body.date)
        };
    }
}