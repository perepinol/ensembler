
interface JsonFetchOptions extends Omit<RequestInit, 'body'> {
    body?: unknown;
}

export class BaseService {
    private root;

    constructor(root: string) {
        this.root = '/api' + root;
    }

    protected async jsonFetch(url: string, options?: JsonFetchOptions) {
        const response = await this.fetch(url, {
            ...(options ?? {}),
            body: options?.body ? JSON.stringify(options.body) : undefined,
            headers: {
                'Content-Type': 'application/json'
            }
        });

        return await response.json();
    }

    protected async fetch(url: string, options?: RequestInit) {
        return await fetch(this.root + url.replace(/\/$/, ''), options);
    }
}