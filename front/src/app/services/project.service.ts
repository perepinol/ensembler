import { Musician, New, Project } from '../../types';
import { AcademicYear } from '../utils/academic-year/academic-year';
import { CrudService } from './crud.service';

export interface IncomingProjectDto extends Omit<Project, 'year'> {
  year: number;
}

interface OutgoingProjectDto extends Omit<IncomingProjectDto, 'participants'> {
  participantIds: Musician['id'][];
}

export class ProjectService extends CrudService<Project, IncomingProjectDto, OutgoingProjectDto> {
  constructor() {
    super('/project');
  }

  protected override toJson(body: Project | New<Project>): OutgoingProjectDto | New<OutgoingProjectDto> {
    const { year, participants, ...other } = body;
    return {
      ...other,
      year: year.mainYear(),
      participantIds: participants.map(participant => participant.id)
    };
  }

  public override fromJson(body: IncomingProjectDto): Project {
    return {
      ...body,
      year: new AcademicYear(body.year)
    };
  }
}
