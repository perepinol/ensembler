import { Concert, ConcertPayout, MileagePayout, Model, MusicianPayout, PayoutAggregation, Project, ProjectPayout, Rehearsal, RehearsalPayout } from 'src/types';
import { BaseService } from './base.service';
import { IncomingProjectDto, ProjectService } from './project.service';
import { RehearsalIncomingDto, RehearsalService } from './rehearsal.service';
import { ConcertIncomingDto, ConcertService } from './concert.service';

interface RehearsalPayoutDto extends Omit<RehearsalPayout, 'rehearsal'> {
  rehearsalId: Rehearsal['id'];
}

interface MileagePayoutDto extends Omit<MileagePayout, 'rehearsal'> {
  rehearsalId: Rehearsal['id'];
}

interface ConcertPayoutDto extends Omit<ConcertPayout, 'concert'> {
  concertId: Concert['id'];
}

interface MusicianPayoutDto extends Omit<MusicianPayout, 'rehearsals' | 'mileages' | 'concerts'> {
  rehearsals: PayoutAggregation<RehearsalPayoutDto>;
  mileages: PayoutAggregation<MileagePayoutDto>;
  concerts: PayoutAggregation<ConcertPayoutDto>;
}

interface ProjectPayoutDto extends Omit<ProjectPayout, 'project' | 'payouts'> {
  project: IncomingProjectDto;
  rehearsals: RehearsalIncomingDto[];
  concerts: ConcertIncomingDto[];
  payouts: Record<Project['id'], MusicianPayoutDto>;
}

export class PayoutService extends BaseService {
  private projectService: ProjectService;
  private rehearsalService: RehearsalService;
  private concertService: ConcertService;

  constructor() {
    super('/payout');
    this.projectService = new ProjectService();
    this.rehearsalService = new RehearsalService();
    this.concertService = new ConcertService();
  }

  async getPayout(projectId: Project['id']): Promise<ProjectPayout> {
    const json: ProjectPayoutDto = await this.jsonFetch(`/${projectId}`, {
      method: 'GET'
    });

    return {
      total: json.total,
      project: this.projectService.fromJson(json.project),
      payouts: mapEntries(json.payouts, p => {
        const rehearsals: PayoutAggregation<RehearsalPayout> = {
          total: p.rehearsals.total,
          payouts: p.rehearsals.payouts.map(p => ({
            total: p.total,
            rehearsal: this.rehearsalService.fromJson(getById(json.rehearsals, p.rehearsalId))
          }))
        };
        const mileages: PayoutAggregation<MileagePayout> = {
          total: p.mileages.total,
          payouts: p.mileages.payouts.map(p => ({
            total: p.total,
            rehearsal: this.rehearsalService.fromJson(getById(json.rehearsals, p.rehearsalId))
          }))
        };
        const concerts: PayoutAggregation<ConcertPayout> = {
          total: p.concerts.total,
          payouts: p.concerts.payouts.map(p => ({
            total: p.total,
            concert: this.concertService.fromJson(getById(json.concerts, p.concertId))
          }))
        };
        return {
          ...p,
          rehearsals,
          mileages,
          concerts
        };
      })
    };
  }

  async exportPayout(projectId: Project['id']): Promise<void> {
    const response = await this.fetch(`/${projectId}/export`, {
      method: 'GET'
    });

    if (response.status < 200 || response.status >= 300) {
      throw Error(await response.text());
    }

    const filename = response.headers.get('Content-Disposition')?.match(/filename=(.+)/)?.[1];
    if (!filename) {
      throw new Error('Filename not available from headers');
    }

    const blob = await response.blob();

    const a = document.createElement('a');
    a.href = window.URL.createObjectURL(blob);
    a.download = filename;

    a.click();
  }
}

function mapEntries<K extends string | number | symbol, V, W>(input: Record<K, V>, mapFunc: (value: V) => W): Record<K, W> {
  return Object.entries(input).map(([k, v]) => [k, mapFunc(v as V)] as [K, W]).reduce((accum, [k, v]) => ({ ...accum, [k]: v }), {} as Record<K, W>);
}

function getById<T extends Model>(items: T[], id: T['id']): T {
  const item = items.find(item => item.id === id);
  if (!item) {
    throw new Error(`Item ${id} not found`);
  }
  return item;
}