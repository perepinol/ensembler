import { Model, New } from '../../types';
import { BaseService } from './base.service';


export class CrudService<Type extends Model, IncomingDto extends Model = Type, OutgoingDto extends Model = IncomingDto> extends BaseService {
    constructor(root: string) {
        super(root);
    }

    async getAll(): Promise<Type[]> {
        const result: IncomingDto[] = await super.jsonFetch('/');

        return result.map(element => this.fromJson(element));
    }

    async get(id: Type['id']): Promise<Type> {
        return await this.fetchType('GET', `/${id}`);
    }

    async create(model: New<Type>): Promise<Type> {
        return await this.fetchType('POST', '/', model);
    }

    async update(model: Type): Promise<Type> {
        return await this.fetchType('PUT', `/${model.id}`, model);
    }

    async delete(model: Type): Promise<Type> {
        return await this.fetchType('DELETE', `/${model.id}`);
    }

    private async fetchType(method: string, url: string, body?: Type | New<Type>): Promise<Type> {
        const result = await super.jsonFetch(url, {
                body: body ? this.toJson(body) : undefined,
                method
        });

        return this.fromJson(result);
    }

    protected toJson(body: Type | New<Type>): OutgoingDto | New<OutgoingDto> {
        return body as unknown as OutgoingDto;
    }

    protected fromJson(body: IncomingDto): Type {
        return body as unknown as Type;
    }
}