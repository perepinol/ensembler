import { Musician, New } from '../../types';
import { CrudService } from './crud.service';

interface OutgoingMusicianDto extends Omit<Musician, 'location' | 'instrument'> {
  locationId: number;
  instrumentId: number;
}

export class MusicianService extends CrudService<Musician, Musician, OutgoingMusicianDto> {
  constructor() {
    super('/musician');
  }

  protected override toJson(body: Musician | New<Musician>): OutgoingMusicianDto | New<OutgoingMusicianDto> {
    const { location, instrument, ...other } = body;
    return {
      ...other,
      locationId: location.id,
      instrumentId: instrument.id
    };
  }
}
