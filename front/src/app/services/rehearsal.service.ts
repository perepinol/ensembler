import { Musician, Project, Rehearsal, RehearsalAttendance } from 'src/types';
import { CrudService } from './crud.service';
import { dateToString, stringToDate } from './formatters';
import { Duration } from '../utils/duration/duration';

interface RehearsalAttendanceIncomingDto extends Omit<RehearsalAttendance, 'duration'> {
  durationSeconds: number;
}

interface RehearsalAttendanceOutgoingDto extends Omit<RehearsalAttendanceIncomingDto, 'musician'> {
  id: Musician['id'];
}

export interface RehearsalIncomingDto extends Omit<Rehearsal, 'date' | 'duration' | 'attendees'> {
  date: string;
  durationSeconds: number;
  attendees: RehearsalAttendanceIncomingDto[];
}

interface RehearsalOutgoingDto extends Omit<RehearsalIncomingDto, 'project' | 'attendees'> {
  projectId: Project['id'];
  attendees: RehearsalAttendanceOutgoingDto[];
}

export class RehearsalService extends CrudService<Rehearsal, RehearsalIncomingDto, RehearsalOutgoingDto> {
  constructor() {
    super('/rehearsal');
  }

  protected override toJson(body: Rehearsal): RehearsalOutgoingDto {
    const { project, attendees, duration, ...other } = body;
    return {
      ...other,
      date: dateToString(body.date),
      durationSeconds: duration.totalSeconds,
      projectId: project.id,
      attendees: attendees.map(attendance => ({
        durationSeconds: attendance.duration.totalSeconds,
        id: attendance.musician.id
      }))
    };
  }

  public override fromJson(body: RehearsalIncomingDto): Rehearsal {
    const {date, durationSeconds, attendees, ...other } = body;
    return {
      ...other,
      date: stringToDate(date),
      duration: Duration.fromSeconds(durationSeconds),
      attendees: attendees.map(attendee => ({
        musician: attendee.musician,
        duration: Duration.fromSeconds(attendee.durationSeconds)
      }))
    };
  }
}