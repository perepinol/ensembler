import { Instrument } from '../../types';
import { BaseService } from './base.service';

export class InstrumentService extends BaseService {
  constructor() {
    super('/musician/instrument');
  }

  async getAll(): Promise<Instrument[]> {
    const result: Instrument[] = await super.jsonFetch('/');

    return result;
}
}
