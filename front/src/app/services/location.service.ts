import { CrudService } from './crud.service';
import { Location } from '../../types';

export class LocationService extends CrudService<Location> {
  constructor() {
    super('/location');
  }
}