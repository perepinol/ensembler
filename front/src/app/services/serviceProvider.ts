import { Injectable } from '@angular/core';
import { MusicianService } from './musician.service';
import { ProjectService } from './project.service';
import { RehearsalService } from './rehearsal.service';
import { ConcertService } from './concert.service';
import { LocationService } from './location.service';
import { HistoricalValueService } from './historical-value.service';
import { InstrumentService } from './instrument.service';
import { PayoutService } from './payout.service';


@Injectable({
    providedIn: 'root'
})
export class ServiceProvider {
    locationService = new LocationService();
    instrumentService = new InstrumentService();
    musicianService = new MusicianService();
    projectService = new ProjectService();
    rehearsalService = new RehearsalService();
    concertService = new ConcertService();
    historicalValueService = new HistoricalValueService();
    payoutService = new PayoutService();
}