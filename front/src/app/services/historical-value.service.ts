import { Historical, HistoricalType } from 'src/types';
import { BaseService } from './base.service';
import { dateToString } from './formatters';

interface HistoricalDto extends Omit<Historical, 'since'> {
    since: string;
}

export class HistoricalValueService extends BaseService {
    constructor() {
        super('/historical');
    }

    async getAll(): Promise<Record<HistoricalType, Historical[]>> {
        const promises = Object.values(HistoricalType)
            .map(async type => [type, await this.getAllByType(type)] as [HistoricalType, Historical[]]);
        const entries = await Promise.all(promises);
        
        return Object.fromEntries(entries) as Record<HistoricalType, Historical[]>;
    }

    async getAllByType(type: HistoricalType): Promise<Historical[]> {
        const result: HistoricalDto[] = await super.jsonFetch(`/${type}`);

        return result.map(this.fromJson);
    }

    async create(type: HistoricalType, value: Historical): Promise<Historical> {
        const result: HistoricalDto = await super.jsonFetch(`/${type}`, { method: 'POST', body: this.toJson(value) });

        return this.fromJson(result);
    }

    async deleteLast(type: HistoricalType): Promise<Historical> {
        const result: HistoricalDto = await super.jsonFetch(`/${type}`, { method: 'DELETE' });

        return this.fromJson(result);
    }

    private toJson(historical: Historical): HistoricalDto {
        return {
            ...historical,
            since: dateToString(historical.since)
        };
    }

    private fromJson(dto: HistoricalDto): Historical {
        return {
            ...dto,
            since: new Date(dto.since)
        };
    }
}