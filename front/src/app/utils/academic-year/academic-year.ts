interface Format {
  regex: RegExp,
  transform: (captures: string[], value: string) => [number, number];
}

const formats: Format[] = [{
  regex: /^([0-9]{2})[\\/-\\. ]([0-9]{2})$/, // 22/23, 22-23...
  transform: (captures) => {
    const century = Math.floor(new Date().getFullYear() / 100) * 100;
    return [century + Number.parseInt(captures[0]), century + Number.parseInt(captures[1])];
  }
}, {
  regex: /^([0-9]{4})[\\/-\\. ]([0-9]{4})$/, // 2022/2023, 2022-2023...,
  transform: (captures) => {
    return [Number.parseInt(captures[0]), Number.parseInt(captures[1])];
  }
}];

export class AcademicYear {
  private firstYear: number;
  private secondYear: number;

  constructor(firstYear = new Date().getFullYear(), secondYear?: number) {
    if (secondYear && firstYear + 1 != secondYear) {
      throw new Error('Years must be consecutive');
    }

    this.firstYear = secondYear ? firstYear : firstYear - 1;
    this.secondYear = secondYear ?? firstYear;
  }

  startDate(): Date {
    return new Date(this.firstYear, 8, 1);
  }

  endDate(): Date {
    return new Date(this.secondYear, 7, 31);
  }

  mainYear() {
    return this.secondYear;
  }

  includes(date: Date) {
    return this.dateRange()[0] <= date && date < this.dateRange()[1];
  }

  private dateRange() {
    return [new Date(this.firstYear, 8, 1), new Date(this.secondYear, 8, 1)];
  }

  toString(separator = '/') {
    return this.firstYear + separator + this.secondYear;
  }

  static parse(yearAsString: string): AcademicYear {
    const years = formats.map(format => {
      const result = format.regex.exec(yearAsString);

      if (!result || !result[1] || !result[2]) {
        return undefined;
      }

      const transformedResult = format.transform(result.slice(1, 3), yearAsString);

      if (Number.isNaN(transformedResult[0]) || Number.isNaN(transformedResult[1])) {
        return undefined;
      }

      return transformedResult;
    })
      .find(Boolean);
    
    if (!years) {
      throw new Error('Error parsing academic year');
    }

    return new AcademicYear(years[0], years[1]);
  }
}