import { Duration } from './duration';

describe('Duration', () => {
    describe('fromSeconds()', () => {
        [{
            value: 3600,
            hours: 1,
            minutes: 0,
            seconds: 0
        }, {
            value: 7200,
            hours: 2,
            minutes: 0,
            seconds: 0
        }, {
            value: 60,
            hours: 0,
            minutes: 1,
            seconds: 0
        }, {
            value: 5,
            hours: 0,
            minutes: 0,
            seconds: 5
        }, {
            value: 3742,
            hours: 1,
            minutes: 2,
            seconds: 22
        }].forEach(({ value, hours, minutes, seconds }) => {
            it(`returns ${hours}, ${minutes}, ${seconds} for value ${value}`, () => {
                const result = Duration.fromSeconds(value);
                expect(result.hours).toEqual(hours);
                expect(result.minutes).toEqual(minutes);
                expect(result.seconds).toEqual(seconds);
            });
        });
    });

    describe('parse()', () => {
        [{
            value: '1h',
            hours: 1,
            minutes: 0,
            seconds: 0
        }, {
            value: '2h',
            hours: 2,
            minutes: 0,
            seconds: 0
        }, {
            value: '1m',
            hours: 0,
            minutes: 1,
            seconds: 0
        }, {
            value: '5s',
            hours: 0,
            minutes: 0,
            seconds: 5
        }, {
            value: '1h 2m 22s',
            hours: 1,
            minutes: 2,
            seconds: 22
        }, {
            value: '1.5h 30m',
            hours: 2,
            minutes: 0,
            seconds: 0
        }, {
            value: '5430s',
            hours: 1,
            minutes: 30,
            seconds: 30
        }].forEach(({ value, hours, minutes, seconds }) => {
            it(`returns ${hours}, ${minutes}, ${seconds} for value ${value}`, () => {
                const result = Duration.parse(value);
                expect(result.hours).toEqual(hours);
                expect(result.minutes).toEqual(minutes);
                expect(result.seconds).toEqual(seconds);
            });
        });
    });

    describe('equals()', () => {
        [{
            value1: new Duration(0, 0, 0),
            value2: new Duration(0, 0, 0),
            expected: true
        }, {
            value1: new Duration(0, 0, 10),
            value2: new Duration(0, 0, 0),
            expected: false
        }, {
            value1: new Duration(1, 2, 0),
            value2: new Duration(2, 2, 0),
            expected: false
        }, {
            value1: new Duration(0, 1, 0),
            value2: new Duration(0, 3, 0),
            expected: false
        }, {
            value1: new Duration(1, 2, 3),
            value2: new Duration(1, 2, 3),
            expected: true
        }, {
            value1: new Duration(1, 0, 0),
            value2: new Duration(0, 60, 0),
            expected: true
        }].forEach(({ value1, value2, expected }) => {
            it(`returns ${expected} for ${value1.toString()} and ${value2.toString()}`, () => {
                expect(value1.equals(value2)).toEqual(expected);
            });
        });
    });

    describe('compareTo()', () => {
        [{
            value1: new Duration(0, 0, 0),
            value2: new Duration(0, 0, 0),
            expected: 0
        }, {
            value1: new Duration(0, 0, 10),
            value2: new Duration(0, 0, 0),
            expected: 1
        }, {
            value1: new Duration(1, 2, 0),
            value2: new Duration(2, 2, 0),
            expected: -1
        }, {
            value1: new Duration(0, 1, 0),
            value2: new Duration(0, 3, 0),
            expected: -1
        }, {
            value1: new Duration(1, 2, 3),
            value2: new Duration(1, 2, 3),
            expected: 0
        }, {
            value1: new Duration(1, 0, 0),
            value2: new Duration(0, 60, 0),
            expected: 0
        }].forEach(({ value1, value2, expected }) => {
            it(`returns ${expected} for ${value1.toString()} and ${value2.toString()}`, () => {
                expect(value1.compareTo(value2)).toEqual(expected);
            });
        });
    });
});