// Matches a string of the form <x>h <y>m <z>s, where x and y can be whole or decimal numbers (using dot) and z is a whole number.
// The three fields are optional and the separator can be anything except numbers, h, m and s.
const durationStringRegex = /^(\d+(?:\.\d+)?h)?[^\dhms]?(\d+(?:\.\d+)?m)?[^\dhms]?(\d+s)?$/;

const moveDecimalToNextUnit = (decimalValue: number, factor: number) => {
  const whole = Math.floor(decimalValue);
  const decimal = decimalValue - whole;

  return {
    whole,
    passOn: decimal * factor
  };
};

export class Duration {
  private _hours: number;
  private _minutes: number;
  private _seconds: number;

  constructor(hours: number, minutes = 0, seconds = 0) {
    const wholeHours = moveDecimalToNextUnit(hours, 60).whole;
    const totalMinutes = minutes + moveDecimalToNextUnit(hours, 60).passOn;
    
    const wholeMinutes = moveDecimalToNextUnit(totalMinutes, 60).whole;
    const totalSeconds = seconds + moveDecimalToNextUnit(totalMinutes, 60).passOn;

    const wholeSeconds = moveDecimalToNextUnit(totalSeconds, 0).whole;

    this._seconds = wholeSeconds % 60;
    this._minutes = (wholeMinutes + Math.floor(wholeSeconds / 60)) % 60;
    this._hours = wholeHours + Math.floor(wholeSeconds / 3600) + Math.floor(wholeMinutes / 60);
  }

  get hours() {
    return this._hours;
  }

  get minutes() {
    return this._minutes;
  }

  get seconds() {
    return this._seconds;
  }

  get totalSeconds() {
    return this.hours * 3600 + this.minutes * 60 + this.seconds;
  }

  get empty() {
    return this.totalSeconds === 0;
  }

  add(other: Duration): Duration {
    return new Duration(this.hours + other.hours, this.minutes + other.minutes, this.seconds + other.seconds);
  }

  toString() {
    const values = [this._hours, this._minutes, this._seconds];
    const labels = ['h', 'm', 's'];

    const nonZeroIndices = [...Array(values.length).keys()]
      .filter(i => values[i] > 0)
      .map(i => `${values[i]}${labels[i]}`);
    
      if (!nonZeroIndices.length) {
        return '0';
      }

      return nonZeroIndices.reduce((a, b) => `${a} ${b}`);
  }

  equals(other: Duration): boolean {
    return this.totalSeconds === other.totalSeconds;
  }

  compareTo(other: Duration): number {
    return Math.sign(this.totalSeconds - other.totalSeconds);
  }

  static fromSeconds(durationInSeconds: number) {
    return new Duration(0, 0, durationInSeconds);
  }

  static parse(durationString: string) {
    const matches = durationString.match(durationStringRegex);
    
    if (!matches) {
      throw new Error('Invalid format');
    }

    const matchesAsFloat = matches.slice(1)
      .map(value => value?.slice(0, value.length - 1))
      .map(value => value ? Number.parseFloat(value) : undefined);

    const [hours, minutes, seconds] = matchesAsFloat;

    return new Duration(hours ?? 0, minutes ?? 0, seconds ?? 0);
  }
}