import { Musician } from 'src/types';
import { sortMusicians } from './formatting';

describe('sortMusicians', () => {
    const cases: { title: string, musicians: Musician[], expected: Musician['id'][] }[] = [{
        title: 'by family name',
        musicians: [{
            id: 1,
            givenName: 'A',
            familyName: 'B',
            instrument: {
                id: 1,
                name: 'a'
            },
            location: {
                id: 1,
                name: '',
                distance: 0
            }
        }, {
            id: 2,
            givenName: 'A',
            familyName: 'A',
            instrument: {
                id: 1,
                name: 'a'
            },
            location: {
                id: 1,
                name: '',
                distance: 0
            }
        }],
        expected: [2, 1]
    }, {
        title: 'by given name',
        musicians: [{
            id: 1,
            givenName: 'B',
            familyName: 'A',
            instrument: {
                id: 1,
                name: 'a'
            },
            location: {
                id: 1,
                name: '',
                distance: 0
            }
        }, {
            id: 2,
            givenName: 'A',
            familyName: 'A',
            instrument: {
                id: 1,
                name: 'a'
            },
            location: {
                id: 1,
                name: '',
                distance: 0
            }
        }],
        expected: [2, 1]
    }, {
        title: 'by instrument',
        musicians: [{
            id: 1,
            givenName: 'A',
            familyName: 'A',
            instrument: {
                id: 1,
                name: 'a'
            },
            location: {
                id: 1,
                name: '',
                distance: 0
            }
        }, {
            id: 2,
            givenName: 'A',
            familyName: 'A',
            instrument: {
                id: 2,
                name: 'a'
            },
            location: {
                id: 1,
                name: '',
                distance: 0
            }
        }, {
            id: 3,
            givenName: 'A',
            familyName: 'A',
            instrument: {
                id: 1,
                name: 'a'
            },
            location: {
                id: 1,
                name: '',
                distance: 0
            }
        }],
        expected: [1, 3, 2]
    }, {
        title: 'by family name when one is empty',
        musicians: [{
            id: 1,
            givenName: 'A',
            familyName: '',
            instrument: {
                id: 1,
                name: 'a'
            },
            location: {
                id: 1,
                name: '',
                distance: 0
            }
        }, {
            id: 2,
            givenName: 'A',
            familyName: 'A',
            instrument: {
                id: 1,
                name: 'a'
            },
            location: {
                id: 1,
                name: '',
                distance: 0
            }
        }],
        expected: [2, 1]
    }];
    
    cases.forEach(({ title, musicians, expected }) => {
        it(`sorts musicians ${title}`, () => {
            expect(sortMusicians(musicians).map(musician => musician.id)).toEqual(expected);
        });
    });
});