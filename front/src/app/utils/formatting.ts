import { Musician } from 'src/types';

export const parseNumberOrUndefined = (numberRepresentation?: string | null): number | undefined => {
  const numberOrNan = numberRepresentation ? Number.parseFloat(numberRepresentation) : undefined;
  return numberOrNan === undefined || Number.isNaN(numberOrNan) ? undefined : numberOrNan;
};

export const parseNumberOrZero = (n: string) => parseNumberOrUndefined(n) ?? 0;

export const formatDate = (date: Date) => {
  return `${date.getFullYear()}-${(date.getMonth() + 1).toString().padStart(2, '0')}-${date.getDate().toString().padStart(2, '0')}`;
};

const sortStringsIfPresent = (a: string, b: string) => {
  if (!a) {
    return 1;
  }

  if (!b) {
    return -1;
  }

  return a.toLowerCase().localeCompare(b.toLowerCase());
};

export const musicianComparer = (a: Musician, b: Musician): number => {
  return a.instrument.id - b.instrument.id 
      || sortStringsIfPresent(a.familyName, b.familyName)
      || a.givenName.toLowerCase().localeCompare(b.givenName.toLowerCase());
};

export const sortMusicians = (musicians: Musician[]) => {
  return [...musicians].sort(musicianComparer);
};