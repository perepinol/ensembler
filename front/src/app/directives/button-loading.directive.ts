import { ComponentRef, Directive, Input, OnChanges, Renderer2, SimpleChanges, ViewContainerRef } from '@angular/core';
import { MatButton } from '@angular/material/button';
import { MatProgressSpinner } from '@angular/material/progress-spinner';

@Directive({
  // eslint-disable-next-line @angular-eslint/directive-selector
  selector: 'button',
  standalone: true,
  providers: [MatButton]
})
export class ButtonLoadingDirective implements OnChanges {
  @Input() loading = false;

  private spinner?: ComponentRef<MatProgressSpinner>;
  private labelSpan?: Element;

  private button: MatButton;
  private viewContainerRef: ViewContainerRef;
  private renderer: Renderer2;

  constructor(button: MatButton, viewContainerRef: ViewContainerRef, renderer: Renderer2) {
    this.button = button;
    this.viewContainerRef = viewContainerRef;
    this.renderer = renderer;

  }

  ngOnChanges(changes: SimpleChanges): void {
    if (!changes['loading']) {
      return;
    }

    if (changes['loading'].currentValue) {
      if (!this.spinner) {
        this.labelSpan = this.button._elementRef.nativeElement.querySelector('span.mdc-button__label');

        if (this.labelSpan) {
          this.renderer.setStyle(this.labelSpan, 'visibility', 'hidden');

          this.spinner = this.viewContainerRef.createComponent(MatProgressSpinner);
          this.spinner.instance.diameter = 20;
          this.spinner.instance.mode = 'indeterminate';
          this.renderer.setStyle(this.spinner.instance._elementRef.nativeElement, 'position', 'absolute');
          this.renderer.appendChild(this.button._elementRef.nativeElement, this.spinner.instance._elementRef.nativeElement);
        }

        this.button.disabled = true;
      }
    } else {
      this.button.disabled = false;

      this.spinner?.destroy();
      this.spinner = undefined;

      if (this.labelSpan) {
        this.renderer.removeStyle(this.labelSpan, 'visibility');
        this.labelSpan = undefined;
      }
    }
  }
}
