import { Directive, Input } from '@angular/core';
import { FormControl, NG_VALIDATORS, ValidationErrors, Validator } from '@angular/forms';
import { AcademicYear } from '../utils/academic-year/academic-year';
import { dateToString } from '../services/formatters';

@Directive({
  selector: '[appSameYearValidator]',
  standalone: true,
  providers: [{
    provide: NG_VALIDATORS,
    useExisting: SameYearValidatorDirective,
    multi: true
  }]
})
export class SameYearValidatorDirective implements Validator {
  @Input('appSameYearValidator') academicYear: AcademicYear | undefined;

  validate(control: FormControl<Date>): ValidationErrors | null {
    if (!this.academicYear || !control.value) {
      return null;
    }


    if (!this.academicYear.includes(new Date(control.value))) {
      return {
        invalidDate: {
          start: dateToString(this.academicYear.startDate()),
          end: dateToString(this.academicYear.endDate()),
          value: control.value
        }
      };
    }

    return null;
  }
}
