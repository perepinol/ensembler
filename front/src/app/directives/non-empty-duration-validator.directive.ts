import { Directive } from '@angular/core';
import { FormControl, NG_VALIDATORS, ValidationErrors, Validator } from '@angular/forms';
import { Duration } from '../utils/duration/duration';

@Directive({
  selector: '[appNonEmptyDurationValidator]',
  standalone: true,
  providers: [{
    provide: NG_VALIDATORS,
    useExisting: NonEmptyDurationValidatorDirective,
    multi: true
  }]
})
export class NonEmptyDurationValidatorDirective implements Validator {
  validate(control: FormControl<Duration>): ValidationErrors | null {
    if (!control.value || control.value.empty) {
      return {
        empty: true
      };
    }

    return null;
  }
}
