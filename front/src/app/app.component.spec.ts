import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { RouterModule } from '@angular/router';

describe('AppComponent', () => {
  let fixture: ComponentFixture<AppComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [RouterModule.forRoot([])]
    }).compileComponents();
    fixture = TestBed.createComponent(AppComponent);
  });

  it('should create the app', () => {
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });

  it('renders a toolbar', () => {
    expect(fixture.nativeElement.querySelector('mat-toolbar')).toBeTruthy();
  });

  it('renders the application\'s name in the toolbar', () => {
    expect(fixture.nativeElement.querySelector('mat-toolbar').querySelector('h1').textContent).toEqual('Ensembler');
  });

  it('includes navigation to relevant pages in the toolbar', () => {
    const expected = [
      '/',
      '/musician',
      '/project',
      '/location',
      '/historical'
    ];
    fixture.detectChanges();
    expect([...fixture.nativeElement.querySelector('mat-toolbar').querySelectorAll('a').values()].map((element: HTMLElement) => element.getAttribute('href'))).toEqual(expected);
  });
});
