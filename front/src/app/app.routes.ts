import { Routes } from '@angular/router';

export const routes: Routes = [{
  title: 'Musicians',
  loadComponent: () => import('./components/musicians/musicians.component').then(mod => mod.MusiciansComponent),
  path: 'musician',
  pathMatch: 'prefix'
}, {
  title: 'Projects',
  loadComponent: () => import('./components/projects/projects.component').then(mod => mod.ProjectsComponent),
  path: 'project',
  pathMatch: 'prefix'
}, {
  title: 'New project',
  loadComponent: () => import('./components/projects/project-edition/project-edition.component').then(mod => mod.ProjectEditionComponent),
  path: 'project/new',
  pathMatch: 'full'
}, {
  title: 'Project details',
  loadComponent: () => import('./components/projects/project-details/project-details.component').then(mod => mod.ProjectDetailsComponent),
  path: 'project/:id',
  pathMatch: 'full'
}, {
  title: 'Project edition',
  loadComponent: () => import('./components/projects/project-edition/project-edition.component').then(mod => mod.ProjectEditionComponent),
  path: 'project/:id/edit',
  pathMatch: 'full'
}, {
  title: 'Project payout',
  loadComponent: () => import('./components/projects/project-details/payout/payout.component').then(mod => mod.PayoutComponent),
  path: 'project/:id/payout',
  pathMatch: 'full'
}, {
  title: 'Locations',
  loadComponent: () => import('./components/locations/locations.component').then(mod => mod.LocationsComponent),
  path: 'location',
  pathMatch: 'prefix'
}, {
  title: 'New rehearsal',
  loadComponent: () => import('./components/projects/project-details/rehearsal-edition/rehearsal-edition.component').then(mod => mod.RehearsalEditionComponent),
  path: 'project/:projectId/rehearsal',
  pathMatch: 'full'
}, {
  title: 'Rehearsal edition',
  loadComponent: () => import('./components/projects/project-details/rehearsal-edition/rehearsal-edition.component').then(mod => mod.RehearsalEditionComponent),
  path: 'project/:projectId/rehearsal/:id',
  pathMatch: 'full'
}, {
  title: 'Historical values',
  loadComponent: () => import('./components/historical-values/historical-values.component').then(mod => mod.HistoricalValuesComponent),
  path: 'historical',
  pathMatch: 'full'
}, {
  title: 'Home',
  loadComponent: () => import('./components/overview/overview.component').then(mod => mod.OverviewComponent),
  path: '',
  pathMatch: 'full'
}, {
  title: 'Not found redirect',
  path: '**',
  redirectTo: '/'
}];
