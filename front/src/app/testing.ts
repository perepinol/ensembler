import { TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';

export const useParamMap = async (paramMap: Record<string, unknown> = {}, queryParamMap: Record<string, unknown> = {}) => {
  await TestBed.overrideProvider(ActivatedRoute, {
    useValue: {
      snapshot: {
        paramMap: {
          get: (key: string) => paramMap[key]
        },
        queryParamMap: {
            get: (key: string) => queryParamMap[key]
        }
      }
    }
  });
};