import { Component } from '@angular/core';
import { MatTableModule } from '@angular/material/table';
import { HistoricalValueService } from 'src/app/services/historical-value.service';
import { ServiceProvider } from 'src/app/services/serviceProvider';
import { Historical, HistoricalType } from 'src/types';
import { HistoricalValueTableComponent } from './historical-value-table/historical-value-table.component';
import { FlexModule } from '@angular/flex-layout';

@Component({
  selector: 'app-historical-values',
  templateUrl: './historical-values.component.html',
  styleUrl: './historical-values.component.scss',
  standalone: true,
  imports: [MatTableModule, HistoricalValueTableComponent, FlexModule]
})
export class HistoricalValuesComponent {
  private historicalValueService: HistoricalValueService;

  historicalTypes = HistoricalType;
  
  rehearsalPrices: Historical[];
  concertPrices: Historical[];
  mileagePrices: Historical[];

  constructor(serviceProvider: ServiceProvider) {
    this.historicalValueService = serviceProvider.historicalValueService;

    this.rehearsalPrices = [];
    this.concertPrices = [];
    this.mileagePrices = [];
  }

  async handleDeleteRehearsalPrice() {
    await this.historicalValueService.deleteLast(HistoricalType.REHEARSAL);
    this.rehearsalPrices = this.rehearsalPrices.slice(1);
  }

  async handleDeleteConcertPrice() {
    await this.historicalValueService.deleteLast(HistoricalType.CONCERT);
    this.concertPrices = this.concertPrices.slice(1);
  }

  async handleDeleteMileagePrice() {
    await this.historicalValueService.deleteLast(HistoricalType.MILEAGE);
    this.mileagePrices = this.mileagePrices.slice(1);
  }
}
