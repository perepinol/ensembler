import { NgIf } from '@angular/common';
import { Component, Input, OnInit } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatTableModule } from '@angular/material/table';
import { ButtonLoadingDirective } from 'src/app/directives/button-loading.directive';
import { dateToString } from 'src/app/services/formatters';
import { HistoricalValueService } from 'src/app/services/historical-value.service';
import { ServiceProvider } from 'src/app/services/serviceProvider';
import { Historical, HistoricalType } from 'src/types';

interface FormField extends Omit<Historical, 'since'> {
  since: string;
}

@Component({
  selector: 'app-historical-value-table',
  templateUrl: './historical-value-table.component.html',
  styleUrl: './historical-value-table.component.scss',
  standalone: true,
  imports: [MatTableModule, FormsModule, MatFormFieldModule, MatInputModule, MatButtonModule, ButtonLoadingDirective, NgIf, MatIconModule]
})
export class HistoricalValueTableComponent implements OnInit {
  columns = ['start', 'end', 'value', 'actions'];

  @Input()
  type!: HistoricalType;

  @Input()
  ariaLabel = '';

  @Input()
  unit = '';

  values: Historical[];
  addLoading: boolean;
  deleteLoading: boolean;
  _newHistorical: FormField;

  private historicalValueService: HistoricalValueService;

  constructor(serviceProvider: ServiceProvider) {
    this.historicalValueService = serviceProvider.historicalValueService;

    this.values = [];
    this.addLoading = false;
    this.deleteLoading = false;
    this._newHistorical = {
      since: '',
      value: 0
    };
  }

  async ngOnInit() {
    this.values = await this.historicalValueService.getAllByType(this.type);
    this.values.sort((a, b) => b.since.getTime() - a.since.getTime());
  }

  // Assumes that the values are sorted in reverse by date
  getHistoricalValues(values: Historical[]) {
    return values.map((v, i) => ({
      position: i,
      start: dateToString(v.since),
      end: i === 0 ? '-' : dateToString(values[i - 1].since),
      value: Math.round(v.value * 100) / 100
    }));
  }

  async add() {
    this.addLoading = true;
    
    try {
      const newHistorical = await this.historicalValueService.create(this.type, {
        since: new Date(this._newHistorical.since),
        value: this._newHistorical.value
      });

      this.values = [newHistorical, ...this.values];

      this._newHistorical = {
        since: '',
        value: 0
      };
    } catch (error) {
      console.error(error);
    }
   
    this.addLoading = false;
  }

  async delete() {
    this.deleteLoading = true;

    try {
      await this.historicalValueService.deleteLast(this.type);
      this.values = this.values.slice(1);
    } catch (error) {
      console.error(error);
    }

    this.deleteLoading = false;
  }

  getInputMinDate() {
    return this.values.length ? dateToString(this.values[0].since) : undefined;
  }

  isValidInputDate() {
    const minDate = this.getInputMinDate();
    return !minDate || minDate < this._newHistorical.since;
  }
}
