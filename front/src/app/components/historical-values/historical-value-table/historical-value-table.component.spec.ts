import { TestBed } from '@angular/core/testing';

import { HistoricalValueTableComponent } from './historical-value-table.component';
import { screen, render, within } from '@testing-library/angular';
import { UserEvent } from '@testing-library/user-event/dist/types/setup/setup';
import userEvent from '@testing-library/user-event';
import { Historical, HistoricalType } from 'src/types';
import { ServiceProvider } from 'src/app/services/serviceProvider';

const getRows = (s: typeof screen) => s.getAllByRole('row').slice(2);

const values: Historical[] = [{
  since: new Date('2023-02-01'),
  value: 3.335
}, {
  since: new Date('2023-01-02'),
  value: 2
}];

describe('HistoricalValueTableComponent', () => {
  let user: UserEvent;
  let serviceProvider: ServiceProvider;

  [HistoricalType.CONCERT, HistoricalType.MILEAGE].forEach(historicalType => {
    describe(`${historicalType}`, () => {
      beforeEach(async () => {
        user = userEvent.setup();

        serviceProvider = new ServiceProvider();
        spyOn(serviceProvider.historicalValueService, 'create').and.callFake((type: HistoricalType, value: Historical) => Promise.resolve(value));
        spyOn(serviceProvider.historicalValueService, 'deleteLast').and.callFake(() => Promise.resolve(values[0]));
        spyOn(serviceProvider.historicalValueService, 'getAllByType').and.callFake(() => Promise.resolve(values));

        await TestBed.configureTestingModule({
          providers: [{
            provide: ServiceProvider,
            useValue: serviceProvider
          }]
        })
          .compileComponents();

        await render(HistoricalValueTableComponent, {
          componentInputs: {
            type: historicalType,
            ariaLabel: '',
            unit: 'testunit'
          }
        });

        await screen.findAllByRole('cell');
      });

      it('shows the unit in the \'Value\' header', () => {
        const headers = screen.getAllByRole('columnheader').map(element => element.innerText);

        expect(headers.reduce((a, b) => a + ' ' + b)).toContain('testunit');
      });

      it('creates a new row when the add button is clicked', async () => {
        const headers = screen.getAllByRole('columnheader').map(element => element.innerText);
        const valueHeaderIndex = headers.map(text => !!text.match('Value')).indexOf(true);
        const firstRow = screen.getAllByRole('row')[1];
        const cells = within(firstRow).getAllByRole('columnheader');

        const dateInput = within(cells[headers.indexOf('Start')]).getByRole('input');
        const valueInput = within(cells[valueHeaderIndex]).getByRole('spinbutton');

        await user.clear(dateInput);
        await user.type(dateInput, '2023-03-03');
        await user.clear(valueInput);
        await user.type(valueInput, '5');
        // Does not work for some reason
        // await user.click(within(firstRow).getByRole('button', { name: 'Add' }));
        within(firstRow).getByRole('button', { name: 'Add' }).click();
        
        expect(serviceProvider.historicalValueService.create).toHaveBeenCalledWith(historicalType, {
          since: new Date('2023-03-03'),
          value: 5
        });
      });

      it('disables the \'Add\' button if the fields are not filled', async () => {
        await user.clear(screen.getByRole('input'));

        expect(screen.getByRole('button', { name: 'Add' }).getAttribute('disabled')).not.toEqual(null);
      });

      it('disables the \'Add\' button if the input date is smaller than the oldest entry', async () => {
        await user.clear(screen.getByRole('input'));
        await user.type(screen.getByRole('input'), '2021-01-01');

        expect(screen.getByRole('button', { name: 'Add' }).getAttribute('disabled')).not.toEqual(null);
      });

      it('shows dates and numerical values, properly formatted', async () => {
        const rows = getRows(screen);
        const cellTexts = rows.map(row => within(row).getAllByRole('cell').map(cell => cell.innerText).slice(0, -1));

        expect(cellTexts).toContain(['2023-01-02', '2023-02-01', '2']);
        expect(cellTexts).toContain(['2023-02-01', '-', '3.34']);
      });

      it('shows values sorted from newest to oldest', async () => {
        const rows = getRows(screen);
        const startTimestamps = rows.map(row => within(row).getAllByRole('cell')[0].innerText).map(dateString => new Date(dateString).getTime());

        expect(startTimestamps.length).toBeGreaterThan(0);
        expect([...startTimestamps].sort((a, b) => b - a)).toEqual(startTimestamps);
      });

      it('shows a delete button in the first value row', () => {
        const rows = getRows(screen);
        expect(within(rows[0]).queryByRole('button', { name: 'delete' })).toBeTruthy();
        expect(within(rows[1]).queryByRole('button', { name: 'delete' })).toBeFalsy();
      });

      it('emits an event when the delete button is clicked', async () => {
        // Does not work for some reason
        // await user.click(screen.getByRole('button', { name: 'delete' }));
        screen.getByRole('button', { name: 'delete' }).click();
        expect(serviceProvider.historicalValueService.deleteLast).toHaveBeenCalledWith(historicalType);
      });
    });
  });
});
