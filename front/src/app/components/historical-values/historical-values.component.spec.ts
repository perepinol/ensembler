import { TestBed } from '@angular/core/testing';

import { HistoricalValuesComponent } from './historical-values.component';
import { render, screen, within } from '@testing-library/angular';
import { ServiceProvider } from 'src/app/services/serviceProvider';
import { HistoricalType } from 'src/types';

const values = [{
  type: HistoricalType.MILEAGE,
  region: 'Mileage prices',
  unit: '€/km'
}, {
  type: HistoricalType.REHEARSAL,
  region: 'Rehearsal prices',
  unit: '€/h'
}, {
  type: HistoricalType.CONCERT,
  region: 'Concert prices',
  unit: '€'
}];

describe('HistoricalValuesComponent', () => {
  let serviceProvider: ServiceProvider;

  beforeEach(async () => {
    serviceProvider = new ServiceProvider();

    await TestBed.configureTestingModule({
      providers: [{
        provide: ServiceProvider,
        useValue: serviceProvider
      }]
    })
      .compileComponents();
  });

  values.forEach(valueObject => {
    describe(valueObject.region, () => {
      beforeEach(async () => {
        await render(HistoricalValuesComponent);
      });

      it('shows the correct unit in the \'Value\' header', () => {
        const region = screen.getByRole('region', { name: valueObject.region });
        const table = within(region).getByRole('table');
        const headers = within(table).getAllByRole('columnheader').map(element => element.innerText);

        expect(headers.reduce((a, b) => a + ' ' + b)).toContain(`(${valueObject.unit})`);
      });
    });
  });
});
