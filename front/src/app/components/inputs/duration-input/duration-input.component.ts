import { FocusMonitor } from '@angular/cdk/a11y';
import { Component, ElementRef, HostBinding, Input, OnDestroy, Optional, Renderer2, Self } from '@angular/core';
import { NgControl, ControlValueAccessor, FormGroup, FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldControl } from '@angular/material/form-field';
import { Subject } from 'rxjs';
import { Duration } from 'src/app/utils/duration/duration';
import { parseNumberOrZero } from 'src/app/utils/formatting';

@Component({
  selector: 'app-duration-input',
  templateUrl: './duration-input.component.html',
  styleUrls: ['./duration-input.component.scss'],
  standalone: true,
  imports: [ReactiveFormsModule],
  providers: [{ 
    provide: MatFormFieldControl, 
    useExisting: DurationInputComponent 
  }]
})
export class DurationInputComponent implements MatFormFieldControl<Duration>, ControlValueAccessor, OnDestroy {
  static nextId = 0;

  parts: FormGroup;

  private _required = false;
  private _name = '';
  private _placeholder = '';
  private _disabled = false;

  private _focusMonitor;
  private _ref: ElementRef<HTMLDivElement>;
  private _renderer;

  _onTouched: () => void = () => {
    console.log('Not defined yet');
  };

  stateChanges = new Subject<void>;
  id = `duration-input-${DurationInputComponent.nextId++}`;
  ngControl: NgControl;
  focused: boolean;

  controlType = undefined;
  autofilled = undefined;

  @HostBinding('attr.aria-describedby') userAriaDescribedBy = '';
  
  @Input() max?: Duration;

  constructor(formBuilder: FormBuilder, @Optional() @Self() ngControl: NgControl, focusMonitor: FocusMonitor, ref: ElementRef, renderer: Renderer2) {
    this.parts = formBuilder.group({
      hours: '',
      minutes: '',
      seconds: ''
    });

    this.ngControl = ngControl;
    if (this.ngControl != null) {
      this.ngControl.valueAccessor = this;
    }
    this._focusMonitor = focusMonitor;
    this._ref = ref;
    this._renderer = renderer;

    this.focused = false;

    this._focusMonitor.monitor(ref.nativeElement, true).subscribe((origin) => {
      this.focused = !!origin;
      this.stateChanges.next();
    });
  }

  @Input()
  get value(): Duration {
    return new Duration(
      parseNumberOrZero(this.parts.value.hours),
      parseNumberOrZero(this.parts.value.minutes),
      parseNumberOrZero(this.parts.value.seconds)
    );
  }

  set value(duration: Duration | null) {
    this.parts.setValue({
      hours: duration?.hours || '',
      minutes: duration?.minutes || '',
      seconds: duration?.seconds || ''
    });
    this.stateChanges.next();
  }

  @Input()
  get name(): string {
    return this._name;
  }

  set name(name: string) {
    this._name = name;
  }

  @Input()
  get placeholder(): string {
    return this._placeholder;
  }

  set placeholder(name: string) {
    this._placeholder = name;
    this.stateChanges.next();
  }

  get empty(): boolean {
    return this.value.empty;
  }

  get shouldLabelFloat(): boolean {
    return !this.empty || this.focused;
  }

  @Input()
  get required(): boolean {
    return this._required;
  }

  set required(value: boolean | string) {
    if (typeof value === 'boolean') {
      this._required = value;
    } else {
      this._required = true;
    }
    this.stateChanges.next();
  }

  @Input()
  get disabled(): boolean {
    return this._disabled;
  }

  set disabled(value: boolean) {
    this._disabled = value;
  }

  get errorState(): boolean {
    return this.required && this.empty;
  }

  setDescribedByIds(ids: string[]): void {
    this.userAriaDescribedBy = ids.join(' ');
  }

  onContainerClick(event: MouseEvent): void {
    if ((event.target as Element).tagName.toLowerCase() != 'input') {
      this._ref.nativeElement.querySelector('input')?.focus();
    }
  }

  writeValue(value: Duration | null): void {
    this.value = value;
    this._renderer.setProperty(this._ref.nativeElement.querySelectorAll('input')[0], 'value', value?.hours || '');
    this._renderer.setProperty(this._ref.nativeElement.querySelectorAll('input')[1], 'value', value?.minutes || '');
    this._renderer.setProperty(this._ref.nativeElement.querySelectorAll('input')[2], 'value', value?.seconds || '');
  }

  registerOnChange(fn: (value: Duration) => void): void {
    this.parts.valueChanges.subscribe((changes: Record<string, string>) => {
      const values = [changes['hours'], changes['minutes'], changes['seconds']].map(value => parseNumberOrZero(value));
      fn(new Duration(values[0], values[1], values[2]));
    });
  }

  registerOnTouched(fn: () => void): void {
    this._onTouched = () => {
      this.cleanValue();
      fn();
    };
  }

  setDisabledState(isDisabled: boolean): void {
    this._renderer.setProperty(this._ref.nativeElement.querySelectorAll('input')[0], 'disabled', isDisabled);
    this._renderer.setProperty(this._ref.nativeElement.querySelectorAll('input')[1], 'disabled', isDisabled);
    this._renderer.setProperty(this._ref.nativeElement.querySelectorAll('input')[2], 'disabled', isDisabled);
  }

  ngOnDestroy(): void {
    this.stateChanges.complete();
    this._focusMonitor.stopMonitoring(this._ref.nativeElement);
    this._ref.nativeElement.remove();
  }

  preventNonNumbers(event: KeyboardEvent) {
    if (event.ctrlKey || event.altKey) {
      return;
    }

    const input = event.key;

    // These are always valid
    const bypassChars = ['Backspace', 'Enter', 'Tab'];
    if (bypassChars.includes(input)) {
      return;
    }

    // These change focus to the correct input
    const index = ['h', 'm', 's'].indexOf(input);
    if (index >= 0) {
      event.preventDefault();

      // If the letter matches the input, advance to the next section
      if (index < 2 && this._ref.nativeElement.querySelectorAll('input').item(index) === document.activeElement) {
        this._ref.nativeElement.querySelectorAll('input')[index + 1].focus();
        return;
      }

      this._ref.nativeElement.querySelectorAll('input')[index].focus();
    }

    // The first dot and all numbers are valid
    if (input !== '.' && Number.isNaN(Number.parseInt(input))) {
      event.preventDefault();
      return;
    }

    const existingText = (event.target as HTMLInputElement).getAttribute('formControlName') ?? '';
    const prevValue = this.parts.value[existingText];

    if (!prevValue) {
      return;
    }

    const pattern = /^\d*\.?\d*$/;
    if (!`${prevValue}${input}`.match(pattern)) {
      event.preventDefault();
    }
  }

  cleanValue() {
    let v = this.value; // This does the normalisation already
    
    if (this.max && v.compareTo(this.max) > 0) {
      v = this.max;
    }
    
    this.value = v;
  }
}
