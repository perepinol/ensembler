import { TestBed } from '@angular/core/testing';

import { DurationInputComponent } from './duration-input.component';
import { render, screen } from '@testing-library/angular';
import { Duration } from 'src/app/utils/duration/duration';
import userEvent from '@testing-library/user-event';
import { UserEvent } from '@testing-library/user-event/dist/types/setup/setup';

describe('DurationInputComponent', () => {
  let user: UserEvent;

  beforeEach(async () => {
    user = userEvent.setup();
    await TestBed.configureTestingModule({
    })
      .compileComponents();
  });

  it('renders empty inputs for initial values that are zero', async () => {
    await render(DurationInputComponent, { componentProperties: {
      value: new Duration(0, 10, 0)
    }});

    expect(screen.getAllByRole<HTMLInputElement>('textbox').map(e => e.value)).toEqual(['', '10', '']);
  });

  it('does not allow values other than numbers and one \'.\'', async () => {
    await render(DurationInputComponent, { componentProperties: {
      value: new Duration(0, 0, 0)
    }});

    await user.type(screen.getAllByRole('textbox')[0], 'abc');
    expect(screen.queryByDisplayValue('abc')).toBeFalsy();
    await user.type(screen.getAllByRole('textbox')[1], '123.0');
    expect(screen.queryByDisplayValue('123.0')).toBeTruthy();
    await user.type(screen.getAllByRole('textbox')[2], '127.0.0.1');
    expect(screen.queryByDisplayValue('127.0.0.1')).toBeFalsy();

    await user.clear(screen.getAllByRole('textbox')[2]);
    await user.type(screen.getAllByRole('textbox')[2], '127.0');
    expect(screen.queryByDisplayValue('127.0')).toBeTruthy();
  });

  it('rearranges duration values on blur', async () => {
    await render(DurationInputComponent, { componentProperties: {
      value: new Duration(0, 0, 0)
    }});

    await user.type(screen.getAllByRole('textbox')[0], '1.5');
    await user.tab();
    
    expect(screen.queryByDisplayValue('1')).toBeTruthy();
    expect(screen.queryByDisplayValue('30')).toBeTruthy();
  });
  
  it('sets the values to the maximum on blur if maximum is exceeded', async () => {
    await render(DurationInputComponent, { componentProperties: {
      value: new Duration(0, 0, 0),
      max: new Duration(5, 15, 10)
    }});

    await user.type(screen.getAllByRole('textbox')[0], '4');
    await user.type(screen.getAllByRole('textbox')[1], '120');
    await user.type(screen.getAllByRole('textbox')[2], '0');
    await user.tab();
    
    expect(screen.queryByDisplayValue('5')?.id).toEqual(screen.getAllByRole('textbox')[0].id);
    expect(screen.queryByDisplayValue('15')?.id).toEqual(screen.getAllByRole('textbox')[1].id);
    expect(screen.queryByDisplayValue('10')?.id).toEqual(screen.getAllByRole('textbox')[2].id);
  });
});
