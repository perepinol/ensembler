import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AcademicYearInputComponent } from './academic-year-input.component';

describe('AcademicYearInputComponent', () => {
  let component: AcademicYearInputComponent;
  let fixture: ComponentFixture<AcademicYearInputComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
    })
    .compileComponents();

    fixture = TestBed.createComponent(AcademicYearInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
