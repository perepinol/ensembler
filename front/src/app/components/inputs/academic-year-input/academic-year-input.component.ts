import { FocusMonitor } from '@angular/cdk/a11y';
import { Component, ElementRef, HostBinding, Input, OnDestroy, Optional, Renderer2, Self, TemplateRef, ViewChild } from '@angular/core';
import { ControlValueAccessor, FormsModule, NgControl, NgModel } from '@angular/forms';
import { MatFormFieldControl } from '@angular/material/form-field';
import { Subject } from 'rxjs';
import { AcademicYear } from 'src/app/utils/academic-year/academic-year';

@Component({
  selector: 'app-academic-year-input',
  templateUrl: './academic-year-input.component.html',
  styleUrls: ['./academic-year-input.component.scss'],
  standalone: true,
  imports: [FormsModule],
  providers: [{
    provide: MatFormFieldControl,
    useExisting: AcademicYearInputComponent
  }]
})
export class AcademicYearInputComponent implements MatFormFieldControl<AcademicYear>, ControlValueAccessor, OnDestroy {
  static nextId = 0;

  stringValue: string;

  private _required = false;
  private _name = '';
  private _placeholder = '';
  private _disabled = false;

  private _focusMonitor;
  private _ref: ElementRef<HTMLDivElement>;
  private _renderer;
  private _errorState = false;

  _onTouched: () => void = () => {
    console.log('Not defined yet');
  };

  stateChanges = new Subject<void>;
  id = `duration-input-${AcademicYearInputComponent.nextId++}`;
  ngControl: NgControl;
  focused: boolean;

  controlType = undefined;
  autofilled = undefined;

  @HostBinding('attr.aria-describedby') userAriaDescribedBy = '';

  @ViewChild('control') control: NgModel | undefined;
  @ViewChild('template', { static: true }) template!: TemplateRef<HTMLElement>;

  constructor(@Optional() @Self() ngControl: NgControl, focusMonitor: FocusMonitor, ref: ElementRef, renderer: Renderer2) {
    this.stringValue = '';
    this.ngControl = ngControl;
    if (this.ngControl != null) {
      this.ngControl.valueAccessor = this;
    }
    this._focusMonitor = focusMonitor;
    this._ref = ref;
    this._renderer = renderer;

    this.focused = false;

    this._focusMonitor.monitor(ref.nativeElement, true).subscribe((origin) => {
      this.focused = !!origin;
      this.stateChanges.next();
    });
  }

  @Input()
  get value(): AcademicYear | null {
    try {
      return AcademicYear.parse(this.stringValue);
    } catch {
      return null;
    }
  }

  set value(academicYear: AcademicYear | null) {
    this.stringValue = academicYear?.toString() ?? '';
    this.stateChanges.next();
  }

  @Input()
  get name(): string {
    return this._name;
  }

  set name(name: string) {
    this._name = name;
  }

  @Input()
  get placeholder(): string {
    return this._placeholder;
  }

  set placeholder(name: string) {
    this._placeholder = name;
    this.stateChanges.next();
  }

  get empty(): boolean {
    return false;
  }

  get shouldLabelFloat(): boolean {
    return !this.empty || this.focused;
  }

  @Input()
  get required(): boolean {
    return this._required;
  }

  set required(value: boolean | string) {
    if (typeof value === 'boolean') {
      this._required = value;
    } else {
      this._required = true;
    }
    this.stateChanges.next();
  }

  @Input()
  get disabled(): boolean {
    return this._disabled;
  }

  set disabled(value: boolean) {
    this._disabled = value;
    this.stateChanges.next();
  }

  get errorState(): boolean {
    return this._errorState;
  }

  set errorState(value: boolean) {
    this._errorState = value;
    this.stateChanges.next();
  }

  setDescribedByIds(ids: string[]): void {
    this.userAriaDescribedBy = ids.join(' ');
  }

  onContainerClick(event: MouseEvent): void {
    if ((event.target as Element).tagName.toLowerCase() != 'input') {
      this._ref.nativeElement.querySelector('input')?.focus();
    }
  }

  writeValue(value: AcademicYear | null): void {
    this._renderer.setProperty(this._ref.nativeElement.querySelector('input'), 'value', value?.toString() ?? '');
  }

  registerOnChange(fn: (value: AcademicYear | null) => void): void {
    this.control?.valueChanges?.subscribe((change: string) => {
      try {
        fn(AcademicYear.parse(change));
        this.errorState = false;
      } catch {
        this.errorState = (this.control?.touched || this.control?.dirty) ?? true;
        this.ngControl.control?.setErrors({ invalidFormat: {
          value: change
        } });
        return;
      }
    });
  }

  registerOnTouched(fn: () => void): void {
    this._onTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this._renderer.setProperty(this._ref.nativeElement.querySelector('input'), 'disabled', isDisabled);
  }

  ngOnDestroy(): void {
    this.stateChanges.complete();
    this._focusMonitor.stopMonitoring(this._ref.nativeElement);
    this._ref.nativeElement.remove();
  }
}
