import { TestBed } from '@angular/core/testing';

import { ProjectsComponent } from './projects.component';
import { render, screen, waitFor, within } from '@testing-library/angular';
import { MatDialog } from '@angular/material/dialog';
import { ServiceProvider } from 'src/app/services/serviceProvider';
import { Project } from 'src/types';
import { AcademicYear } from 'src/app/utils/academic-year/academic-year';
import { UserEvent } from '@testing-library/user-event/dist/types/setup/setup';
import userEvent from '@testing-library/user-event';

describe('ProjectsComponent', () => {
  let user: UserEvent;
  beforeEach(async () => {
    user = userEvent.setup();

    const serviceProvider = new ServiceProvider();
    spyOn(serviceProvider.projectService, 'getAll').and.callFake((): Promise<Project[]> => {
      return Promise.resolve([{
        id: 1,
        name: 'Test',
        year: new AcademicYear(2023),
        conductor: 'Testy McTestface',
        participants: []
      }]);
    });

    await TestBed.configureTestingModule({
      providers: [MatDialog, {
        provide: ServiceProvider,
        useValue: serviceProvider
      }]
    })
    .compileComponents();

    await render(ProjectsComponent);
  });

  it('renders a table with headers \'Name\', \'Year\' and \'Conductor\'', async () => {
    expect(screen.queryByRole('table')).toBeTruthy();
    expect(screen.getAllByRole('columnheader').map(cell => cell.innerText)).toEqual(jasmine.arrayContaining(['Name', 'Year', 'Conductor']));

    await waitFor(() => {
      expect(within(screen.getAllByRole('row')[1]).getAllByRole('cell').map(cell => cell.innerText)).toEqual(jasmine.arrayContaining([
        'Test',
        '2022/2023',
        'Testy McTestface'
      ]));
    });
  });

  it('opens a delete dialog when clicking on the delete button', async () => {
    const deleteButton = await screen.findByRole('button', { name: 'delete' });
    await user.click(deleteButton);

    await waitFor(() => {
      expect(screen.queryByRole('dialog')).toBeTruthy();
    });
  });
});
