import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ProjectService } from 'src/app/services/project.service';
import { ServiceProvider } from 'src/app/services/serviceProvider';
import { Project } from 'src/types';
import { ConfirmationDialogComponent } from '../confirmation-dialog/confirmation-dialog.component';
import { MatTableModule } from '@angular/material/table';
import { MatIconModule } from '@angular/material/icon';
import { RouterModule } from '@angular/router';
import { MatButtonModule } from '@angular/material/button';
import { FlexModule } from '@angular/flex-layout';

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  standalone: true,
  imports: [MatTableModule, RouterModule, MatIconModule, MatButtonModule, FlexModule]
})
export class ProjectsComponent implements OnInit {
  columns = ['name', 'year', 'conductor', 'actions'];
  projects: Project[];

  private dialog: MatDialog;
  private projectService: ProjectService;

  constructor(dialog: MatDialog, serviceProvider: ServiceProvider) {
    this.projects = [];

    this.dialog = dialog;
    this.projectService = serviceProvider.projectService;
  }

  async ngOnInit(): Promise<void> {
    this.projects = await this.projectService.getAll();
  }

  toggleDeleteDialog(project: Project) {
    this.dialog.open(ConfirmationDialogComponent, { data: {
      title: 'Delete project',
      actionText: `delete project ${project.name} (${project.year})`,
      actionButtonText: 'Delete',
      onConfirm: async () => {
        await this.projectService.delete(project);
      }
    }}).afterClosed().subscribe(booleanOrError => {
      if (booleanOrError === undefined) {
        return;
      }
      
      if (typeof booleanOrError !== 'boolean') {
        console.error(booleanOrError);
      }
      
      if (booleanOrError !== true) {
        return;
      }

      this.projects = this.projects.filter(p => p.id !== project.id);
    });
  }
}
