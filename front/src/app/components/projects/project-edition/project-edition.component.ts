import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MusicianService } from 'src/app/services/musician.service';
import { ProjectService } from 'src/app/services/project.service';
import { ServiceProvider } from 'src/app/services/serviceProvider';
import { parseNumberOrUndefined, sortMusicians } from 'src/app/utils/formatting';
import { Musician, New, Project } from 'src/types';
import { AcademicYear } from 'src/app/utils/academic-year/academic-year';
import { EditPage } from '../../generic/edit-page-base/edit-page.component';
import { FormsModule } from '@angular/forms';
import { EditPageComponent } from '../../generic/edit-page/edit-page.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatTableModule } from '@angular/material/table';
import { NgIf, TitleCasePipe } from '@angular/common';
import { AcademicYearInputComponent } from '../../inputs/academic-year-input/academic-year-input.component';
import { MatCheckboxModule } from '@angular/material/checkbox';

@Component({
  selector: 'app-project-edition',
  templateUrl: './project-edition.component.html',
  styleUrls: ['./project-edition.component.scss'],
  standalone: true,
  imports: [FormsModule, EditPageComponent, MatFormFieldModule, MatInputModule, MatTableModule, NgIf, AcademicYearInputComponent, MatCheckboxModule, TitleCasePipe]
})
export class ProjectEditionComponent extends EditPage<Project> implements OnInit {
  columns = ['selected', 'givenName', 'familyName', 'instrument'];

  private duplicateSourceProjectId?: Project['id'];

  private projectService: ProjectService;
  private musicianService: MusicianService;

  musicians: Musician[];

  constructor(router: Router, route: ActivatedRoute, serviceProvider: ServiceProvider) {
    super(router, route);

    // Parse query param
    this.duplicateSourceProjectId = this.modelId === undefined ? parseNumberOrUndefined(route.snapshot.queryParamMap.get('from')) : undefined;

    this.projectService = serviceProvider.projectService;
    this.musicianService = serviceProvider.musicianService;

    this.musicians = [];
  }

  async ngOnInit(): Promise<void> {
    this.musicians = sortMusicians(await this.musicianService.getAll());

    const projectIdToFetch = this.modelId ?? this.duplicateSourceProjectId;

    if (projectIdToFetch === undefined) {
      return;
    }

    const project = await this.projectService.get(projectIdToFetch);
    if (!project) {
      throw new Error(`Project ${this.modelId} not found`);
    }
    if (this.modelId !== undefined) {
      this.model = project;
    } else {
      delete (project as Partial<Project>).id; // To avoid unused variables
      this.model = project;
    }
  }

  override getDefaultModel(): New<Project> {
    return {
      name: '',
      year: new AcademicYear(),
      conductor: '',
      participants: []
    };
  }

  getModelName(): string {
    return 'project';
  }

  override create(model: New<Project>): Promise<Project> {
    return this.projectService.create(model);
  }
  override update(model: Project): Promise<Project> {
    return this.projectService.update(model);
  }

  isMusicianSelected(musician: Musician): boolean {
    return this.model.participants.map(participant => participant.id).includes(musician.id);
  }

  isSomeMusicianSelected(): boolean {
    return this.model.participants.length > 0;
  }

  areAllMusiciansSelected(): boolean {
    return this.model.participants.length === this.musicians.length;
  }

  toggleAllMusicians() {
    if (this.areAllMusiciansSelected()) {
      this.model.participants = [];
    } else {
      this.model.participants = this.musicians;
    }
  }

  toggleMusician(musician: Musician) {
    if (this.isMusicianSelected(musician)) {
      this.model.participants = this.model.participants.filter(participant => participant.id !== musician.id);
      return;
    }

    this.model.participants = [...this.model.participants, musician];
  }
}
