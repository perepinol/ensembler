import { TestBed } from '@angular/core/testing';

import { ProjectEditionComponent } from './project-edition.component';
import { render, screen, waitFor, waitForElementToBeRemoved, within } from '@testing-library/angular';
import userEvent from '@testing-library/user-event';
import { UserEvent } from '@testing-library/user-event/dist/types/setup/setup';
import { New, Project } from 'src/types';
import { ServiceProvider } from 'src/app/services/serviceProvider';
import { Router } from '@angular/router';
import { AcademicYear } from 'src/app/utils/academic-year/academic-year';
import { useParamMap } from 'src/app/testing';

describe('ProjectEditionComponent', () => {
  const serviceProvider = new ServiceProvider();

  let user: UserEvent;
  let navigateSpy: jasmine.Spy;

  beforeEach(async () => {
    user = userEvent.setup();
    navigateSpy = jasmine.createSpy('navigate');

    spyOn(serviceProvider.projectService, 'create').and.callFake((project: New<Project>) => {
      return new Promise(resolve => setTimeout(() => resolve({ ...project, id: 1 }), 10));
    });
    spyOn(serviceProvider.projectService, 'get').and.callFake((id: number) => {
      return Promise.resolve({
        id,
        name: 'Blah',
        year: new AcademicYear(2023),
        conductor: 'Bluh',
        participants: []
      });
    });
    spyOn(serviceProvider.projectService, 'update').and.callFake((project: Project) => {
      return new Promise(resolve => setTimeout(() => resolve(project), 10));
    });

    spyOn(serviceProvider.musicianService, 'getAll').and.callFake(() => {
      return Promise.resolve([{
        id: 1,
        givenName: 'Musician',
        familyName: 'First',
        instrument: {
          id: 1,
          name: 'bagpipes'
        },
        location: {
          id: 1,
          name: 'Test',
          distance: 1
        }
      }, {
        id: 2,
        givenName: 'Musician',
        familyName: 'Second',
        instrument: {
          id: 1,
          name: 'viola'
        },
        location: {
          id: 1,
          name: 'Test',
          distance: 1
        }
      }]);
    });

    await TestBed.configureTestingModule({
      providers: [{
        provide: Router,
        useValue: {
          navigate: navigateSpy
        }
      }, {
        provide: ServiceProvider,
        useValue: serviceProvider
      }]
    })
      .compileComponents();
    await useParamMap({
      id: 1
    });
  });

  describe('Common', () => {
    beforeEach(async () => {
      await render(ProjectEditionComponent);
    });

    it('navigates back when the Cancel button is clicked', () => {
      screen.getByRole('button', { name: 'Cancel' }).click();

      expect(navigateSpy).toHaveBeenCalledTimes(1);
      expect(navigateSpy).toHaveBeenCalledWith(['..'], { relativeTo: jasmine.anything() });
    });

    it('renders the Submit button as disabled if the form is not filled', async () => {
      await user.clear(screen.getByLabelText('Name'));

      await waitFor(() => {
        expect(screen.getByRole('button', { name: 'Submit' }).getAttribute('disabled')).toEqual('true');
      });
    });

    it('renders the Submit button as disabled if the year field is invalid', async () => {
      await user.type(screen.getByLabelText('Name'), 'A');
      await user.clear(screen.getByLabelText('Year'));
      await user.type(screen.getByLabelText('Year'), '01/03');
      await user.type(screen.getByLabelText('Conductor'), 'C');

      expect(screen.getByRole('button', { name: 'Submit' }).getAttribute('disabled')).toEqual('true');
    });

    it('renders a table containing all musicians with checkboxes for each of them', async () => {
      const musicians = await serviceProvider.musicianService.getAll();

      const table = screen.getByRole('table');

      await within(table).findAllByRole('cell');

      const musicianRows = within(table).getAllByRole('row').slice(1);

      expect(musicianRows.length).toEqual(musicians.length);
      musicianRows.forEach(row => {
        expect(within(row).queryByRole('checkbox')).toBeTruthy();
      });
    });
  });

  describe('Create mode', () => {
    beforeEach(async () => {
      await useParamMap({
        id: 'new'
      });
      await render(ProjectEditionComponent);
    });

    it('should create a create header', async () => {
      expect(screen.getAllByRole('heading').map(h => h.innerText)).toContain('Create project');
    });

    it('calls ProjectService.create when submitting the form', async () => {
      await user.type(screen.getByLabelText('Name'), 'A');
      await user.clear(screen.getByLabelText('Year'));
      await user.type(screen.getByLabelText('Year'), '2022/2023');
      await user.type(screen.getByLabelText('Conductor'), 'C');

      screen.getByRole('button', { name: 'Submit' }).click();

      const progressBar = await screen.findByRole('progressbar');
      await waitForElementToBeRemoved(progressBar);

      await waitFor(() => {
        expect(serviceProvider.projectService.create).toHaveBeenCalledTimes(1);
        expect(navigateSpy).toHaveBeenCalledTimes(1);
        expect(navigateSpy).toHaveBeenCalledWith(['..'], { relativeTo: jasmine.anything() });
      });
    });
  });

  describe('Duplicate mode', () => {
    beforeEach(async () => {
      await useParamMap({
        id: 'new'
      }, {
        from: 5
      });
      await render(ProjectEditionComponent);
    });

    it('prefills dialog information if the query parameter \'from\' is given', async () => {
      expect(screen.getAllByRole('heading').map(h => h.innerText)).toContain('Create project');

      await waitFor(() => {
        expect(screen.getByDisplayValue('Blah').id).toEqual(screen.getByLabelText('Name').id);
        expect(screen.getByDisplayValue('2022/2023').id).toEqual(screen.getByLabelText('Year').id);
        expect(screen.getByDisplayValue('Bluh').id).toEqual(screen.getByLabelText('Conductor').id);
      });
    });
  });

  describe('Edit mode', () => {
    beforeEach(async () => {
      await render(ProjectEditionComponent);
    });

    it('should create an edit header', async () => {
      expect(screen.getAllByRole('heading').map(h => h.innerText)).toContain('Update project');

      await waitFor(() => {
        expect(screen.getByDisplayValue('Blah').id).toEqual(screen.getByLabelText('Name').id);
        expect(screen.getByDisplayValue('2022/2023').id).toEqual(screen.getByLabelText('Year').id);
        expect(screen.getByDisplayValue('Bluh').id).toEqual(screen.getByLabelText('Conductor').id);
      });

    });

    it('calls ProjectService.update when submitting the form', async () => {
      await user.clear(screen.getByLabelText('Name'));
      await user.clear(screen.getByLabelText('Year'));
      await user.clear(screen.getByLabelText('Conductor'));

      await user.type(screen.getByLabelText('Name'), 'A');
      await user.type(screen.getByLabelText('Year'), '2000/2001');
      await user.type(screen.getByLabelText('Conductor'), 'C');

      screen.getByRole('button', { name: 'Submit' }).click();

      const progressBar = await screen.findByRole('progressbar');
      await waitForElementToBeRemoved(progressBar);

      await waitFor(() => {
        expect(serviceProvider.projectService.update).toHaveBeenCalledTimes(1);
        expect(navigateSpy).toHaveBeenCalledTimes(1);
        expect(navigateSpy).toHaveBeenCalledWith(['..'], { relativeTo: jasmine.anything() });
      });
    });
  });
});
