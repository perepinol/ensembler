import { Component, Inject } from '@angular/core';
import { New, Project, Rehearsal } from 'src/types';
import { ServiceProvider } from 'src/app/services/serviceProvider';
import { RehearsalService } from 'src/app/services/rehearsal.service';
import { formatDate } from 'src/app/utils/formatting';
import { Duration } from 'src/app/utils/duration/duration';
import { AcademicYear } from 'src/app/utils/academic-year/academic-year';
import { MAT_DIALOG_DATA, MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { EditDialog } from 'src/app/components/generic/edit-page-base/edit-dialog.component';
import { FormsModule } from '@angular/forms';
import { NgIf, TitleCasePipe } from '@angular/common';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { SameYearValidatorDirective } from 'src/app/directives/same-year-validator.directive';
import { ButtonLoadingDirective } from 'src/app/directives/button-loading.directive';
import { MatButtonModule } from '@angular/material/button';
import { DurationInputComponent } from 'src/app/components/inputs/duration-input/duration-input.component';
import { NonEmptyDurationValidatorDirective } from 'src/app/directives/non-empty-duration-validator.directive';

type NonFormFields = Pick<Rehearsal, 'attendees' | 'project'>;

interface FormModel extends Omit<Rehearsal, 'date' | 'attendees' | 'project'> {
  date: string;
}

interface DialogData {
  rehearsal?: Rehearsal;
  project: Project;
}

const toFormModel = (rehearsal: Rehearsal): FormModel => {
  const { date, duration, id } = rehearsal;
  return {
    id,
    date: formatDate(date),
    duration
  };
};

const fromFormModel = (formModel: FormModel | New<FormModel>, otherFields: NonFormFields): Rehearsal | New<Rehearsal> => {
  const { date, ...other } = formModel;

  return {
    ...other,
    ...otherFields,
    date: new Date(date)
  };
};

@Component({
  selector: 'app-rehearsal-edition',
  templateUrl: './rehearsal-edition.component.html',
  styleUrls: ['./rehearsal-edition.component.scss'],
  standalone: true,
  imports: [FormsModule, TitleCasePipe, MatFormFieldModule, MatInputModule, SameYearValidatorDirective, NgIf, ButtonLoadingDirective, MatButtonModule, DurationInputComponent, NonEmptyDurationValidatorDirective, MatDialogModule]
})
export class RehearsalEditionComponent extends EditDialog<FormModel> {
  private constantFields: NonFormFields;

  private rehearsalService: RehearsalService;

  constructor(dialogRef: MatDialogRef<RehearsalEditionComponent>, @Inject(MAT_DIALOG_DATA) data: DialogData, serviceProvider: ServiceProvider) {
    super(dialogRef, data.rehearsal && toFormModel(data.rehearsal));

    this.rehearsalService = serviceProvider.rehearsalService;

    if (data.rehearsal && data.project.id !== data.rehearsal.project.id) {
      throw new Error('Rehearsal and project do not match');
    }

    this.constantFields = {
      project: data.project,
      attendees: data.rehearsal?.attendees ?? []
    };

    if (data.rehearsal) {
      this.model = toFormModel(data.rehearsal);
    }
  }

  override getDefaultModel(): New<FormModel> {
    return {
      date: '',
      duration: Duration.fromSeconds(0)
    };
  }
  
  override async create(model: New<FormModel>): Promise<FormModel> {
    return toFormModel(await this.rehearsalService.create(fromFormModel(model, this.constantFields))) as FormModel;
  }

  override async update(model: FormModel): Promise<FormModel> {
    return toFormModel(await this.rehearsalService.update(fromFormModel(model, this.constantFields) as Rehearsal)) as FormModel;
  }

  getAcademicYearForProject() {
    return this.constantFields.project.year ?? new AcademicYear(new Date().getFullYear());
  }

  override close(newRehearsal?: FormModel): void {
    super.close(newRehearsal && (fromFormModel(newRehearsal, this.constantFields) as Rehearsal));
  }
}
