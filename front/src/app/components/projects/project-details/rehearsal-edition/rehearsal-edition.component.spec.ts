import { TestBed } from '@angular/core/testing';

import { RehearsalEditionComponent } from './rehearsal-edition.component';
import { render, screen, waitFor, waitForElementToBeRemoved } from '@testing-library/angular';
import userEvent from '@testing-library/user-event';
import { UserEvent } from '@testing-library/user-event/dist/types/setup/setup';
import { ServiceProvider } from 'src/app/services/serviceProvider';
import { New, Project, Rehearsal } from 'src/types';
import { AcademicYear } from 'src/app/utils/academic-year/academic-year';
import { Duration } from 'src/app/utils/duration/duration';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

const project: Project = {
  id: 1,
  name: 'Test project',
  year: new AcademicYear(2023),
  conductor: '',
  participants: []
};

describe('RehearsalEditionComponent', () => {
  const serviceProvider = new ServiceProvider();

  let user: UserEvent;
  let closeSpy: jasmine.Spy;

  beforeEach(async () => {
    user = userEvent.setup();
    closeSpy = jasmine.createSpy('close');

    spyOn(serviceProvider.rehearsalService, 'create').and.callFake((rehearsal: New<Rehearsal>): Promise<Rehearsal> => {
      return new Promise(resolve => setTimeout(() => resolve({
        id: 1,
        ...rehearsal
      }), 10));
    });

    spyOn(serviceProvider.rehearsalService, 'update').and.callFake((rehearsal: Rehearsal): Promise<Rehearsal> => {
      return new Promise(resolve => setTimeout(() => resolve(rehearsal), 10));
    });

    await TestBed.configureTestingModule({
      providers: [{
        provide: MAT_DIALOG_DATA,
        useValue: {
          rehearsal: {
            id: 1,
            date: new Date('2023-05-01'),
            attendees: [],
            project,
            duration: Duration.fromSeconds(10)
          },
          project
        }
      }, {
        provide: MatDialogRef,
        useValue: {
          close: closeSpy
        }
      }, {
        provide: ServiceProvider,
        useValue: serviceProvider
      }]
    })
      .compileComponents();
  });

  describe('Common', () => {
    beforeEach(async () => {
      await render(RehearsalEditionComponent);
    });

    it('navigates back when the Cancel button is clicked', () => {
      screen.getByRole('button', { name: 'Cancel' }).click();

      expect(closeSpy).toHaveBeenCalledTimes(1);
    });

    it('renders the Submit button as disabled if the form is not filled', async () => {
      await user.clear(screen.getByLabelText('Date'));

      expect(screen.getByRole('button', { name: 'Submit' }).getAttribute('disabled')).toEqual('true');
    });

    it('renders the Submit button as disabled if the duration is zero', async () => {
      const getActiveElementOrThrow = (document: Document) => {
        if (!document.activeElement) throw new Error('No active element');
        return document.activeElement;
      };
      
      await user.clear(screen.getByLabelText('Duration'));
      await user.tab();
      await user.clear(getActiveElementOrThrow(document));
      await user.tab();
      await user.clear(getActiveElementOrThrow(document));

      expect(screen.getByRole('button', { name: 'Submit' }).getAttribute('disabled')).toEqual('true');
    });
  });

  describe('Create mode', () => {
    beforeEach(async () => {
      await TestBed.overrideProvider(MAT_DIALOG_DATA, {
        useValue: {
          rehearsal: undefined,
          project
        }
      });
      await render(RehearsalEditionComponent);
    });

    it('should create a create header', async () => {
      expect(screen.getAllByRole('heading').map(h => h.innerText)).toContain('Create rehearsal');
    });

    it('calls create when submitting the form', async () => {
      await user.clear(screen.getByLabelText('Date'));
      await user.type(screen.getByLabelText('Date'), '2023-01-01');
      await user.clear(screen.getByLabelText('Duration'));
      await user.type(screen.getByLabelText('Duration'), '2');

      screen.getByRole('button', { name: 'Submit' }).click();

      const progressBar = await screen.findByRole('progressbar');
      await waitForElementToBeRemoved(progressBar);

      await waitFor(() => {
        expect(serviceProvider.rehearsalService.create).toHaveBeenCalledTimes(1);
        expect(closeSpy).toHaveBeenCalledTimes(1);
      });
    });
  });

  describe('Edit mode', () => {
    beforeEach(async () => {
      await render(RehearsalEditionComponent);
    });

    it('should create an edit header and have the correct date', async () => {
      expect(screen.getAllByRole('heading').map(h => h.innerText)).toContain('Edit rehearsal');

      await waitFor(() => {
        expect(screen.getByDisplayValue('2023-05-01').id).toEqual(screen.getByLabelText('Date').id);
      });

    });

    it('calls update when submitting the form', async () => {
      await user.clear(screen.getByLabelText('Date'));
      await user.type(screen.getByLabelText('Date'), '2023-01-01');

      screen.getByRole('button', { name: 'Submit' }).click();

      const progressBar = await screen.findByRole('progressbar');
      await waitForElementToBeRemoved(progressBar);

      await waitFor(() => {
        expect(serviceProvider.rehearsalService.update).toHaveBeenCalledTimes(1);
        expect(closeSpy).toHaveBeenCalledTimes(1);
      });
    });
  });
});
