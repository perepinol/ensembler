import { TestBed } from '@angular/core/testing';

import { ProjectDetailsComponent } from './project-details.component';
import { render, screen, waitFor, within } from '@testing-library/angular';
import { ServiceProvider } from 'src/app/services/serviceProvider';
import { AcademicYear } from 'src/app/utils/academic-year/academic-year';
import { useParamMap } from 'src/app/testing';
import { UserEvent } from '@testing-library/user-event/dist/types/setup/setup';
import userEvent from '@testing-library/user-event';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { Concert, Historical, HistoricalType, Musician, Project, Rehearsal } from 'src/types';
import { Duration } from 'src/app/utils/duration/duration';

const project: Project = {
  id: 1,
  name: 'Test project',
  conductor: 'Herbert von Karajan',
  year: new AcademicYear(2023),
  participants: [{
    id: 1,
    familyName: 'Pavarotti',
    givenName: 'Luciano',
    instrument: {
      id: 1,
      name: 'tenor'
    },
    location: {
      id: 1,
      name: 'Test',
      distance: 1000
    }
  }]
};

const waitForLoad = async () => {
  await within(screen.getByRole('region', { name: 'General information' })).findByText(project.name);
  await within(screen.getByRole('region', { name: 'Rehearsals' })).findByText('5h');
};

describe('ProjectDetailsComponent', () => {
  let serviceProvider: ServiceProvider;
  let user: UserEvent;
  let navigate: jasmine.Spy;
  let router: Router;

  beforeEach(async () => {
    user = userEvent.setup();

    serviceProvider = new ServiceProvider();
    spyOn(serviceProvider.projectService, 'get').and.callFake(() => Promise.resolve(project));

    spyOn(serviceProvider.rehearsalService, 'getAll').and.callFake((): Promise<Rehearsal[]> => Promise.resolve([{
      id: 1,
      date: new Date(2023, 0, 2),
      project,
      duration: new Duration(1, 0, 0),
      attendees: [{
        musician: {} as Musician,
        duration: new Duration(1)
      }]
    }, {
      id: 2,
      date: new Date(2023, 0, 1),
      project,
      duration: new Duration(5),
      attendees: []
    }]));

    spyOn(serviceProvider.concertService, 'getAll').and.callFake((): Promise<Concert[]> => Promise.resolve([{
      id: 1,
      date: new Date(2023, 5, 15),
      project,
      location: 'Paris',
      attendees: []
    }, {
      id: 2,
      date: new Date(2023, 3, 1),
      project,
      location: 'Berlin',
      attendees: []
    }]));

    spyOn(serviceProvider.historicalValueService, 'getAllByType').and.callFake(() => Promise.resolve([]));

    await TestBed.configureTestingModule({
      providers: [MatDialog, {
        provide: ServiceProvider,
        useValue: serviceProvider
      }]
    })
      .compileComponents();

    await useParamMap({ id: 1 });
  });

  describe('common data', () => {
    beforeEach(async () => {
      await render(ProjectDetailsComponent);

      router = TestBed.inject(Router);
      navigate = spyOn(router, 'navigate');

      await waitForLoad();
    });

    it('shows the general information for the project', () => {
      const generalInfo = screen.getByRole('region', { name: 'General information' });

      expect(within(generalInfo).queryByText('Test project')).toBeTruthy();
      expect(within(generalInfo).queryByText('Herbert von Karajan')).toBeTruthy();
      expect(within(generalInfo).queryByText('2022/2023')).toBeTruthy();
    });

    it('shows a table with all the participants in the project', () => {
      const participantTable = screen.getByRole('table', { name: 'musicians' });
      const rows = within(participantTable).getAllByRole('row');
      expect(rows.length).toEqual(2);

      expect(within(rows[1]).getAllByRole('cell').map(element => element.innerText)).toEqual(['Luciano', 'Pavarotti', 'Tenor']);
    });

    it('navigates to the edit page when clicking on the edit button', async () => {
      await user.click(within(screen.getByRole('region', { name: 'header' })).getByRole('button', { name: 'edit' }));
      expect(navigate).toHaveBeenCalledTimes(1);
      expect(navigate).toHaveBeenCalledWith(['edit'], { relativeTo: jasmine.anything() });
    });

    it('opens a confirm dialog when clicking on the delete button', async () => {
      await user.click(within(screen.getByRole('region', { name: 'header' })).getByRole('button', { name: 'delete' }));

      await waitFor(() => {
        expect(screen.queryByRole('dialog')).toBeTruthy();
      });
    });

    describe('rehearsals', () => {
      it('shows a table with all the rehearsals in the project', () => {
        const rehearsalTable = screen.getByRole('table', { name: 'rehearsals' });
        const rows = within(rehearsalTable).getAllByRole('row');
        expect(rows.length).toEqual(3);

        const rowContents = within(rows[1]).getAllByRole('cell').map(cell => cell.innerText);
        expect(rowContents).toContain('2023-01-01');
        expect(rowContents).toContain('5h');
      });

      it('opens a rehearsal creation modal when clicking on the \'Create\' button', async () => {
        await user.click(within(screen.getByRole('region', { name: 'Rehearsals' })).getByRole('button', { name: 'create' }));

        const dialog = await screen.findByRole('dialog');
        expect(within(dialog).queryByText('Create rehearsal')).toBeTruthy();
      });

      it('opens a rehearsal edition modal when clicking on the edit button for the row for that rehearsal', async () => {
        const rehearsalTable = screen.getByRole('table', { name: 'rehearsals' });
        const rows = within(rehearsalTable).getAllByRole('row');
        const btn = within(rows[1]).getByRole('button', { name: 'edit' });

        await user.click(btn);

        const dialog = await screen.findByRole('dialog');
        expect(within(dialog).queryByText('Edit rehearsal')).toBeTruthy();
      });

      it('disables the edit button if the rehearsal attendance has already been recorded', () => {
        const rehearsalTable = screen.getByRole('table', { name: 'rehearsals' });
        const rows = within(rehearsalTable).getAllByRole('row');
        const btn = within(rows[2]).getByRole('button', { name: 'edit' });

        expect(btn.getAttribute('disabled')).not.toEqual(null);
      });

      it('opens a delete dialog when clicking on the delete button', async () => {
        const rehearsalTable = screen.getByRole('table', { name: 'rehearsals' });
        const rows = within(rehearsalTable).getAllByRole('row');
        const btn = within(rows[1]).getByRole('button', { name: 'delete' });

        await user.click(btn);

        await waitFor(() => {
          expect(screen.queryByRole('dialog')).toBeTruthy();
        });
      });

      it('opens an attendance dialog when clicking on the attendance button', async () => {
        await user.click(within(screen.getByRole('region', { name: 'Rehearsals' })).getAllByRole('button', { name: 'record attendance' })[0]);

        await waitFor(() => {
          expect(screen.queryByRole('dialog')).toBeTruthy();
        });
      });
    });

    describe('concerts', () => {
      it('shows a table with all the concerts in the project', () => {
        const concertTable = screen.getByRole('table', { name: 'concerts' });
        const rows = within(concertTable).getAllByRole('row');
        expect(rows.length).toEqual(3);

        const rowContents = within(rows[1]).getAllByRole('cell').map(cell => cell.innerText);
        expect(rowContents).toContain('2023-04-01');
        expect(rowContents).toContain('Berlin');
      });

      it('opens a concert creation modal when clicking on the \'Create\' button', async () => {
        await user.click(within(screen.getByRole('region', { name: 'Concerts' })).getByRole('button', { name: 'create' }));

        const dialog = await screen.findByRole('dialog');
        expect(within(dialog).queryByText('Create concert')).toBeTruthy();
      });

      it('opens a concert edition modal when clicking on the edit button for the row for that concert', async () => {
        const concertTable = screen.getByRole('table', { name: 'concerts' });
        const rows = within(concertTable).getAllByRole('row');
        const btn = within(rows[1]).getByRole('button', { name: 'edit' });

        await user.click(btn);

        const dialog = await screen.findByRole('dialog');
        expect(within(dialog).queryByText('Edit concert')).toBeTruthy();
      });

      it('opens a delete dialog when clicking on the delete button', async () => {
        const concertTable = screen.getByRole('table', { name: 'concerts' });
        const rows = within(concertTable).getAllByRole('row');
        const btn = within(rows[1]).getByRole('button', { name: 'delete' });

        await user.click(btn);

        await waitFor(() => {
          expect(screen.queryByRole('dialog')).toBeTruthy();
        });
      });

      it('opens an attendance dialog when clicking on the attendance button', async () => {
        await user.click(within(screen.getByRole('region', { name: 'Concerts' })).getAllByRole('button', { name: 'record attendance' })[0]);

        await waitFor(() => {
          expect(screen.queryByRole('dialog')).toBeTruthy();
        });
      });
    });
  });

  describe('payouts', () => {
    const renderWithData = async (data: Record<HistoricalType, Historical[]>) => {
      spyOn(serviceProvider.historicalValueService, 'getAll').and.callFake(() => Promise.resolve(data));

      await render(ProjectDetailsComponent);

      router = TestBed.inject(Router);
      navigate = spyOn(router, 'navigate');

      await waitForLoad();
    };

    [{
      title: 'disables the button when there are no rehearsal prices',
      historicals: {
        [HistoricalType.REHEARSAL]: [],
        [HistoricalType.MILEAGE]: [{
          since: project.year.startDate(),
          value: 1
          
        }],
        [HistoricalType.CONCERT]: [{
          since: project.year.startDate(),
          value: 1
          
        }]
      },
      expected: true
    }, {
      title: 'disables the button when there are no mileage prices',
      historicals: {
        [HistoricalType.REHEARSAL]: [{
          since: project.year.startDate(),
          value: 1
          
        }],
        [HistoricalType.MILEAGE]: [],
        [HistoricalType.CONCERT]: [{
          since: project.year.startDate(),
          value: 1
          
        }]
      },
      expected: true
    }, {
      title: 'disables the button when there are no concert prices',
      historicals: {
        [HistoricalType.REHEARSAL]: [{
          since: project.year.startDate(),
          value: 1
          
        }],
        [HistoricalType.MILEAGE]: [{
          since: project.year.startDate(),
          value: 1
          
        }],
        [HistoricalType.CONCERT]: []
      },
      expected: true
    }, {
      title: 'disables the button when there are no rehearsal prices before the first rehearsal',
      historicals: {
        [HistoricalType.REHEARSAL]: [{
          since: project.year.endDate(),
          value: 1
          
        }],
        [HistoricalType.MILEAGE]: [{
          since: project.year.startDate(),
          value: 1
          
        }],
        [HistoricalType.CONCERT]: [{
          since: project.year.startDate(),
          value: 1
          
        }]
      },
      expected: true
    }, {
      title: 'disables the button when there are no concert prices before the first concert',
      historicals: {
        [HistoricalType.REHEARSAL]: [{
          since: project.year.startDate(),
          value: 1
          
        }],
        [HistoricalType.MILEAGE]: [{
          since: project.year.startDate(),
          value: 1
          
        }],
        [HistoricalType.CONCERT]: [{
          since: project.year.endDate(),
          value: 1
          
        }]
      },
      expected: true
    }, {
      title: 'disables the button when there are no mileage prices before the first rehearsal',
      historicals: {
        [HistoricalType.REHEARSAL]: [{
          since: project.year.startDate(),
          value: 1
          
        }],
        [HistoricalType.MILEAGE]: [{
          since: project.year.endDate(),
          value: 1
          
        }],
        [HistoricalType.CONCERT]: [{
          since: project.year.startDate(),
          value: 1
          
        }]
      },
      expected: true
    }, {
      title: 'enables the button when all historicals are available',
      historicals: {
        [HistoricalType.REHEARSAL]: [{
          since: project.year.startDate(),
          value: 1
          
        }],
        [HistoricalType.MILEAGE]: [{
          since: project.year.startDate(),
          value: 1
          
        }],
        [HistoricalType.CONCERT]: [{
          since: project.year.startDate(),
          value: 1
          
        }]
      },
      expected: false
    }].forEach(({ title, historicals, expected }) => {
      it(title, async () => {
        await renderWithData(historicals);

        if (expected) {
          expect(screen.getByRole('button', { name: 'pay out' }).getAttribute('disabled')).toEqual('true');
        } else {
          expect(screen.getByRole('button', { name: 'pay out' }).getAttribute('disabled')).not.toEqual('true');
        }
      });
    });

    it('navigates to the payout page when clicking on the payout button', async () => {
      await renderWithData({
        [HistoricalType.REHEARSAL]: [{
          since: project.year.startDate(),
          value: 1
          
        }],
        [HistoricalType.MILEAGE]: [{
          since: project.year.startDate(),
          value: 1
          
        }],
        [HistoricalType.CONCERT]: [{
          since: project.year.startDate(),
          value: 1
          
        }]
      });
      await user.click(within(screen.getByRole('region', { name: 'header' })).getByRole('button', { name: 'pay out' }));

      expect(navigate).toHaveBeenCalledTimes(1);
      expect(navigate).toHaveBeenCalledWith(['payout'], { relativeTo: jasmine.anything() });
    });
  });
});
