import { TitleCasePipe } from '@angular/common';
import { Component, Inject } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MAT_DIALOG_DATA, MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { EditDialog } from 'src/app/components/generic/edit-page-base/edit-dialog.component';
import { ButtonLoadingDirective } from 'src/app/directives/button-loading.directive';
import { SameYearValidatorDirective } from 'src/app/directives/same-year-validator.directive';
import { ConcertService } from 'src/app/services/concert.service';
import { dateToString, stringToDate } from 'src/app/services/formatters';
import { ServiceProvider } from 'src/app/services/serviceProvider';
import { AcademicYear } from 'src/app/utils/academic-year/academic-year';
import { Concert, New, Project } from 'src/types';

interface DialogData {
  concert?: Concert;
  project: Project;
}

type NonFormFields = Pick<Concert, 'project' | 'attendees'>;

interface FormModel extends Omit<Concert, 'date' | 'project' | 'attendees'> {
  date: string;
}

const toFormModel = (concert: Concert): FormModel => {
  return {
    id: concert.id,
    date: dateToString(concert.date),
    location: concert.location
  };
};

const fromFormModel = (model: FormModel | New<FormModel>, constantFields: NonFormFields): Concert | New<Concert> => {
  const { date, location, ...other } = model;
  return {
    ...constantFields,
    ...other,
    date: stringToDate(date),
    location
  };
};

@Component({
  selector: 'app-concert-edition',
  templateUrl: './concert-edition.component.html',
  styleUrls: ['./concert-edition.component.scss'],
  standalone: true,
  imports: [FormsModule, TitleCasePipe, MatFormFieldModule, SameYearValidatorDirective, ButtonLoadingDirective, MatButtonModule, MatInputModule, MatDialogModule]
})
export class ConcertEditionComponent extends EditDialog<FormModel> {
  private constantFields: NonFormFields;

  private concertService: ConcertService;

  constructor(dialogRef: MatDialogRef<ConcertEditionComponent>, @Inject(MAT_DIALOG_DATA) data: DialogData, serviceProvider: ServiceProvider) {
    super(dialogRef, data.concert && toFormModel(data.concert));

    this.concertService = serviceProvider.concertService;

    if (data.concert && data.project.id !== data.concert.project.id) {
      throw new Error('Concert and project do not match');
    }

    this.constantFields = {
      project: data.project,
      attendees: data.concert?.attendees ?? []
    };
  }

  override getDefaultModel(): New<FormModel> {
    return {
      date: '',
      location: ''
    };
  }
  
  override async create(model: New<FormModel>): Promise<FormModel> {
    return toFormModel(await this.concertService.create(fromFormModel(model, this.constantFields))) as FormModel;
  }

  override async update(model: FormModel): Promise<FormModel> {
    return toFormModel(await this.concertService.update(fromFormModel(model, this.constantFields) as Concert)) as FormModel;
  }

  getAcademicYearForProject() {
    return this.constantFields.project.year ?? new AcademicYear(new Date().getFullYear());
  }

  override close(newConcert?: FormModel): void {
    super.close(newConcert && (fromFormModel(newConcert, this.constantFields) as Concert));
  }
}
