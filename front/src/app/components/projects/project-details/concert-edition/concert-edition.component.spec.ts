import { TestBed } from '@angular/core/testing';

import { ConcertEditionComponent } from './concert-edition.component';
import { render, screen, waitFor, waitForElementToBeRemoved } from '@testing-library/angular';
import userEvent from '@testing-library/user-event';
import { UserEvent } from '@testing-library/user-event/dist/types/setup/setup';
import { ServiceProvider } from 'src/app/services/serviceProvider';
import { Concert, New, Project } from 'src/types';
import { AcademicYear } from 'src/app/utils/academic-year/academic-year';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

const project: Project = {
  id: 1,
  name: 'Test project',
  year: new AcademicYear(2023),
  conductor: '',
  participants: []
};

describe('ConcertEditionComponent', () => {
  const serviceProvider = new ServiceProvider();

  let user: UserEvent;
  let closeSpy: jasmine.Spy;

  beforeEach(async () => {
    user = userEvent.setup();
    closeSpy = jasmine.createSpy('close');

    spyOn(serviceProvider.concertService, 'create').and.callFake((concert: New<Concert>): Promise<Concert> => {
      return new Promise(resolve => setTimeout(() => resolve({
        id: 1,
        ...concert
      }), 10));
    });

    spyOn(serviceProvider.concertService, 'update').and.callFake((concert: Concert): Promise<Concert> => {
      return new Promise(resolve => setTimeout(() => resolve(concert), 10));
    });

    await TestBed.configureTestingModule({
      providers: [{
        provide: MAT_DIALOG_DATA,
        useValue: {
          concert: {
            id: 1,
            date: new Date('2023-02-03'),
            location: 'Paris',
            attendees: [],
            project
          },
          project
        }
      }, {
        provide: MatDialogRef,
        useValue: {
          close: closeSpy
        }
      }, {
        provide: ServiceProvider,
        useValue: serviceProvider
      }]
    })
      .compileComponents();
  });

  describe('Common', () => {
    beforeEach(async () => {
      await render(ConcertEditionComponent);
    });

    it('navigates back when the Cancel button is clicked', () => {
      screen.getByRole('button', { name: 'Cancel' }).click();

      expect(closeSpy).toHaveBeenCalledTimes(1);
    });

    it('renders the Submit button as disabled if the date is not filled', async () => {
      await user.clear(screen.getByLabelText('Date'));

      expect(screen.getByRole('button', { name: 'Submit' }).getAttribute('disabled')).toEqual('true');
    });

    it('renders the Submit button as disabled if the location is not filled', async () => {
      await user.clear(screen.getByLabelText('Location'));

      expect(screen.getByRole('button', { name: 'Submit' }).getAttribute('disabled')).toEqual('true');
    });
  });

  describe('Create mode', () => {
    beforeEach(async () => {
      await TestBed.overrideProvider(MAT_DIALOG_DATA, {
        useValue: {
          concert: undefined,
          project
        }
      });
      await render(ConcertEditionComponent);
    });

    it('should create a create header', async () => {
      expect(screen.getAllByRole('heading').map(h => h.innerText)).toContain('Create concert');
    });

    it('calls create when submitting the form', async () => {
      await user.clear(screen.getByLabelText('Date'));
      await user.type(screen.getByLabelText('Date'), '2023-01-01');
      await user.clear(screen.getByLabelText('Location'));
      await user.type(screen.getByLabelText('Location'), 'Dublin');

      screen.getByRole('button', { name: 'Submit' }).click();

      const progressBar = await screen.findByRole('progressbar');
      await waitForElementToBeRemoved(progressBar);

      await waitFor(() => {
        expect(serviceProvider.concertService.create).toHaveBeenCalledTimes(1);
        expect(closeSpy).toHaveBeenCalledTimes(1);
      });
    });
  });

  describe('Edit mode', () => {
    beforeEach(async () => {
      await render(ConcertEditionComponent);
    });

    it('should create an edit header and have the correct date', async () => {
      expect(screen.getAllByRole('heading').map(h => h.innerText)).toContain('Edit concert');

      await waitFor(() => {
        expect(screen.getByDisplayValue('2023-02-03').id).toEqual(screen.getByLabelText('Date').id);
      });

    });

    it('calls update when submitting the form', async () => {
      await user.clear(screen.getByLabelText('Date'));
      await user.type(screen.getByLabelText('Date'), '2023-01-01');

      screen.getByRole('button', { name: 'Submit' }).click();

      const progressBar = await screen.findByRole('progressbar');
      await waitForElementToBeRemoved(progressBar);

      await waitFor(() => {
        expect(serviceProvider.concertService.update).toHaveBeenCalledTimes(1);
        expect(closeSpy).toHaveBeenCalledTimes(1);
      });
    });
  });
});
