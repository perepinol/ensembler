import { NgClass, NgIf } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatTableModule } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { PayoutService } from 'src/app/services/payout.service';
import { ServiceProvider } from 'src/app/services/serviceProvider';
import { formatDate, musicianComparer, parseNumberOrUndefined } from 'src/app/utils/formatting';
import { Musician, Project, ProjectPayout } from 'src/types';

// Already properly formatted floats because we don't need to use these for anything except displaying
interface MusicianTotal {
  musician: Musician;
  total: string;
}

interface Event {
  date: string;
  rehearsal?: string;
  concert?: string;
  mileage?: string;
}

type RowTotals = Omit<Event, 'date'>;

@Component({
  selector: 'app-payout',
  templateUrl: './payout.component.html',
  styleUrl: './payout.component.scss',
  standalone: true,
  imports: [MatTableModule, NgIf, NgClass, MatButtonModule]
})
export class PayoutComponent implements OnInit {
  columns = ['givenName', 'familyName', 'totalPayout'];
  detailsColumns = ['date', 'rehearsal', 'mileage', 'concert'];

  private router: Router;
  private route: ActivatedRoute;

  private payoutService: PayoutService;

  private projectId: Project['id'];
  selectedMusicianId: number;

  payout: ProjectPayout;

  total: string;
  totals: MusicianTotal[];

  constructor(router: Router, route: ActivatedRoute, serviceProvider: ServiceProvider) {
    this.router = router;
    this.route = route;

    this.payoutService = serviceProvider.payoutService;

    const projectId = parseNumberOrUndefined(route.snapshot.paramMap.get('id'));
    if (!projectId) {
      throw new Error('Project ID is undefined in route');
    }
    this.projectId = projectId ?? -1;
    this.selectedMusicianId = -1;

    this.payout = {
      payouts: {},
      project: {
        name: ''
      } as Project,
      total: 0
    };
    this.total = '0';
    this.totals = [];
  }

  async ngOnInit(): Promise<void> {
    this.payout = await this.payoutService.getPayout(this.projectId);
    this.totals = this.calculateTotals();
    this.total = PayoutComponent.formatMoney(this.payout.total);
  }

  async export() {
    try {
      await this.payoutService.exportPayout(this.projectId);
    } catch (ignored) { // eslint-disable-line @typescript-eslint/no-unused-vars
      // Ignore error, this is temporary
    }
  }

  back() {
    this.router.navigate(['..'], { relativeTo: this.route });
  }

  shouldShowDetails() {
    return this.selectedMusicianId >= 0;
  }

  isSelected(row: MusicianTotal) {
    return row.musician.id === this.selectedMusicianId;
  }

  selectRow(row: MusicianTotal) {
    this.selectedMusicianId = row.musician.id;
  }

  selectedMusician(): string | undefined {
    const musician = this.totals.find(total => total.musician.id === this.selectedMusicianId)?.musician;
    if (!musician) {
      return;
    }

    return `${musician.givenName} ${musician.familyName}`;
  }

  eventsForSelectedRow(): Event[] {
    if (this.selectedMusicianId < 0) throw new Error('No row selected');

    const musicianPayouts = this.payout.payouts[this.selectedMusicianId];
    return [
      ...musicianPayouts.rehearsals.payouts.map(payout => ({
        date: payout.rehearsal.date,
        rehearsal: PayoutComponent.formatMoney(payout.total),
        mileage: PayoutComponent.formatMoney(musicianPayouts.mileages.payouts.find(p => p.rehearsal.id === payout.rehearsal.id)?.total ?? 0)
      })),
      ...musicianPayouts.concerts.payouts.map(payout => ({
        date: payout.concert.date,
        concert: PayoutComponent.formatMoney(payout.total)
      }))
    ].sort((a, b) => {
      return a.date.valueOf() - b.date.valueOf();
    }).map(event => ({
      ...event,
      date: formatDate(event.date)
    }));
  }

  totalsForSelectedRow(): RowTotals {
    if (this.selectedMusicianId < 0) throw new Error('No row selected');

    const musicianPayouts = this.payout.payouts[this.selectedMusicianId];
    return {
      rehearsal: PayoutComponent.formatMoney(musicianPayouts.rehearsals.total),
      concert: PayoutComponent.formatMoney(musicianPayouts.concerts.total),
      mileage: PayoutComponent.formatMoney(musicianPayouts.mileages.total)
    };
  }

  private calculateTotals(): MusicianTotal[] {
    return Object.entries(this.payout.payouts).map(([musicianId, payout]) => {
      const musician = this.payout.project.participants.find(p => p.id === Number.parseInt(musicianId));
      if (!musician) {
        throw new Error(`Musician ${musicianId} not found in project`);
      }

      return {
        musician: musician,
        total: PayoutComponent.formatMoney(payout.total)
      };
    }).sort((a, b) => musicianComparer(a.musician, b.musician));
  }

  private static formatMoney(amount: number) {
    return amount.toFixed(Math.round(amount) === amount ? 0 : 2);
  }
}
