import { TestBed } from '@angular/core/testing';

import { PayoutComponent } from './payout.component';
import { render, screen, within } from '@testing-library/angular';
import { useParamMap } from 'src/app/testing';
import { Concert, Musician, Project, ProjectPayout, Rehearsal } from 'src/types';
import { AcademicYear } from 'src/app/utils/academic-year/academic-year';
import { ServiceProvider } from 'src/app/services/serviceProvider';
import { Duration } from 'src/app/utils/duration/duration';
import userEvent from '@testing-library/user-event';
import { UserEvent } from '@testing-library/user-event/dist/types/setup/setup';
import { Router } from '@angular/router';

const musician1: Musician = {
  id: 1,
  givenName: 'M',
  familyName: 'One',
  instrument: {
    id: 0,
    name: ''
  }
} as Musician;

const musician2: Musician = {
  id: 2,
  givenName: 'M',
  familyName: 'Two',
  instrument: {
    id: 0,
    name: ''
  }
} as Musician;

const project: Project = {
  id: 1,
  name: 'The Project',
  conductor: 'A conductor',
  year: new AcademicYear(2023),
  participants: [musician1, musician2]
};

const rehearsal1: Rehearsal = {
  id: 1,
  date: new Date(2023, 2, 1),
  project,
  duration: new Duration(4),
  attendees: []
};

const rehearsal2: Rehearsal = {
  id: 1,
  date: new Date(2023, 4, 1),
  project,
  duration: new Duration(4),
  attendees: []
};

const concert1: Concert = {
  id: 1,
  date: new Date(2023, 2, 15),
  project,
  location: 'location',
  attendees: []
};

/*
const concert2: Concert = {
  id: 1,
  date: new Date(2023, 6, 21),
  project,
  location: 'location',
  attendees: []
};
*/

const payout: ProjectPayout = {
  total: 41,
  project,
  payouts: {
    [musician1.id]: {
      rehearsals: {
        total: 11.4499999,
        payouts: [{
          rehearsal: rehearsal1,
          total: 1
        }, {
          rehearsal: rehearsal2,
          total: 10.4499999
        }]
      },
      concerts: {
        total: 20,
        payouts: [{
          concert: concert1,
          total: 20
        }]
      },
      mileages: {
        total: 10,
        payouts: [{
          rehearsal: rehearsal1,
          total: 10
        }]
      },
      total: 41.4499999
    },
    [musician2.id]: {
      rehearsals: {
        total: 0,
        payouts: []
      },
      concerts: {
        total: 15,
        payouts: [{
          concert: concert1,
          total: 15
        }]
      },
      mileages: {
        total: 0,
        payouts: []
      },
      total: 15
    }
  }
};

describe('PayoutComponent', () => {
  let navigate: jasmine.Spy;
  let user: UserEvent;

  beforeEach(async () => {
    user = userEvent.setup();
    const serviceProvider = new ServiceProvider();

    spyOn(serviceProvider.payoutService, 'getPayout').and.callFake((): Promise<ProjectPayout> => Promise.resolve(payout));

    await TestBed.configureTestingModule({
      providers: [{
        provide: ServiceProvider,
        useValue: serviceProvider
      }]
    })
      .compileComponents();

    await useParamMap({
      id: 1
    });

    await render(PayoutComponent);

    navigate = spyOn(TestBed.inject(Router), 'navigate');

    await within(screen.getAllByRole('heading')[0]).findByText(project.name, { exact: false });
  });

  it('should show a title indicating for which project the payout is', () => {
    expect(screen.getAllByRole('heading').map(h => h.innerText)).toContain(`Payout for ${project.name} (${project.year.toString()})`);
  });

  it('should show a table with all the participants and their total payout', () => {
    const table = screen.getByRole('table', { name: 'payouts' });
    project.participants.forEach(participant => {
      const participantRow = within(table).getAllByRole('row').find(row => row.innerText.includes(participant.familyName))!;
      expect(within(participantRow).queryByText(`${Math.round(payout.payouts[participant.id].total * 100) / 100}€`)).toBeTruthy();
    });
  });

  it('should navigate back when clicking on the back button', async () => {
    await user.click(screen.getByRole('button', { name: 'back' }));
    expect(navigate).toHaveBeenCalledWith(['..'], { relativeTo: jasmine.anything() });
  });

  const hint = 'Click on a musician to show the payout details';
  
  it('shows an empty details section if no musician is selected', () => {
    const details = screen.getByRole('region', { name: 'details' });
    expect(within(details).queryByText(hint)).toBeTruthy();
  });

  it('shows a table under details when a musician is clicked', async () => {
    await user.click(within(screen.getByRole('table', { name: 'payouts' })).getAllByRole('row')[1]);
    const details = screen.getByRole('region', { name: 'details' });
    expect(within(details).queryByText(hint)).toBeFalsy();
    expect(within(details).queryByRole('table')).toBeTruthy();
    expect(within(details).queryByText(`${musician1.givenName} ${musician1.familyName}`));
  });
});
