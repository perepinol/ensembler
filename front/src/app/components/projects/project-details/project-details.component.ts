import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { ProjectService } from 'src/app/services/project.service';
import { ServiceProvider } from 'src/app/services/serviceProvider';
import { AcademicYear } from 'src/app/utils/academic-year/academic-year';
import { parseNumberOrUndefined, sortMusicians } from 'src/app/utils/formatting';
import { Concert, Historical, HistoricalType, Project, Rehearsal } from 'src/types';
import { ConfirmationDialogComponent } from '../../confirmation-dialog/confirmation-dialog.component';
import { RehearsalService } from 'src/app/services/rehearsal.service';
import { RehearsalEditionComponent } from './rehearsal-edition/rehearsal-edition.component';
import { dateToString } from 'src/app/services/formatters';
import { ConcertService } from 'src/app/services/concert.service';
import { ConcertEditionComponent } from './concert-edition/concert-edition.component';
import { RehearsalAttendanceComponent } from '../../overview/attendance/rehearsal-attendance/rehearsal-attendance.component';
import { ConcertAttendanceComponent } from '../../overview/attendance/concert-attendance/concert-attendance.component';
import { HistoricalValueService } from 'src/app/services/historical-value.service';
import { FieldDisplayComponent } from '../../field-display/field-display.component';
import { MatTableModule } from '@angular/material/table';
import { DatePipe, NgIf, TitleCasePipe } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { FlexModule } from '@angular/flex-layout';

@Component({
  selector: 'app-project-details',
  templateUrl: './project-details.component.html',
  styleUrls: ['./project-details.component.scss'],
  standalone: true,
  imports: [FieldDisplayComponent, MatTableModule, NgIf, DatePipe, MatButtonModule, MatIconModule, TitleCasePipe, RehearsalEditionComponent, ConcertEditionComponent, FlexModule]
})
export class ProjectDetailsComponent implements OnInit {
  participantTableColumns = ['givenName', 'familyName', 'instrument'];
  rehearsalTableColumns = ['date', 'duration', 'actions'];
  concertTableColumns = ['date', 'location', 'actions'];

  private router: Router;
  private route: ActivatedRoute;
  private dialog: MatDialog;

  private projectService: ProjectService;
  private rehearsalService: RehearsalService;
  private concertService: ConcertService;
  private historicalValueService: HistoricalValueService;

  private projectId: Project['id'];

  project: Project;
  _rehearsals: Rehearsal[];
  _concerts: Concert[];

  private _historicals: Record<HistoricalType, Historical[]>;

  get rehearsals() {
    return this._rehearsals;
  }

  private set rehearsals(rehearsals: Rehearsal[]) {
    this._rehearsals = rehearsals.sort((a, b) => a.date.valueOf() - b.date.valueOf());
  }

  get concerts() {
    return this._concerts;
  }

  private set concerts(concerts: Concert[]) {
    this._concerts = concerts.sort((a, b) => a.date.valueOf() - b.date.valueOf());
  }

  get participants() {
    return sortMusicians(this.project.participants);
  }

  constructor(router: Router, route: ActivatedRoute, serviceProvider: ServiceProvider, dialog: MatDialog) {
    this.router = router;
    this.route = route;
    this.dialog = dialog;

    this.projectService = serviceProvider.projectService;
    this.rehearsalService = serviceProvider.rehearsalService;
    this.concertService = serviceProvider.concertService;
    this.historicalValueService = serviceProvider.historicalValueService;
    
    const projectId = parseNumberOrUndefined(route.snapshot.paramMap.get('id'));
    if (!projectId) {
      throw new Error('Project ID is undefined in route');
    }
    this.projectId = projectId ?? -1;

    this.project = {
      id: -1,
      name: '',
      conductor: '',
      year: new AcademicYear(new Date().getFullYear()),
      participants: []
    };
    this._rehearsals = [];
    this._concerts = [];
    this._historicals = {
      [HistoricalType.REHEARSAL]: [],
      [HistoricalType.MILEAGE]: [],
      [HistoricalType.CONCERT]: []
    };
  }

  async ngOnInit(): Promise<void> {
    this.project = await this.projectService.get(this.projectId);
    const allRehearsals = await this.rehearsalService.getAll();
    this.rehearsals = allRehearsals.filter(rehearsal => rehearsal.project.id === this.project.id);
    const allConcerts = await this.concertService.getAll();
    this.concerts = allConcerts.filter(concert => concert.project.id === this.project.id);
    this._historicals = await this.historicalValueService.getAll();
  }

  payOut() {
    this.router.navigate(['payout'], { relativeTo: this.route });
  }

  edit() {
    this.router.navigate(['edit'], { relativeTo: this.route });
  }

  delete() {
    this.dialog.open(ConfirmationDialogComponent, { data: {
      title: 'Delete project',
      actionText: `delete project ${this.project.name} (${this.project.year})`,
      actionButtonText: 'Delete',
      onConfirm: async () => {
        await this.projectService.delete(this.project);
      }
    }}).afterClosed().subscribe(booleanOrError => {
      if (booleanOrError === undefined) {
        return;
      }
      
      if (typeof booleanOrError !== 'boolean') {
        console.error(booleanOrError);
      }
      
      if (booleanOrError !== true) {
        return;
      }

      this.router.navigate(['..'], { relativeTo: this.route });
    });
  }

  createRehearsal() {
    this.dialog.open(RehearsalEditionComponent, { data: {
      project: this.project
    }}).afterClosed().subscribe(result => {
      if (!result) {
        return;
      }

      this.rehearsals = [...this.rehearsals, result];
    });
  }

  editRehearsal(rehearsal: Rehearsal) {
    this.dialog.open(RehearsalEditionComponent, { data: {
      rehearsal: { ...rehearsal },
      project: this.project
    }}).afterClosed().subscribe(result => {
      if (!result) {
        return;
      }

      this.rehearsals = this.rehearsals.map(r => r.id === result.id ? result : r);
    });
  }

  deleteRehearsal(rehearsal: Rehearsal) {
    this.dialog.open(ConfirmationDialogComponent, { data: {
      title: 'Delete rehearsal',
      actionText: `delete rehearsal for ${rehearsal.project.name} on ${dateToString(rehearsal.date)}`,
      actionButtonText: 'Delete',
      onConfirm: async () => {
        await this.rehearsalService.delete(rehearsal);
      }
    }}).afterClosed().subscribe(booleanOrError => {
      if (booleanOrError === undefined) {
        return;
      }
      
      if (typeof booleanOrError !== 'boolean') {
        console.error(booleanOrError);
      }
      
      if (booleanOrError !== true) {
        return;
      }

      this.rehearsals = this.rehearsals.filter(r => r.id !== rehearsal.id);
    });
  }

  recordRehearsalAttendance(rehearsal: Rehearsal) {
    this.dialog.open(RehearsalAttendanceComponent, { data: {...rehearsal} }).afterClosed().subscribe(result => {
      if (!result) {
        return;
      }

      this.rehearsals = this.rehearsals.map(rehearsal => rehearsal.id === result.id ? result : rehearsal);
    });
  }

  recordConcertAttendance(concert: Concert) {
    this.dialog.open(ConcertAttendanceComponent, { data: {...concert} }).afterClosed().subscribe(result => {
      if (!result) {
        return;
      }

      this.rehearsals = this.rehearsals.map(rehearsal => rehearsal.id === result.id ? result : rehearsal);
    });
  }

  createConcert() {
    this.dialog.open(ConcertEditionComponent, { data: {
      project: this.project
    }}).afterClosed().subscribe(result => {
      if (!result) {
        return;
      }

      this.concerts = [...this.concerts, result];
    });
  }

  editConcert(concert: Concert) {
    this.dialog.open(ConcertEditionComponent, { data: {
      concert,
      project: this.project
    }}).afterClosed().subscribe(result => {
      if (!result) {
        return;
      }

      this.concerts = this.concerts.map(concert => concert.id === result.id ? result : concert);
    });
  }

  deleteConcert(concert: Concert) {
    this.dialog.open(ConfirmationDialogComponent, { data: {
      title: 'Delete concert',
      actionText: `delete concert for ${concert.project.name} on ${dateToString(concert.date)}`,
      actionButtonText: 'Delete',
      onConfirm: async () => {
        await this.concertService.delete(concert);
      }
    }}).afterClosed().subscribe(booleanOrError => {
      if (booleanOrError === undefined) {
        return;
      }
      
      if (typeof booleanOrError !== 'boolean') {
        console.error(booleanOrError);
      }
      
      if (booleanOrError !== true) {
        return;
      }

      this.concerts = this.concerts.filter(c => c.id !== concert.id);
    });
  }

  hasValidHistoricals(): boolean {
    const earliestRehearsal = this._rehearsals.sort((a, b) => a.date.valueOf() - b.date.valueOf())[0];
    const earliestConcert = this._concerts.sort((a, b) => a.date.valueOf() - b.date.valueOf())[0];

    const earliestHistoricals = Object.values(this._historicals)
      .map(historicalList => historicalList.sort((a, b) => a.since.valueOf() - b.since.valueOf())[0]);
    
    return earliestHistoricals.every(h => h)
      && (!earliestRehearsal || earliestHistoricals.every(h => h.since <= earliestRehearsal.date))
      && (!earliestConcert || earliestHistoricals.every(h => h.since <= earliestConcert.date));
  }
}
