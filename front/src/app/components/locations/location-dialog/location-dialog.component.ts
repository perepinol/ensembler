import { TitleCasePipe } from '@angular/common';
import { Component, Inject } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MAT_DIALOG_DATA, MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { ButtonLoadingDirective } from 'src/app/directives/button-loading.directive';
import { LocationService } from 'src/app/services/location.service';
import { ServiceProvider } from 'src/app/services/serviceProvider';
import { Location, New, isExistingModel } from 'src/types';

@Component({
  selector: 'app-location-dialog',
  templateUrl: './location-dialog.component.html',
  styleUrls: ['./location-dialog.component.scss'],
  standalone: true,
  imports: [FormsModule, TitleCasePipe, MatFormFieldModule, MatInputModule, ButtonLoadingDirective, MatButtonModule, MatDialogModule]
})
export class LocationDialogComponent {
  private dialogRef: MatDialogRef<LocationDialogComponent>;
  private locationService: LocationService;

  location: Location | New<Location>;

  loading: boolean;

  constructor(dialogRef: MatDialogRef<LocationDialogComponent>, serviceProvider: ServiceProvider, @Inject(MAT_DIALOG_DATA) location?: Location) {
    this.locationService = serviceProvider.locationService;
    this.dialogRef = dialogRef;

    this.location = location ?? {
      name: '',
      distance: 0
    };

    this.loading = false;
  }

  getDialogType(): 'create' | 'edit' {
    return isExistingModel(this.location) ? 'edit' : 'create';
  }

  close() {
    this.dialogRef.close();
  }

  async submit() {
    this.loading = true;

    try {
      let returnedLocation: Location;
      
      if (isExistingModel(this.location)) {
        returnedLocation = await this.locationService.update(this.location);
      } else {
        returnedLocation = await this.locationService.create(this.location);
      }

      this.dialogRef.close(returnedLocation);
    } catch (error) {
      console.error(error);
    }

    this.loading = false;
  }
}
