import { TestBed } from '@angular/core/testing';
import { render, screen, waitFor, waitForElementToBeRemoved } from '@testing-library/angular';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { UserEvent } from '@testing-library/user-event/dist/types/setup/setup';
import userEvent from '@testing-library/user-event';

import { LocationDialogComponent } from './location-dialog.component';
import { LocationService } from 'src/app/services/location.service';
import { ServiceProvider } from 'src/app/services/serviceProvider';
import { Location, New } from 'src/types';

describe('LocationDialogComponent', () => {
  let user: UserEvent;
  let close: jasmine.Spy;
  let locationService: LocationService;
  
  beforeEach(async () => {
    user = userEvent.setup();
    close = jasmine.createSpy('dialogClose');
    locationService = new LocationService();

    spyOn(locationService, 'create').and.callFake((location: New<Location>) => {
      return new Promise(resolve => setTimeout(() => resolve({ ...location, id: 1 }), 10));
    });
    spyOn(locationService, 'update').and.callFake((location: Location) => {
      return new Promise(resolve => setTimeout(() => resolve(location), 10));
    });

    await TestBed.configureTestingModule({
      providers: [{
        provide: MatDialogRef,
        useValue: {
          close
        }
      }, {
        provide: MAT_DIALOG_DATA,
        useValue: undefined
      }, {
        provide: ServiceProvider,
        useValue: {
          locationService
        }
      }]
    })
    .compileComponents();
  });

  describe('Common', () => {
    beforeEach(async () => {
      await render(LocationDialogComponent);
    });

    it('closes the modal when the Cancel button is clicked', () => {
      screen.getByRole('button', { name: 'Cancel' }).click();

      expect(close).toHaveBeenCalledTimes(1);
      expect(close).toHaveBeenCalledWith();
    });

    it('renders the Submit button as disabled if the form is not filled', async () => {
      await user.clear(screen.getByLabelText('Name'));

      expect(screen.getByRole('button', { name: 'Submit' }).getAttribute('disabled')).toEqual('true');
    });
  });

  describe('Create dialog', () => {
    beforeEach(async () => {
      await render(LocationDialogComponent);
    });

    it('should create a create dialog', async () => {
      expect(screen.getAllByRole('heading').map(h => h.innerText)).toContain('Create location');
    });

    it('calls MusicianService.create when submitting the form', async () => {
      await user.type(screen.getByLabelText('Name'), 'A');
      await user.clear(screen.getByLabelText('Distance'));
      await user.type(screen.getByLabelText('Distance'), '50');

      screen.getByRole('button', { name: 'Submit' }).click();

      const progressBar = await screen.findByRole('progressbar');
      await waitForElementToBeRemoved(progressBar);

      await waitFor(() => {
        expect(locationService.create).toHaveBeenCalledTimes(1);
        expect(close).toHaveBeenCalledTimes(1);
        expect(close).toHaveBeenCalledWith({
          id: 1,
          name: 'A',
          distance: 50
        });
      });
    });
  });

  describe('Edit dialog', () => {
    beforeEach(async () => {
      await TestBed.overrideProvider(MAT_DIALOG_DATA, {
        useValue: {
          id: 1,
          name: 'Paris',
          distance: 100
        }
      })
        .compileComponents();

      await render(LocationDialogComponent);
    });

    it('should create an edit dialog', async () => {
      expect(screen.getAllByRole('heading').map(h => h.innerText)).toContain('Edit location');

      expect(screen.getByDisplayValue('Paris').id).toEqual(screen.getByLabelText('Name').id);
      expect(screen.getByDisplayValue('100').id).toEqual(screen.getByLabelText('Distance').id);
    });

    it('calls MusicianService.update when submitting the form', async () => {
      await user.clear(screen.getByLabelText('Name'));
      await user.clear(screen.getByLabelText('Distance'));

      await user.type(screen.getByLabelText('Name'), 'A');
      await user.type(screen.getByLabelText('Distance'), '10');

      screen.getByRole('button', { name: 'Submit' }).click();

      const progressBar = await screen.findByRole('progressbar');
      await waitForElementToBeRemoved(progressBar);

      await waitFor(() => {
        expect(locationService.update).toHaveBeenCalledTimes(1);
        expect(close).toHaveBeenCalledTimes(1);
        expect(close).toHaveBeenCalledWith({
          id: 1,
          name: 'A',
          distance: 10
        });
      });
    });
  });
});
