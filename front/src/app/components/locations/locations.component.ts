import { Component, OnInit } from '@angular/core';
import { LocationService } from 'src/app/services/location.service';
import { ServiceProvider } from 'src/app/services/serviceProvider';
import { Location } from '../../../types';
import { MatDialog } from '@angular/material/dialog';
import { LocationDialogComponent } from './location-dialog/location-dialog.component';
import { ConfirmationDialogComponent } from '../confirmation-dialog/confirmation-dialog.component';
import { MatTableModule } from '@angular/material/table';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { FlexModule } from '@angular/flex-layout';

@Component({
  selector: 'app-locations',
  templateUrl: './locations.component.html',
  styleUrls: ['./locations.component.scss'],
  standalone: true,
  imports: [MatTableModule, MatIconModule, MatButtonModule, FlexModule]
})
export class LocationsComponent implements OnInit {
  columns = ['name', 'distance', 'actions'];

  private dialog: MatDialog;
  private locationService: LocationService;

  locations: Location[];

  constructor(dialog: MatDialog, serviceProvider: ServiceProvider) {
    this.dialog = dialog;
    this.locationService = serviceProvider.locationService;

    this.locations = [];
  }

  async ngOnInit(): Promise<void> {
    this.locations = await this.locationService.getAll();
  }

  toggleCreateDialog() {
    this.dialog.open(LocationDialogComponent).afterClosed().subscribe(result => {
      if (!result) {
        return;
      }

      this.locations = [...this.locations, result];
    });
  }

  toggleEditDialog(location: Location) {
    this.dialog.open(LocationDialogComponent, { data: { ...location } }).afterClosed().subscribe(result => {
      if (!result) {
        return;
      }

      this.locations = [...this.locations.filter(location => location.id !== result.id), result];
    });
  }

  toggleDeleteDialog(location: Location) {
    this.dialog.open(ConfirmationDialogComponent, {
      data: {
        title: 'Delete location',
        actionText: `delete location "${location.name}"`,
        actionButtonText: 'Delete',
        onConfirm: async () => {
          await this.locationService.delete(location);
        }
      }
    }).afterClosed().subscribe(booleanOrError => {
      if (booleanOrError === undefined) {
        return;
      }
      
      if (typeof booleanOrError !== 'boolean') {
        console.error(booleanOrError);
      }
      
      if (booleanOrError !== true) {
        return;
      }
      
      this.locations = this.locations.filter(l => l.id !== location.id);
    });
  }
}
