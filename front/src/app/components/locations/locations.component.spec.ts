import { TestBed } from '@angular/core/testing';
import { render, screen, waitFor, within } from '@testing-library/angular';

import { LocationsComponent } from './locations.component';
import { ServiceProvider } from 'src/app/services/serviceProvider';
import { MatDialog } from '@angular/material/dialog';
import { UserEvent } from '@testing-library/user-event/dist/types/setup/setup';
import userEvent from '@testing-library/user-event';

describe('LocationsComponent', () => {
  let user: UserEvent;
  beforeEach(async () => {
    user = userEvent.setup();
    const serviceProvider = new ServiceProvider();

    spyOn(serviceProvider.locationService, 'getAll').and.callFake(() => Promise.resolve([{
      id: 1,
      name: 'LocationX',
      distance: 15
    }]));

    await TestBed.configureTestingModule({
      providers: [MatDialog, {
        provide: ServiceProvider,
        useValue: serviceProvider
      }]
    })
    .compileComponents();

    await render(LocationsComponent);
  });

  it('renders a table with headers \'Name\' and \'Distance\'', async () => {
    const expected = ['LocationX', '15'];

    expect(screen.queryByRole('table')).toBeTruthy();
    expect(screen.getAllByRole('columnheader').map(cell => cell.innerText)).toEqual(jasmine.arrayContaining(['Name', 'Distance']));

    await waitFor(() => {
      const cells = within(screen.getAllByRole('row')[1]).getAllByRole('cell').map(cell => cell.innerText);
      expected.forEach((expectedValue, i) => expect(cells[i]).toEqual(expectedValue));
    });
  });

  it('opens a form when clicking on the create button', async () => {
    screen.getByRole('button', { name: 'Create' }).click();

    expect(screen.queryByRole('form')).toBeTruthy();
  });

  it('opens a form when clicking on the edit button', async () => {
    const editButton = await screen.findByRole('button', { name: 'edit' });
    editButton.click();

    await waitFor(() => {
      expect(screen.queryByRole('form')).toBeTruthy();
    });
  });

  it('opens a delete dialog when clicking on the delete button', async () => {
    const deleteButton = await screen.findByRole('button', { name: 'delete' });
    await user.click(deleteButton);

    await waitFor(() => {
      expect(screen.queryByRole('dialog')).toBeTruthy();
    });
  });
});
