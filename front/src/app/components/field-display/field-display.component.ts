import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-field-display',
  templateUrl: './field-display.component.html',
  styleUrls: ['./field-display.component.scss'],
  standalone: true
})
export class FieldDisplayComponent {
  @Input() headerId!: unknown;
  @Input() name!: unknown;
  @Input() value!: unknown;
}
