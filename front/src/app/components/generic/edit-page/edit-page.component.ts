import { NgIf } from '@angular/common';
import { Component, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { FlexModule } from '@angular/flex-layout';
import { NgForm } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { ButtonLoadingDirective } from 'src/app/directives/button-loading.directive';

@Component({
  selector: 'app-edit-page',
  templateUrl: './edit-page.component.html',
  styleUrls: ['./edit-page.component.scss'],
  standalone: true,
  imports: [NgIf, ButtonLoadingDirective, MatButtonModule, FlexModule]
})
export class EditPageComponent {
  @Input() modelForm!: NgForm;
  @Input() modelName!: string;
  @Input() modelRepresentation = '';
  @Input() formType?: 'create' | 'edit';
  @Input() loading = false;
  @Output() back = new EventEmitter<void>();

  @ViewChild('modelForm') form?: NgForm;

  getPageTitle(): string {
    let title: string;
    switch (this.formType) {
      case 'create':
        return `Create ${this.modelName}`;
      case 'edit':
        title = `Update ${this.modelName}`;
        break;
      default:
        title = this.modelName.slice(0, 1).toUpperCase() + this.modelName.slice(1);
    }
    return title + (this.modelRepresentation ? `: ${this.modelRepresentation}` : '');
  }

  close() {
    this.back.emit();
  }
}
