import { TestBed } from '@angular/core/testing';

import { EditPageComponent } from './edit-page.component';
import { render, screen } from '@testing-library/angular';
import { NgForm } from '@angular/forms';

describe('EditPageComponent', () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
    })
    .compileComponents();
  });

  [{
    modelName: 'Test model',
    formType: undefined,
    modelRepresentation: undefined,
    expected: 'Test model'
  }, {
    modelName: 'test',
    formType: undefined,
    modelRepresentation: '{whatever: \'structure\'}',
    expected: 'Test: {whatever: \'structure\'}'
  }, {
    modelForm: new NgForm([], []),
    modelName: 'Test',
    formType: 'create',
    modelRepresentation: 'asdasd',
    expected: 'Create Test'
  }, {
    modelForm: new NgForm([], []),
    modelName: 'asd',
    formType: 'edit',
    modelRepresentation: 'asdasd',
    expected: 'Update asd: asdasd'
  }].forEach(({ modelForm, modelName, formType, modelRepresentation, expected }) => {
    it(`shows header ${expected} for model ${modelName} represented with ${modelRepresentation} on a ${formType} form`, async () => {
      await render(EditPageComponent, {
        componentInputs: {
          modelForm,
          modelName,
          formType,
          modelRepresentation
        }
      });

      expect(screen.getByRole('heading').textContent).toEqual(expected);
    });
  });
});
