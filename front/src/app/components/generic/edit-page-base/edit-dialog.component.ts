import { Model } from 'src/types';
import { EditPageBase } from './edit-base.component';
import { MatDialogRef } from '@angular/material/dialog';

export abstract class EditDialog<T extends Model> extends EditPageBase<T> {
  private dialogRef: MatDialogRef<unknown>;

  constructor(dialogRef: MatDialogRef<unknown>, model?: T) {
    super({
      model
    });

    this.dialogRef = dialogRef;
  }

  override close(newModel?: unknown): void {
    if (newModel) {
      this.dialogRef.close(newModel);
    } else {
      this.dialogRef.close();
    }
  }
}
