import { Model, New, isExistingModel } from 'src/types';

interface Props<T extends Model> {
  modelId?: T['id'];
  model?: T;
}

export abstract class EditPageBase<T extends Model> {
  modelId?: T['id'];

  model: T | New<T>;
  loading: boolean;

  // Provide model if available. Otherwise provide modelId and use setModel afterwards.
  constructor({ modelId, model }: Props<T>) {
    this.modelId = model?.id ?? modelId;
    this.model = model ?? this.getDefaultModel(); // The default model's ID might not match modelId if the model is fetched later
    this.loading = false;
  }

  setModel(model: T) {
    if (this.modelId !== model.id) {
      throw new Error('Model ID and page ID do not match');
    }
    this.model = model;
  }

  abstract getDefaultModel(): New<T>;

  abstract create(model: New<T>): Promise<T>;

  abstract update(model: T): Promise<T>;

  abstract close(newModel?: T): void;

  getFormType(): 'create' | 'edit' {
    return this.modelId ? 'edit' : 'create';
  }

  async submit() {
    this.loading = true;

    let result: T;
    try {
      if (isExistingModel(this.model)) {
        result = await this.update(this.model);
      } else {
        result = await this.create(this.model);
      }

      this.close(result);
    } catch (error) {
      console.error(error);
    }

    this.loading = false;
  }
}
