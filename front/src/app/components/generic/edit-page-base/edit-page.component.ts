import { ActivatedRoute, Router } from '@angular/router';
import { parseNumberOrUndefined } from 'src/app/utils/formatting';
import { Model } from 'src/types';
import { EditPageBase } from './edit-base.component';

export abstract class EditPage<T extends Model> extends EditPageBase<T> {
  protected router: Router;
  protected route: ActivatedRoute;

  constructor(router: Router, route: ActivatedRoute) {
    super({
        modelId: parseNumberOrUndefined(route.snapshot.paramMap.get('id'))
    });
    this.router = router;
    this.route = route;
  }

  override close() {
    this.router.navigate(['..'], { relativeTo: this.route });
  }
}
