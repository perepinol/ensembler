import { TestBed } from '@angular/core/testing';
import { render, screen, within } from '@testing-library/angular';

import { OverviewComponent } from './overview.component';
import { ServiceProvider } from 'src/app/services/serviceProvider';
import { Concert, Musician, Project, Rehearsal } from 'src/types';
import { AcademicYear } from 'src/app/utils/academic-year/academic-year';
import { Duration } from 'src/app/utils/duration/duration';
import { MatDialog } from '@angular/material/dialog';

const musician: Musician = {
  id: 1,
  givenName: 'Test',
  familyName: 'Test',
  instrument: {
    id: 1,
    name: 'Test'
  },
  location: {
    id: 1,
    name: 'test',
    distance: 0
  }
};

const project1: Project = {
  id: 1,
  name: 'Project 1',
  conductor: 'Professor Smith',
  year: new AcademicYear(2023),
  participants: [musician]
};

describe('OverviewComponent', () => {
  beforeEach(async () => {
    jasmine.clock().mockDate(new Date('2023-01-05'));
    
    const serviceProvider = new ServiceProvider();
    spyOn(serviceProvider.rehearsalService, 'getAll').and.callFake((): Promise<Rehearsal[]> => Promise.resolve([{
      id: 1,
      date: new Date('2023-01-01'),
      project: project1,
      duration: Duration.fromSeconds(3600),
      attendees: []
    }, {
      id: 2,
      date: new Date('2022-12-25'),
      project: {
        id: 2,
        name: 'Project 2',
        conductor: 'Mister Jones',
        year: new AcademicYear(2023),
        participants: []
      },
      duration: Duration.fromSeconds(7200),
      attendees: []
    }, {
      id: 3,
      date: new Date('2023-01-10'),
      project: project1,
      duration: Duration.fromSeconds(3600),
      attendees: []
    }, {
      id: 4,
      date: new Date('2022-12-01'),
      project: project1,
      duration: Duration.fromSeconds(3600),
      attendees: [{
        musician,
        duration: Duration.fromSeconds(3600)
      }]
    }]));
    spyOn(serviceProvider.concertService, 'getAll').and.callFake((): Promise<Concert[]> => Promise.resolve([{
      id: 1,
      date: new Date('2022-12-27'),
      project: project1,
      location: 'Prague',
      attendees: []
    }, {
      id: 2,
      date: new Date('2022-12-28'),
      project: project1,
      location: 'Prague',
      attendees: [musician]
    }, {
      id: 3,
      date: new Date('2023-01-10'),
      project: project1,
      location: 'Prague',
      attendees: []
    }]));

    await TestBed.configureTestingModule({
      providers: [{
        provide: ServiceProvider,
        useValue: serviceProvider
      }, MatDialog]
    })
      .compileComponents();

    await render(OverviewComponent);

    await screen.findAllByRole('cell');
  });

  afterEach(() => {
    jasmine.clock().uninstall();
  });

  it('shows a table named \'pending events\'', async () => {
    const expected = [
      ['2022-12-25', 'Rehearsal', 'Project 2', '2h', '-'],
      ['2022-12-27', 'Concert', 'Project 1', '-', 'Prague'],
      ['2023-01-01', 'Rehearsal', 'Project 1', '1h', '-']
    ];

    expect(screen.queryByText('Pending events')).toBeTruthy();
    expect(screen.queryByRole('table', { name: 'pending events' })).toBeTruthy();

    const table = screen.getByRole('table', { name: 'pending events' });
    expect(within(table).getAllByRole('columnheader').map(header => header.innerText)).toEqual(['Date', 'Type', 'Project', 'Duration', 'Location', '']);

    const rows = within(table).getAllByRole('row').slice(1);
    const cellLists = rows.map(row => within(row).getAllByRole('cell').slice(0, -1).map(cell => cell.innerText));

    expect(cellLists).toEqual(expected);
  });

  it('opens a dialog when clicking on the attendance button for a rehearsal', async () => {
    const rehearsalRow = screen.getAllByRole('row').find(row => row.innerText.includes('Rehearsal'));
    if (!rehearsalRow) {
      throw new Error('No rehearsal row found');
    }
    const button = within(rehearsalRow).getByRole('button', { name: 'record attendance' });

    button.click();
    // Not working: await user.click(button);

    const dialog = await screen.findByRole('dialog');
    expect(dialog.innerText.toLowerCase()).toContain('attendance');
  });
});
