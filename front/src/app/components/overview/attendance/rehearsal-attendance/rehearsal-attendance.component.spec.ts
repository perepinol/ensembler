import { TestBed } from '@angular/core/testing';

import { render, screen, waitFor, within } from '@testing-library/angular';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Concert, Location, Musician, Project, Rehearsal } from 'src/types';
import { Duration } from 'src/app/utils/duration/duration';
import { AcademicYear } from 'src/app/utils/academic-year/academic-year';
import { formatDate } from 'src/app/utils/formatting';
import { UserEvent } from '@testing-library/user-event/dist/types/setup/setup';
import userEvent from '@testing-library/user-event';
import { ServiceProvider } from 'src/app/services/serviceProvider';
import { RehearsalAttendanceComponent } from './rehearsal-attendance.component';

const location: Location = {
  id: 1,
  name: 'Some place',
  distance: 0
};

const musicians: Musician[] = [{
  id: 1,
  givenName: 'First',
  familyName: 'Firstman',
  instrument: {
    id: 1,
    name: 'Violin'
  },
  location
}, {
  id: 2,
  givenName: 'Second',
  familyName: 'Secondman',
  instrument: {
    id: 2,
    name: 'Viola'
  },
  location
}];

const project: Project = {
  id: 1,
  name: 'Test project',
  year: new AcademicYear(2023),
  conductor: 'Mister Conductor',
  participants: musicians
};

const rehearsal: Rehearsal = {
  id: 1,
  date: new Date('2023-01-01'),
  duration: new Duration(2),
  project,
  attendees: [{
    musician: musicians[0],
    duration: new Duration(2)
  }]
};

describe('RehearsalAttendanceComponent', () => {
  let user: UserEvent;
  let close: jasmine.Spy;
  let serviceProvider: ServiceProvider;

  beforeEach(async () => {
    user = userEvent.setup();
    close = jasmine.createSpy('dialogClose');

    serviceProvider = new ServiceProvider();
    spyOn(serviceProvider.rehearsalService, 'update').and.callFake((rehearsal: Rehearsal) => Promise.resolve(rehearsal));
    spyOn(serviceProvider.concertService, 'update').and.callFake((concert: Concert) => Promise.resolve(concert));

    await TestBed.configureTestingModule({
      providers: [{
        provide: MatDialogRef,
        useValue: {
          close
        }
      }, {
        provide: MAT_DIALOG_DATA,
        useValue: rehearsal
      }, {
        provide: ServiceProvider,
        useValue: serviceProvider
      }]
    })
      .compileComponents();
  });

  it('shows a title', async () => {
    await render(RehearsalAttendanceComponent);

    expect(screen.getByRole('heading').innerText).toEqual(`Attendance: ${rehearsal.project.name} on ${formatDate(rehearsal.date)}`);
  });

  it('shows a participant table with all the participants in the project', async () => {
    await render(RehearsalAttendanceComponent);

    const table = screen.queryByRole('table', { name: 'musicians' });
    expect(table).toBeTruthy();

    const rows = within(table as HTMLElement).getAllByRole('row').slice(1);
    expect(rows.length).toEqual(rehearsal.project.participants.length);

    rows.forEach(row => {
      expect(within(row).queryByRole('checkbox')).toBeTruthy();
    });
  });

  it('shows participants as checked if they have already been marked as attendees', async () => {
    await render(RehearsalAttendanceComponent);

    const table = screen.getByRole('table', { name: 'musicians' });
    const rows = within(table).getAllByRole('row').slice(1);
    const checked = rows.filter(row => within(row).queryByRole('checkbox', { checked: true }));
    expect(checked.length).toEqual(rehearsal.attendees.length);
  });

  it('shows participants as partially checked if they have been marked as attendees, but not for the whole duration', async () => {
    await TestBed.overrideProvider(MAT_DIALOG_DATA, {
      useValue: {
        ...rehearsal,
        attendees: rehearsal.attendees.map(attendee => ({ ...attendee, duration: new Duration(0, 0, 1) }))
      }
    });
    await render(RehearsalAttendanceComponent);

    const table = screen.getByRole('table', { name: 'musicians' });
    const rows = within(table).getAllByRole('row').slice(1);
    const checked = rows.filter(row => within(row).getByRole('checkbox').getAttribute('aria-checked') === 'mixed');
    expect(checked.length).toEqual(rehearsal.attendees.length);
  });

  [{
    musicians: [],
    checked: false,
    indeterminate: false
  }, {
    musicians: musicians.slice(1),
    checked: false,
    indeterminate: true
  }, {
    musicians: musicians,
    checked: true,
    indeterminate: false
  }].forEach(({ musicians, checked, indeterminate }) => {
    it(`shows a checkbox in the header row with checked=${checked}, indeterminate=${indeterminate} when musicians ${musicians.map(m => m.id)} are checked`, async () => {
      await TestBed.overrideProvider(MAT_DIALOG_DATA, {
        useValue: {
          ...rehearsal,
          attendees: musicians.map(m => ({ musician: m, duration: rehearsal.duration }))
        }
      });
      await render(RehearsalAttendanceComponent);

      const table = screen.getByRole('table', { name: 'musicians' });
      const header = within(table).getAllByRole('row')[0];

      if (!indeterminate) {
        const checkbox = within(header).queryByRole('checkbox', { checked });
        expect(checkbox).toBeTruthy();
        return;
      }

      const checkbox = within(header).getByRole('checkbox');
      expect(checkbox.getAttribute('aria-checked')).toEqual('mixed');
    });
  });

  it('selects all musicians if the checkbox in the header row is selected and not all musicians are selected', async () => {
    await render(RehearsalAttendanceComponent);

    const table = screen.getByRole('table', { name: 'musicians' });
    const header = within(table).getAllByRole('row')[0];
    const checkbox = within(header).getByRole('checkbox');

    await user.click(checkbox);

    const table2 = screen.getByRole('table', { name: 'musicians' });
    const rows = within(table2).getAllByRole('row').slice(1);
    rows.forEach(row => {
      expect(within(row).queryByRole('checkbox', { checked: true })).toBeTruthy();
    });
  });

  it('deselects all musicians if the checkbox in the header row is selected and all musicians are selected', async () => {
    await TestBed.overrideProvider(MAT_DIALOG_DATA, {
      useValue: {
        ...rehearsal,
        attendees: musicians.map(m => ({ musician: m, duration: rehearsal.duration }))
      }
    });

    await render(RehearsalAttendanceComponent);

    const table = screen.getByRole('table', { name: 'musicians' });
    const header = within(table).getAllByRole('row')[0];
    const checkbox = within(header).getByRole('checkbox');

    await user.click(checkbox);

    const table2 = screen.getByRole('table', { name: 'musicians' });
    const rows = within(table2).getAllByRole('row').slice(1);
    rows.forEach(row => {
      expect(within(row).queryByRole('checkbox', { checked: false })).toBeTruthy();
    });
  });

  it('disables the submit button if there are no attendees', async () => {
    await TestBed.overrideProvider(MAT_DIALOG_DATA, {
      useValue: {
        ...rehearsal,
        attendees: []
      }
    });
    await render(RehearsalAttendanceComponent);

    within(screen.getByRole('table', { name: 'musicians' }))
      .getAllByRole('row')
      .slice(1)
      .map(r => within(r).queryByRole('checkbox', { checked: true }))
      .filter(checkbox => !!checkbox)
      .forEach(async checkbox => await user.click(checkbox as HTMLElement));

    await waitFor(() => {
      expect(screen.getByRole('button', { name: 'Submit' }).getAttribute('disabled')).toEqual('true');
    });
  });

  it('closes the modal when the Cancel button is clicked', async () => {
    await render(RehearsalAttendanceComponent);

    screen.getByRole('button', { name: 'Cancel' }).click();

    expect(close).toHaveBeenCalledTimes(1);
    expect(close).toHaveBeenCalledWith();
  });

  it('updates the rehearsal when clicking submit', async () => {
    await TestBed.overrideProvider(MAT_DIALOG_DATA, {
      useValue: {
        ...rehearsal,
        attendees: []
      }
    });
    await render(RehearsalAttendanceComponent);

    const table = screen.getByRole('table', { name: 'musicians' });
    const rows = within(table).getAllByRole('row').slice(1);
    const checkbox = within(rows[0]).getByRole('checkbox');
    await user.click(checkbox);
    screen.getByRole('button', { name: 'Submit' }).click();
    // Not working: await user.click(screen.getByRole('button', { name: 'Submit' }));

    await waitFor(() => {
      expect(serviceProvider.rehearsalService.update).toHaveBeenCalled();
    });

    expect(close).toHaveBeenCalled();
  });
});
