import { TitleCasePipe } from '@angular/common';
import { Component, Inject } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MAT_DIALOG_DATA, MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { MatTableModule } from '@angular/material/table';
import { DurationInputComponent } from 'src/app/components/inputs/duration-input/duration-input.component';
import { ButtonLoadingDirective } from 'src/app/directives/button-loading.directive';
import { RehearsalService } from 'src/app/services/rehearsal.service';
import { ServiceProvider } from 'src/app/services/serviceProvider';
import { Duration } from 'src/app/utils/duration/duration';
import { formatDate, sortMusicians } from 'src/app/utils/formatting';
import { Musician, Rehearsal } from 'src/types';

@Component({
  selector: 'app-rehearsal-attendance',
  templateUrl: './rehearsal-attendance.component.html',
  styleUrl: './rehearsal-attendance.component.scss',
  standalone: true,
  imports: [MatTableModule, MatCheckboxModule, ButtonLoadingDirective, FormsModule, DurationInputComponent, TitleCasePipe, MatButtonModule, MatDialogModule]
})
export class RehearsalAttendanceComponent {
  columns = ['selected', 'duration', 'givenName', 'familyName', 'instrument'];

  private dialogRef: MatDialogRef<RehearsalAttendanceComponent>;
  private rehearsalService: RehearsalService;

  event: Rehearsal;
  loading: boolean;

  get participants() {
    return sortMusicians(this.event.project.participants);
  }

  constructor(dialogRef: MatDialogRef<RehearsalAttendanceComponent>, @Inject(MAT_DIALOG_DATA) event: Rehearsal, serviceProvider: ServiceProvider) {
    this.dialogRef = dialogRef;
    this.rehearsalService = serviceProvider.rehearsalService;

    this.event = event;
    this.loading = false;
  }

  async submit() {
    this.loading = true;

    let result: Rehearsal;
    try {
      result = await this.rehearsalService.update(this.event);
    } catch (e) {
      console.error(e);
      return;
    }

    this.dialogRef.close(result);
    this.loading = false;
  }

  close() {
    this.dialogRef.close();
  }

  formatDate(date: Date) {
    return formatDate(date);
  }

  private defaultDuration = new Duration(0);
  getDurationForMusician(musician: Musician): Duration {
    return this.event.attendees.find(m => m.musician.id === musician.id)?.duration ?? this.defaultDuration;
  }

  updateDurationForMusician(musician: Musician, newDuration: Duration) {
    const currentDuration = this.getDurationForMusician(musician);
    if (newDuration.totalSeconds === 0) {
      if (currentDuration !== this.defaultDuration) {
        this.event.attendees = this.event.attendees.filter(attendee => attendee.musician.id !== musician.id);
      }
      return;
    }

    if (currentDuration === this.defaultDuration) {
      this.event.attendees = [...this.event.attendees, {
        musician,
        duration: newDuration
      }];
      return;
    }

    this.event.attendees = this.event.attendees.map(attendee => attendee.musician.id === musician.id ? { ...attendee, duration: newDuration } : attendee);
  }

  isMusicianSelected(musician: Musician) {
    return !!this.event.attendees.find(m => m.musician.id === musician.id);
  }

  isSomeMusicianSelected() {
    return this.event.attendees.length > 0;
  }

  areAllMusiciansSelected() {
    return this.event.attendees.length === this.participants.length;
  }

  toggleMusician(musician: Musician) {
    if (this.isMusicianSelected(musician)) {
      this.event.attendees = this.event.attendees.filter(m => m.musician.id !== musician.id);
    } else {
      this.event.attendees = [...this.event.attendees, {musician, duration: Duration.fromSeconds(this.event.duration.totalSeconds)}];
    }
  }

  toggleAllMusicians() {
    if (!this.areAllMusiciansSelected()) {
      this.event.attendees = this.participants.map(musician => ({
        musician,
        duration: this.event.duration
      }));
    } else {
      this.event.attendees = [];
    }
  }
}
