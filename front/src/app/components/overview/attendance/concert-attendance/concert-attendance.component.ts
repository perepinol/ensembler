import { TitleCasePipe } from '@angular/common';
import { Component, Inject } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MAT_DIALOG_DATA, MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { MatTableModule } from '@angular/material/table';
import { ButtonLoadingDirective } from 'src/app/directives/button-loading.directive';
import { ConcertService } from 'src/app/services/concert.service';
import { ServiceProvider } from 'src/app/services/serviceProvider';
import { formatDate, sortMusicians } from 'src/app/utils/formatting';
import { Concert, Musician } from 'src/types';

@Component({
  selector: 'app-concert-attendance',
  templateUrl: './concert-attendance.component.html',
  styleUrl: './concert-attendance.component.scss',
  standalone: true,
  imports: [MatDialogModule, MatTableModule, MatButtonModule, MatCheckboxModule, FormsModule, ButtonLoadingDirective, TitleCasePipe, MatDialogModule]
})
export class ConcertAttendanceComponent {
  columns = ['selected', 'givenName', 'familyName', 'instrument'];

  private dialogRef: MatDialogRef<ConcertAttendanceComponent>;
  private concertService: ConcertService;

  event: Concert;
  loading: boolean;

  get participants() {
    return sortMusicians(this.event.project.participants);
  }

  constructor(dialogRef: MatDialogRef<ConcertAttendanceComponent>, @Inject(MAT_DIALOG_DATA) event: Concert, serviceProvider: ServiceProvider) {
    this.dialogRef = dialogRef;
    this.concertService = serviceProvider.concertService;

    this.event = event;
    this.loading = false;
  }

  async submit() {
    this.loading = true;

    let result: Concert;
    try {
      result = await this.concertService.update(this.event);
    } catch (e) {
      console.error(e);
      return;
    }

    this.dialogRef.close(result);
    this.loading = false;
  }

  close() {
    this.dialogRef.close();
  }

  formatDate(date: Date) {
    return formatDate(date);
  }

  isMusicianSelected(musician: Musician) {
    return !!this.event.attendees.find(m => m.id === musician.id);
  }

  isSomeMusicianSelected() {
    return this.event.attendees.length > 0;
  }

  areAllMusiciansSelected() {
    return this.event.attendees.length === this.participants.length;
  }

  toggleMusician(musician: Musician) {
    if (this.isMusicianSelected(musician)) {
      this.event.attendees = this.event.attendees.filter(m => m.id !== musician.id);
    } else {
      this.event.attendees = [...this.event.attendees, musician];
    }
  }

  toggleAllMusicians() {
    if (!this.areAllMusiciansSelected()) {
      this.event.attendees = [...this.participants];
    } else {
      this.event.attendees = [];
    }
  }
}
