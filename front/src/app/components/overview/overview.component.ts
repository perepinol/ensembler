import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogModule } from '@angular/material/dialog';
import { ConcertService } from 'src/app/services/concert.service';
import { RehearsalService } from 'src/app/services/rehearsal.service';
import { ServiceProvider } from 'src/app/services/serviceProvider';
import { Concert, Project, Rehearsal } from 'src/types';
import { formatDate } from 'src/app/utils/formatting';
import { ConcertAttendanceComponent } from './attendance/concert-attendance/concert-attendance.component';
import { RehearsalAttendanceComponent } from './attendance/rehearsal-attendance/rehearsal-attendance.component';
import { MatTableModule } from '@angular/material/table';
import { TitleCasePipe } from '@angular/common';
import { MatIconModule } from '@angular/material/icon';
import { ButtonLoadingDirective } from 'src/app/directives/button-loading.directive';
import { MatButtonModule } from '@angular/material/button';
import { FlexModule } from '@angular/flex-layout';

enum EventType {
  REHEARSAL = 'rehearsal',
  CONCERT = 'concert'
}

interface Event {
  id: Rehearsal['id'] | Concert['id'];
  projectName: Project['name'];
  date: string;
  type: EventType
  duration: string;
  location: string;
}

const rehearsalsToEvents = (rehearsals: Rehearsal[]): Event[] => rehearsals.map(rehearsal => ({
  id: rehearsal.id,
  type: EventType.REHEARSAL,
  date: formatDate(rehearsal.date),
  projectName: rehearsal.project.name,
  duration: rehearsal.duration.toString(),
  location: '-'
}));

const concertsToEvents = (concerts: Concert[]): Event[] => concerts.map(concert => ({
  id: concert.id,
  type: EventType.CONCERT,
  date: formatDate(concert.date),
  projectName: concert.project.name,
  location: concert.location,
  duration: '-'
}));

@Component({
  selector: 'app-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.scss'],
  standalone: true,
  imports: [MatTableModule, MatIconModule, TitleCasePipe, ButtonLoadingDirective, MatButtonModule, MatDialogModule, RehearsalAttendanceComponent, ConcertAttendanceComponent, FlexModule]
})
export class OverviewComponent implements OnInit {
  pendingEventsTableColumns = ['date', 'type', 'project', 'duration', 'location', 'actions'];

  private dialog: MatDialog;

  private rehearsals: Rehearsal[];
  private concerts: Concert[];

  private rehearsalService: RehearsalService;
  private concertService: ConcertService;

  constructor(serviceProvider: ServiceProvider, dialog: MatDialog) {
    this.dialog = dialog;

    this.rehearsals = [];
    this.concerts = [];

    this.rehearsalService = serviceProvider.rehearsalService;
    this.concertService = serviceProvider.concertService;
  }

  async ngOnInit(): Promise<void> {
    this.rehearsals = await this.rehearsalService.getAll();
    this.concerts = await this.concertService.getAll();
  }

  getPendingEvents(): Event[] {
    return [
      ...rehearsalsToEvents(this.rehearsals.filter(rehearsal => rehearsal.date <= new Date() && !rehearsal.attendees.length)),
      ...concertsToEvents(this.concerts.filter(concert => concert.date <= new Date() && !concert.attendees.length))
    ].sort((a, b) => a.date.localeCompare(b.date));
  }

  recordAttendance(event: Event) {
    const recordable = this.getOriginalEvent(event);
    if (event.type === EventType.REHEARSAL) {
      this.dialog.open(RehearsalAttendanceComponent, { data: {...recordable} }).afterClosed().subscribe(result => {
        if (!result) {
          return;
        }
  
        this.rehearsals = this.rehearsals.map(rehearsal => rehearsal.id === result.id ? result : rehearsal);
      });
    }
    
    if (event.type === EventType.CONCERT) {
      this.dialog.open(ConcertAttendanceComponent, { data: {...recordable} }).afterClosed().subscribe(result => {
        if (!result) {
          return;
        }
  
        this.concerts = this.concerts.map(concert => concert.id === result.id ? result : concert);
      });
    }
  }

  private getOriginalEvent(event: Event): Rehearsal | Concert {
    const rehearsalOrConcert = (event.type === EventType.REHEARSAL && this.rehearsals.find(rehearsal => rehearsal.id === event.id)) 
      || (event.type === EventType.CONCERT && this.concerts.find(concert => concert.id === event.id));

    if (!rehearsalOrConcert) {
      throw new Error(`Event with id ${event.id} is neither rehearsal nor concert`);
    }

    return rehearsalOrConcert;
  }
}
