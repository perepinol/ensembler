import { TestBed } from '@angular/core/testing';
import { MatDialog } from '@angular/material/dialog';
import { render, screen, waitFor, within } from '@testing-library/angular';

import { MusiciansComponent } from './musicians.component';
import { ServiceProvider } from '../../services/serviceProvider';
import { Musician } from 'src/types';
import { UserEvent } from '@testing-library/user-event/dist/types/setup/setup';
import userEvent from '@testing-library/user-event';

const musicians: Musician[] = [{
  id: 1,
  givenName: 'Emily',
  familyName: 'Runeberg',
  instrument: {
    id: 2,
    name: 'two'
  },
  location: {
    id: 1,
    name: 'London',
    distance: 0
  }
}, {
  id: 2,
  givenName: 'John',
  familyName: 'Smith',
  instrument: {
    id: 1,
    name: 'one'
  },
  location: {
    id: 1,
    name: 'London',
    distance: 0
  }
}, {
  id: 3,
  givenName: 'Mark',
  familyName: 'Johnson',
  instrument: {
    id: 1,
    name: 'one'
  },
  location: {
    id: 1,
    name: 'London',
    distance: 0
  }
}, {
  id: 4,
  givenName: 'Adam',
  familyName: 'Smith',
  instrument: {
    id: 1,
    name: 'one'
  },
  location: {
    id: 1,
    name: 'London',
    distance: 0
  }
}];

describe('MusiciansComponent', () => {
  let user: UserEvent;

  beforeEach(async () => {
    user = userEvent.setup();

    const serviceProvider = new ServiceProvider();
    spyOn(serviceProvider.musicianService, 'getAll').and.callFake((): Promise<Musician[]> => {
      return Promise.resolve(musicians);
    });

    await TestBed.configureTestingModule({
      providers: [MatDialog, {
        provide: ServiceProvider,
        useValue: serviceProvider
      }]
    })
      .compileComponents();

    await render(MusiciansComponent);
  });

  it('renders a table with headers \'Name\', \'Family name\', \'Instrument\' and \'From\', sorted by instrument ID, then family name, then name', async () => {
    expect(screen.queryByRole('table')).toBeTruthy();
    expect(screen.getAllByRole('columnheader').map(cell => cell.innerText)).toEqual(jasmine.arrayContaining(['Name', 'Family name', 'Instrument', 'From']));

    await waitFor(() => {
      expect(within(screen.getAllByRole('row')[1]).getAllByRole('cell').map(cell => cell.innerText)).toEqual(jasmine.arrayContaining([
        'Mark',
        'Johnson',
        'One',
        'London'
      ]));
      expect(within(screen.getAllByRole('row')[2]).getAllByRole('cell').map(cell => cell.innerText)).toEqual(jasmine.arrayContaining([
        'Adam',
        'Smith',
        'One',
        'London'
      ]));
      expect(within(screen.getAllByRole('row')[3]).getAllByRole('cell').map(cell => cell.innerText)).toEqual(jasmine.arrayContaining([
        'John',
        'Smith',
        'One',
        'London'
      ]));
      expect(within(screen.getAllByRole('row')[4]).getAllByRole('cell').map(cell => cell.innerText)).toEqual(jasmine.arrayContaining([
        'Emily',
        'Runeberg',
        'Two',
        'London'
      ]));
    });
  });

  it('opens a form when clicking on the create button', async () => {
    screen.getByRole('button', { name: 'Create' }).click();

    expect(screen.queryByRole('form')).toBeTruthy();
  });

  it('opens a form when clicking on the edit button', async () => {
    const editButtons = await screen.findAllByRole('button', { name: 'edit' });
    editButtons[0].click();

    await waitFor(() => {
      expect(screen.queryByRole('form')).toBeTruthy();
    });
  });

  it('opens a delete dialog when clicking on the delete button', async () => {
    const deleteButtons = await screen.findAllByRole('button', { name: 'delete' });
    await user.click(deleteButtons[0]);

    await waitFor(() => {
      expect(screen.queryByRole('dialog')).toBeTruthy();
    });
  });
});