import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

import { Musician } from 'src/types';
import { MusicianDialogComponent } from './musician-dialog/musician-dialog.component';
import { MusicianService } from '../../services/musician.service';
import { ServiceProvider } from '../../services/serviceProvider';
import { ConfirmationDialogComponent } from '../confirmation-dialog/confirmation-dialog.component';
import { sortMusicians } from 'src/app/utils/formatting';
import { MatTableModule } from '@angular/material/table';
import { MatIconModule } from '@angular/material/icon';
import { TitleCasePipe } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { FlexModule } from '@angular/flex-layout';

@Component({
  selector: 'app-musicians',
  templateUrl: './musicians.component.html',
  standalone: true,
  imports: [MatTableModule, MatIconModule, TitleCasePipe, MatButtonModule, FlexModule]
})
export class MusiciansComponent implements OnInit {
  columns = ['givenName', 'familyName', 'instrument', 'from', 'actions'];
  musicians: Musician[];

  private dialog: MatDialog;
  private musicianService: MusicianService;

  constructor(dialog: MatDialog, serviceProvider: ServiceProvider) {
    this.musicians = [];
    this.musicianService = serviceProvider.musicianService;
    this.dialog = dialog;
  }
  
  async ngOnInit(): Promise<void> {
    this.musicians = sortMusicians(await this.musicianService.getAll());
  }

  toggleCreateDialog(): void {
    this.dialog.open(MusicianDialogComponent).afterClosed().subscribe(async result => {
      if (!result) {
        return;
      }

      this.musicians = [...this.musicians, result];
    });
  }

  toggleEditDialog(musician: Musician): void {
    this.dialog.open(MusicianDialogComponent, { data: { ...musician }}).afterClosed().subscribe(async result => {
      if (!result) {
        return;
      }

      this.musicians = this.musicians.map(musician => musician.id === result.id ? result : musician);
    });
  }

  toggleDeleteDialog(musician: Musician): void {
    this.dialog.open(ConfirmationDialogComponent, { 
      data: {
        title: 'Delete musician',
        actionText: `delete musician "${musician.givenName} ${musician.familyName}"`,
        actionButtonText: 'Delete',
        onConfirm: async () => {
          await this.musicianService.delete(musician);
        }
      }
    }).afterClosed().subscribe(async (booleanOrError) => {
      if (booleanOrError === undefined) {
        return;
      }
      
      if (typeof booleanOrError !== 'boolean') {
        console.error(booleanOrError);
      }
      
      if (booleanOrError !== true) {
        return;
      }
      
      this.musicians = this.musicians.filter(m => m.id !== musician.id);
    });
  }
}
