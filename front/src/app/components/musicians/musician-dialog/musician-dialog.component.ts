import { NgForOf, TitleCasePipe } from '@angular/common';
import { Component, Inject, OnInit } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MAT_DIALOG_DATA, MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { ButtonLoadingDirective } from 'src/app/directives/button-loading.directive';
import { InstrumentService } from 'src/app/services/instrument.service';
import { LocationService } from 'src/app/services/location.service';
import { MusicianService } from 'src/app/services/musician.service';
import { ServiceProvider } from 'src/app/services/serviceProvider';
import { Instrument, Location, Musician, New, isExistingModel } from 'src/types';

interface InternalMusician extends Omit<Musician, 'location' | 'instrument'> {
  locationId: Location['id'];
  instrumentId: Instrument['id'];
}

@Component({
  selector: 'app-musician-dialog',
  templateUrl: './musician-dialog.component.html',
  styleUrls: ['./musician-dialog.component.scss'],
  standalone: true,
  imports: [FormsModule, TitleCasePipe, MatFormFieldModule, MatInputModule, NgForOf, ButtonLoadingDirective, MatSelectModule, MatButtonModule, MatDialogModule]
})
export class MusicianDialogComponent implements OnInit {
  private ref: MatDialogRef<MusicianDialogComponent>;
  private musicianService: MusicianService;
  private instrumentService: InstrumentService;
  private locationService: LocationService;

  locations: Location[];
  instruments: Instrument[];

  musician: InternalMusician | New<InternalMusician>;
  loading: boolean;

  constructor(dialogRef: MatDialogRef<MusicianDialogComponent>, serviceProvider: ServiceProvider, @Inject(MAT_DIALOG_DATA) musician?: Musician) {
    this.ref = dialogRef;
    this.musicianService = serviceProvider.musicianService;
    this.instrumentService = serviceProvider.instrumentService;
    this.locationService = serviceProvider.locationService;

    this.locations = [];
    this.instruments = [];

    this.musician = musician ? this.toInternal(musician) : {
      givenName: '',
      familyName: '',
      instrumentId: -1,
      locationId: -1
    };
    this.loading = false;
  }

  async ngOnInit(): Promise<void> {
    this.locations = (await this.locationService.getAll()).sort((a, b) => a.name.toLowerCase().localeCompare(b.name.toLowerCase()));
    if (this.musician.locationId < 0) {
      this.musician.locationId = this.locations.at(0)?.id ?? -1;
    }

    this.instruments = (await this.instrumentService.getAll()).sort((a, b) => a.id - b.id);
  }

  getDialogType(): 'create' | 'edit' {
    return isExistingModel(this.musician) ? 'edit' : 'create';
  }

  close() {
    this.ref.close();
  }

  async submit() {
    this.loading = true;

    try {
      const actualMusician = this.getActualMusician();
      let returnedMusician: Musician;

      if (isExistingModel(actualMusician)) {
        returnedMusician = await this.musicianService.update(actualMusician);
      } else {
        returnedMusician = await this.musicianService.create(actualMusician);
      }

      this.ref.close(returnedMusician);
    } catch (error) {
      console.error(error);
    }

    this.loading = false;
  }

  private toInternal(musician: Musician): InternalMusician {
    const { instrument, location, ...other } = musician;
    return {
      ...other,
      instrumentId: instrument.id,
      locationId: location.id
    };
  }

  private getActualMusician(): Musician | New<Musician> {
    const { locationId, instrumentId, ...restOfMusician } = this.musician;

    const location = this.locations.find(location => location.id === locationId);
    if (!location) {
      throw new Error('No location available');
    }

    const instrument = this.instruments.find(instrument => instrument.id === instrumentId);
    if (!instrument) {
      throw new Error('No instrument available');
    }

    return {
      ...restOfMusician,
      instrument,
      location
    };
  }
}
