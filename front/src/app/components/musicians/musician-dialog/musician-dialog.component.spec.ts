import { render, screen, waitFor, waitForElementToBeRemoved, within } from '@testing-library/angular';
import userEvent from '@testing-library/user-event';
import { UserEvent } from '@testing-library/user-event/dist/types/setup/setup';
import { TestBed } from '@angular/core/testing';

import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

import { MusicianDialogComponent } from './musician-dialog.component';
import { ServiceProvider } from 'src/app/services/serviceProvider';
import { Instrument, Location, Musician, New } from 'src/types';

const locations: Location[] = [{
  id: 1,
  name: 'Paris',
  distance: 100
}, {
  id: 2,
  name: 'Moscow',
  distance: 50
}];

const instruments: Instrument[] = [{
  id: 1,
  name: 'Violin'
}, {
  id: 2,
  name: 'Viola'
}];

describe('MusicianDialogComponent', () => {
  const serviceProvider = new ServiceProvider();
  let user: UserEvent;
  let close: jasmine.Spy;

  beforeEach(async () => {
    user = userEvent.setup();
    close = jasmine.createSpy('dialogClose');

    spyOn(serviceProvider.musicianService, 'create').and.callFake((musician: New<Musician>) => {
      return new Promise(resolve => setTimeout(() => resolve({ ...musician, id: 1 }), 10));
    });
    spyOn(serviceProvider.musicianService, 'update').and.callFake((musician: Musician) => {
      return new Promise(resolve => setTimeout(() => resolve(musician), 10));
    });
    spyOn(serviceProvider.instrumentService, 'getAll').and.callFake(() => Promise.resolve([...instruments]));
    spyOn(serviceProvider.locationService, 'getAll').and.callFake(() => Promise.resolve([...locations]));

    await TestBed.configureTestingModule({
      providers: [{
        provide: MatDialogRef,
        useValue: {
          close
        }
      }, {
        provide: MAT_DIALOG_DATA,
        useValue: undefined
      }, {
        provide: ServiceProvider,
        useValue: serviceProvider
      }]
    })
      .compileComponents();
  });

  describe('Common', () => {
    beforeEach(async () => {
      await render(MusicianDialogComponent);
    });

    it('closes the modal when the Cancel button is clicked', () => {
      screen.getByRole('button', { name: 'Cancel' }).click();

      expect(close).toHaveBeenCalledTimes(1);
      expect(close).toHaveBeenCalledWith();
    });

    it('renders the Submit button as disabled if the form is not filled', async () => {
      await user.clear(screen.getByLabelText('Name'));

      expect(screen.getByRole('button', { name: 'Submit' }).getAttribute('disabled')).toEqual('true');
    });
  });

  describe('Create dialog', () => {
    beforeEach(async () => {
      await render(MusicianDialogComponent);
    });

    it('should create a create dialog', async () => {
      expect(screen.getAllByRole('heading').map(h => h.innerText)).toContain('Create musician');
    });

    it('calls MusicianService.create when submitting the form', async () => {
      await user.type(screen.getByLabelText('Name'), 'A');
      await user.type(screen.getByLabelText('Family name'), 'B');
      await user.click(screen.getByLabelText('Instrument'));
      await user.click(within(screen.getByRole('listbox')).getByText(instruments[0].name));
      await user.click(screen.getByLabelText('From'));
      await user.click(within(screen.getByRole('listbox')).getByText(locations[1].name));
      await user.type(screen.getByLabelText('Account number'), '1234');

      screen.getByRole('button', { name: 'Submit' }).click();

      const progressBar = await screen.findByRole('progressbar');
      await waitForElementToBeRemoved(progressBar);

      await waitFor(() => {
        expect(serviceProvider.musicianService.create).toHaveBeenCalledTimes(1);
        expect(close).toHaveBeenCalledTimes(1);
        expect(close).toHaveBeenCalledWith({
          id: 1,
          givenName: 'A',
          familyName: 'B',
          instrument: instruments[0],
          location: locations[1],
          accountNumber: '1234'
        });
      });
    });
  });

  describe('Edit dialog', () => {
    beforeEach(async () => {
      await TestBed.overrideProvider(MAT_DIALOG_DATA, {
        useValue: {
          id: 1,
          givenName: 'Blah',
          familyName: 'Bleh',
          instrument: instruments[1],
          location: locations[1],
          accountNumber: '1234'
        }
      })
        .compileComponents();

      await render(MusicianDialogComponent);
    });

    it('should create an edit dialog', async () => {
      expect(screen.getAllByRole('heading').map(h => h.innerText)).toContain('Edit musician');

      await within(screen.getByLabelText('From')).findByText(locations[1].name);

      expect(screen.getByDisplayValue('Blah').id).toEqual(screen.getByLabelText('Name').id);
      expect(screen.getByDisplayValue('Bleh').id).toEqual(screen.getByLabelText('Family name').id);
      expect(screen.getByLabelText('Instrument').innerText).toEqual(instruments[1].name);
      expect(screen.getByLabelText('From').innerText).toEqual(locations[1].name);
    });

    it('calls MusicianService.update when submitting the form', async () => {
      await user.clear(screen.getByLabelText('Name'));
      await user.clear(screen.getByLabelText('Family name'));
      await user.clear(screen.getByLabelText('Account number'));

      await user.type(screen.getByLabelText('Name'), 'A');
      await user.type(screen.getByLabelText('Family name'), 'B');
      await user.click(screen.getByLabelText('Instrument'));
      await user.click(within(screen.getByRole('listbox')).getByText(instruments[1].name));
      await user.click(screen.getByLabelText('From'));
      await user.click(within(screen.getByRole('listbox')).getByText(locations[0].name));
      await user.type(screen.getByLabelText('Account number'), '5678');

      screen.getByRole('button', { name: 'Submit' }).click();

      const progressBar = await screen.findByRole('progressbar');
      await waitForElementToBeRemoved(progressBar);

      await waitFor(() => {
        expect(serviceProvider.musicianService.update).toHaveBeenCalledTimes(1);
        expect(close).toHaveBeenCalledTimes(1);
        expect(close).toHaveBeenCalledWith({
          id: 1,
          givenName: 'A',
          familyName: 'B',
          instrument: instruments[1],
          location: locations[0],
          accountNumber: '5678'
        });
      });
    });
  });
});
