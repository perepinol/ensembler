import { Component, Inject } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MAT_DIALOG_DATA, MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { ButtonLoadingDirective } from 'src/app/directives/button-loading.directive';

export interface ConfirmationDialogData {
  title: string;
  actionText: string;
  actionButtonText: string;
  onConfirm: () => Promise<void>;
}

@Component({
  selector: 'app-confirmation-dialog',
  templateUrl: './confirmation-dialog.component.html',
  styleUrls: ['./confirmation-dialog.component.scss'],
  standalone: true,
  imports: [ButtonLoadingDirective, MatButtonModule, MatDialogModule]
})
export class ConfirmationDialogComponent {
  private dialogRef: MatDialogRef<ConfirmationDialogComponent>;

  title: ConfirmationDialogData['title'];
  actionText: ConfirmationDialogData['actionText'];
  actionButtonText: ConfirmationDialogData['actionButtonText'];
  private onConfirm: ConfirmationDialogData['onConfirm'];

  loading: boolean;

  constructor(dialogRef: MatDialogRef<ConfirmationDialogComponent>, @Inject(MAT_DIALOG_DATA) data: ConfirmationDialogData) {
    this.dialogRef = dialogRef;
    this.title = data.title;
    this.actionText = data.actionText;
    this.actionButtonText = data.actionButtonText;
    this.onConfirm = data.onConfirm;
    this.loading = false;
  }

  async confirm() {
    this.loading = true;
    
    try {
      await this.onConfirm();
      this.dialogRef.close(true);
    } catch (e) {
      this.dialogRef.close(e);
    }

    this.loading = false;
  }

  close() {
    this.dialogRef.close(false);
  }
}
