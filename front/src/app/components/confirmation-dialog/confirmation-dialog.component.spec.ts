import { TestBed } from '@angular/core/testing';

import { ConfirmationDialogComponent, ConfirmationDialogData } from './confirmation-dialog.component';
import { render, screen, waitFor, waitForElementToBeRemoved } from '@testing-library/angular';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

describe('ConfirmationDialogComponent', () => {
  let closeSpy: jasmine.Spy;
  let dialogData: ConfirmationDialogData;

  beforeEach(async () => {
    dialogData = {
      title: 'This is a title',
      actionText: 'take an action',
      actionButtonText: 'Go ahead!',
      onConfirm: jasmine.createSpy('dialogCallback').and.callFake(() => new Promise(resolve => setTimeout(resolve, 5)))
    };

    closeSpy = jasmine.createSpy('dialogClose');

    await TestBed.configureTestingModule({
      providers: [{
        provide: MatDialogRef,
        useValue: {
          close: closeSpy
        }
      },
      {
        provide: MAT_DIALOG_DATA,
        useValue: dialogData
      }]
    })
      .compileComponents();
  });

  describe('successful callback', () => {
    beforeEach(async () => {
      await render(ConfirmationDialogComponent);
    });

    it('should render the texts in the correct places', () => {
      expect(screen.getByRole('heading').innerText).toEqual(dialogData.title);
      expect(screen.queryByText(`Are you sure you want to ${dialogData.actionText}?`)).toBeTruthy();
      expect(screen.queryByRole('button', { name: dialogData.actionButtonText })).toBeTruthy();
    });

    it('should call callback when clicking on the confirm button', () => {
      screen.getByRole('button', { name: dialogData.actionButtonText }).click();

      expect(dialogData.onConfirm).toHaveBeenCalledTimes(1);
    });

    it('should close and return true when the confirm button has been clicked and the callback is successful', async () => {
      screen.getByRole('button', { name: dialogData.actionButtonText }).click();

      const progressBar = await screen.findByRole('progressbar');
      await waitForElementToBeRemoved(progressBar);

      await waitFor(() => {
        expect(closeSpy).toHaveBeenCalledTimes(1);
        expect(closeSpy).toHaveBeenCalledWith(true);
      });
    });

    it('should close and return false when the cancel button has been clicked', async () => {
      screen.getByRole('button', { name: 'Cancel' }).click();

      await waitFor(() => {
        expect(closeSpy).toHaveBeenCalledTimes(1);
        expect(closeSpy).toHaveBeenCalledWith(false);
      });
    });
  });

  it('should close and return error when the confirm button has been clicked but the callback is not successful', async () => {
    await TestBed.overrideProvider(MAT_DIALOG_DATA, {
      useValue: {
        ...dialogData,
        onConfirm: () => new Promise((ignored, reject) => setTimeout(() => reject(new Error('Test error')), 5))
      }
    });

    await render(ConfirmationDialogComponent);

    screen.getByRole('button', { name: dialogData.actionButtonText }).click();

    const progressBar = await screen.findByRole('progressbar');
    await waitForElementToBeRemoved(progressBar);

    await waitFor(() => {
      expect(closeSpy).toHaveBeenCalledTimes(1);
      expect(closeSpy).toHaveBeenCalledWith(new Error('Test error'));
    });
  });
});
