import createBundler from "@bahmutov/cypress-esbuild-preprocessor";
import createEsbuildPlugin from '@badeball/cypress-cucumber-preprocessor/esbuild';
import { addCucumberPreprocessorPlugin } from '@badeball/cypress-cucumber-preprocessor';
import { defineConfig } from "cypress";
import { rmdir, existsSync } from 'fs';

const setupNodeEvents = async (
  on: Cypress.PluginEvents, 
  config: Cypress.PluginConfigOptions
): Promise<Cypress.PluginConfigOptions> => {
  await addCucumberPreprocessorPlugin(on, config);

  on("file:preprocessor", createBundler({
    plugins: [createEsbuildPlugin(config)]
  }));

  on('task', {
    deleteFolder(folderName) {
      return new Promise((resolve, reject) => {
        if (!existsSync(folderName)) {
          resolve(null);
          return;
        }

        rmdir(folderName, { maxRetries: 10, recursive: true }, (err) => {
          if (err) {
            console.error(err)
            return reject(err)
          }
          resolve(null)
        })
      })
    },
  });

  return config;
};

export default defineConfig({
  e2e: {
    baseUrl: 'http://localhost:8081',
    setupNodeEvents,
    specPattern: 'cypress/e2e/**/*.{feature,ts}'
  }
});
