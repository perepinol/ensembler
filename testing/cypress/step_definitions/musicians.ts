import { Given } from '@badeball/cypress-cucumber-preprocessor';
import { api } from '../support/api';
import { apiRequest } from '../support/helpers';

Given('a musician with name {string}, family name {string}', (givenName: string, familyName: string) => {
    apiRequest(() => api.locationService.getAll()).then(locations => {
        if (!locations.length) {
            return apiRequest(() => api.locationService.create({
                name: 'Test',
                distance: 0
            })).then(l => [l]);
        }

        return cy.wrap(locations);
    }).then(locations => {
        return apiRequest(() => api.instrumentService.getAll()).then(instruments => {
            const instrument = instruments[0];
            if (!instrument) {
                throw new Error('No instruments');
            }

            return apiRequest(() => api.musicianService.create({
                givenName,
                familyName,
                instrument,
                location: locations[0]
            }));
        });
    });
});

Given('a musician with name {string}, family name {string} and instrument {string}', (givenName: string, familyName: string, instrumentName: string) => {
    apiRequest(() => api.locationService.getAll()).then(locations => {
        if (!locations.length) {
            return apiRequest(() => api.locationService.create({
                name: 'Test',
                distance: 0
            })).then(l => [l]);
        }

        return cy.wrap(locations);
    }).then(locations => {
        return apiRequest(() => api.instrumentService.getAll()).then(instruments => {
            const instrument = instruments.find(i => i.name.toLowerCase() === instrumentName.toLowerCase());
            if (!instrument) {
                throw new Error(`No instrument named ${instrumentName}`);
            }

            return apiRequest(() => api.musicianService.create({
                givenName,
                familyName,
                instrument,
                location: locations[0]
            }));
        });
    });
});

Given('a musician with name {string}, family name {string} and instrument {string} from {string}', (
    givenName: string,
    familyName: string,
    instrumentName: string,
    location: string
) => {
    apiRequest(() => api.locationService.getAll()).then(locations => {
        return apiRequest(() => api.instrumentService.getAll()).then(instruments => {
            const instrument = instruments.find(i => i.name.toLowerCase() === instrumentName.toLowerCase());
            if (!instrument) {
                throw new Error(`No instrument named ${instrumentName}`);
            }

            return apiRequest(() => api.musicianService.create({
                givenName,
                familyName,
                instrument,
                location: locations.find(l => l.name === location)!
            }));
        });
    });
});