import { DataTable, Given } from '@badeball/cypress-cucumber-preprocessor';
import { api } from '../support/api';
import { Musician } from 'src/types';
import { allDefined, apiRequest } from '../support/helpers';

Given('a concert on {string} for the project {string} in {string}', (date: string, projectName: string, location: string) => {
    apiRequest(() => api.projectService.getAll()).then(projects => {
        const project = projects.find(project => project.name === projectName);

        if (!project) {
            throw new Error(`Project ${projectName} not found in database`);
        }
        
        return apiRequest(() => api.concertService.create({
            date: new Date(date),
            project,
            location,
            attendees: []
        }));
    });
});

Given('a concert on {string} for the project {string} in {string} with attendees', (date: string, projectName: string, location: string, dataTable: DataTable) => {
    apiRequest(() => api.projectService.getAll()).then(projects => {
        const project = projects.find(project => project.name === projectName);
        
        if (!project) {
            throw new Error(`Project ${projectName} not found in database`);
        }
        
        return apiRequest(() => api.musicianService.getAll()).then(musicians => {
            const attendances = dataTable.hashes()
                .map(hash => {
                    const hashAsMusician = hash as Partial<Musician>;
                    const musician = musicians.find(musician => (Object.keys(hashAsMusician) as (keyof Musician)[]).every((key) => musician[key] === hash[key]));
                    return musician;
                });
            
            if (!allDefined(attendances)) {
                throw new Error('Some participants do not exist');
            }
            
            return apiRequest(() => api.concertService.create({
                date: new Date(date),
                project,
                location,
                attendees: attendances
            }));
        });
    });
});