import { DataTable, Given, When } from '@badeball/cypress-cucumber-preprocessor';
import { api } from '../support/api';
import { Duration } from 'src/app/utils/duration/duration';
import { Musician } from 'src/types';
import { allDefined, apiRequest } from '../support/helpers';

Given('a rehearsal on {string} for the project {string} lasting {int} hours', (date: string, projectName: string, durationHours: number) => {
    apiRequest(() => api.projectService.getAll()).then(projects => {
        const project = projects.find(project => project.name === projectName);

        if (project === undefined) {
            throw Error(`Project ${projectName} not found in database`);
        }
        
        return apiRequest(() => api.rehearsalService.create({
            date: new Date(date),
            project,
            duration: Duration.fromSeconds(durationHours * 3600),
            attendees: []
        }));
    });
});

Given('a rehearsal on {string} for the project {string} lasting {int} hours with attendees', (date: string, projectName: string, durationHours: number, dataTable: DataTable) => {
    apiRequest(() => api.projectService.getAll()).then(projects => {
        const project = projects.find(project => project.name === projectName);

        if (project === undefined) {
            throw Error(`Project ${projectName} not found in database`);
        }

        return apiRequest(() => api.musicianService.getAll()).then(musicians => {
            const attendances = dataTable.hashes()
                .map(hash => {
                    const duration = Duration.parse(hash['duration']);
                    delete hash['duration'];
                    const hashAsMusician = hash as Partial<Musician>;
                    const musician = musicians.find(musician => (Object.keys(hashAsMusician) as (keyof Musician)[]).every((key) => musician[key] === hash[key]));
                    if (!musician) return;

                    return {
                        musician,
                        duration
                    };
                });

            if (!allDefined(attendances)) {
                throw new Error('Some participants do not exist');
            }

            return apiRequest(() => api.rehearsalService.create({
                date: new Date(date),
                project,
                duration: Duration.fromSeconds(durationHours * 3600),
                attendees: attendances
            }));
        });
    });
});

When('I select project {string} for a duration of {int} hours', (project: string, duration: number) => {
    cy.findByRole('table', { name: 'projects' }).findByRole('combobox').selectInMatSelect(project);
    cy.findByRole('table', { name: 'projects' }).findAllByRole('textbox').first().type(duration.toString());
    cy.findByRole('table', { name: 'projects' }).findByRole('button', { name: 'add'}).click();
});

When ('I deselect project {string}', (project: string) => {
    cy.findByRole('table', { name: 'projects' }).findRow({ 'Name': project }).findByRole('button', { name: 'delete' }).click();
});