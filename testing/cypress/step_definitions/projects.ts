import { DataTable, Given, Then, When } from '@badeball/cypress-cucumber-preprocessor';
import { api } from '../support/api';
import { Musician } from 'src/types';
import { AcademicYear } from 'src/app/utils/academic-year/academic-year';
import { apiRequest } from '../support/helpers';

const allDefined = <T>(items: (T | undefined)[]): items is T[] => {
    return items.every(item => item !== undefined);;
};

Given('a project with name {string} for the year {string} conducted by {string} with the following participants', (name: string, year: string, conductor: string, dataTable: DataTable) => {
    apiRequest(() => api.musicianService.getAll()).then(musicians => {
        const participants = dataTable.hashes()
            .map(hash => {
                const hashAsMusician = hash as Partial<Musician>;
                return musicians.find(musician => (Object.keys(hashAsMusician) as (keyof Musician)[]).every((key) => musician[key] === hash[key]));
            });

        if (!allDefined(participants)) {
            throw new Error('Some participants do not exist');
        }

        return apiRequest(() => api.projectService.create({
            name,
            year: AcademicYear.parse(year),
            conductor,
            participants
        }));
    });
});

When('I select the following musicians', (dataTable: DataTable) => {
    dataTable.hashes().forEach(hash => {
        cy.findByRole('table', { name: 'musicians' }).findRow(hash).findByRole('checkbox').check();
    });
});

Then('I see the following participants', (dataTable: DataTable) => {
    dataTable.hashes().forEach(hash => {
        cy.findByRole('table', { name: 'musicians' }).findRow(hash).should('exist');
    });
});

Then('I see the following checked participants', (dataTable: DataTable) => {
    dataTable.hashes().forEach(hash => {
        cy.findByRole('table', { name: 'musicians' }).findRow(hash).findByRole('checkbox').should('be.checked');
    });
});