import path from 'path';
import { parse } from 'papaparse';
import { DataTable, Given, Then } from '@badeball/cypress-cucumber-preprocessor';
import { api } from '../support/api';
import { apiRequest } from '../support/helpers';

Given('a musician with name {string}, family name {string} from {string} with account number {string}', (
    givenName: string,
    familyName: string,
    location: string,
    accountNumber: string
) => {
    apiRequest(() => api.locationService.getAll()).then(locations => {
        return apiRequest(() => api.instrumentService.getAll()).then(instruments => {
            return apiRequest(() => api.musicianService.create({
                givenName,
                familyName,
                instrument: instruments[0],
                location: locations.find(l => l.name === location)!,
                accountNumber
            }));
        });
    });
});

Then('I see a grand total of {float}€', (total: number) => {
    cy.findByText(`Total: ${total.toFixed(2)}€`).should('exist');
});

Then('a file named {string} should be downloaded', (filename: string) => {
    cy.readFile(path.join(Cypress.config('downloadsFolder'), filename));
});

Then('a file named {string} should not be downloaded', (filename: string) => {
    cy.readFile(path.join(Cypress.config('downloadsFolder'), filename)).should('not.exist');
});

enum RowDefs {
    DATE = 'FECHA',
    DESCRIPTION = 'CONCEPTO',
    ACCOUNT = 'CUENTA',
    LEFT = 'DEBE',
    RIGHT = 'HABER',
    ENTRY = 'REGISTRO'
}

interface Payment {
    account: number;
    amount: number;
}

interface Entry {
    date: Date;
    description: string;
    lefts: Payment[];
    rights: Payment[];
}

const toJsDate = (slashDate: string): Date => {
    const [day, month, year] = slashDate.split('/').map(str => Number.parseInt(str, 10));
    return new Date(year < 100 ? 2000 + year : year, month, day);
};

const toEntries = (data: Record<string, string>[]): Entry[] => {
    const groupedByEntry = Object.values(data.reduce((accum, dataPoint) => ({
        ...accum,
        [dataPoint[RowDefs.ENTRY]]: [...(accum[dataPoint[RowDefs.ENTRY]] ?? []), dataPoint]
    }), {} as Record<string, Record<string, string>[]>));

    return groupedByEntry.map(paymentList => ({
        date: toJsDate(paymentList.map(p => p[RowDefs.DATE]).filter(date => date)[0]),
        description: paymentList.map(p => p[RowDefs.DESCRIPTION]).filter(desc => desc)[0],
        lefts: paymentList.map(p => ({
            account: Number.parseInt(p[RowDefs.ACCOUNT]),
            amount: p[RowDefs.LEFT] ? Number.parseInt(p[RowDefs.LEFT]) : 0
        })).filter(payment => payment.amount).sort((a, b) => a.account - b.account),
        rights: paymentList.map(p => ({
            account: Number.parseInt(p[RowDefs.ACCOUNT]),
            amount: p[RowDefs.RIGHT] ? Number.parseInt(p[RowDefs.RIGHT]) : 0
        })).filter(payment => payment.amount).sort((a, b) => a.account - b.account)
    }));
};

Then('the file {string} contains the following entries', (filename: string, dataTable: DataTable) => {
    cy.readFile(Cypress.config('downloadsFolder') + '/' + filename).then(entryList => {
        const { data, errors } = parse<Record<string, string>>(entryList, {
            header: true,
            delimiter: ';'
        });

        if (errors.length > 0) {
            console.error(errors);
            throw new Error('Error parsing CSV');
        }

        const actual = toEntries(data);

        return actual;
    }).should(entryList => {
        const expected = toEntries(dataTable.hashes());

        expect(entryList).to.deep.eq(expected);
    });
});