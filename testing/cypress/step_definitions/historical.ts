import { DataTable, Given, When } from '@badeball/cypress-cucumber-preprocessor';
import { api } from '../support/api';
import { HistoricalType } from 'src/types';
import { apiRequest } from '../support/helpers';

const createHistoricals = (type: HistoricalType, dataTable: DataTable) => {
    for (const hash of dataTable.hashes()) {
        apiRequest(() => api.historicalValueService.create(type, {
            since: new Date(hash['Start date']),
            value: Number.parseFloat(hash['Value'])
        }));
    }
};

Given('the following rehearsal prices', (dataTable: DataTable) => {
    createHistoricals(HistoricalType.REHEARSAL, dataTable);
});

Given('the following concert prices', (dataTable: DataTable) => {
    createHistoricals(HistoricalType.CONCERT, dataTable);
});

Given('the following mileage prices', (dataTable: DataTable) => {
    createHistoricals(HistoricalType.MILEAGE, dataTable);
});

When('I add a value of {float} starting on {string} in {string}', (value: number, dateString: string, type: string) => {
    cy.findByRole('table', { name: type }).findCell({ 'End': '' }, 'Start').findByRole('input').type(dateString);
    cy.findByRole('table', { name: type }).findCell({ 'End': '' }, 'Value').findByRole('spinbutton').clear();
    cy.findByRole('table', { name: type }).findCell({ 'End': '' }, 'Value').findByRole('spinbutton').type(value.toString());
    cy.findByRole('table', { name: type }).findByRole('button', { name: 'Add' }).click();
});