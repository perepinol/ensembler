import { Given } from '@badeball/cypress-cucumber-preprocessor';
import { api } from '../support/api';
import { apiRequest } from '../support/helpers';

Given('a location named {string} at a travelling distance of {int}', (name: string, distance: number) => {
    apiRequest(() => api.locationService.create({
        name,
        distance
    }));
});