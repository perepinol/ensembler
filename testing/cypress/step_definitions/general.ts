import { DataTable, Given, Then, When } from '@badeball/cypress-cucumber-preprocessor';

Given('I open the application at {string}', (path: string) => {
    cy.visit(path);
});

Given('today\'s date is {string}', (date: string) => {
    cy.clock(new Date(date), ['Date']);
    cy.request({
        method: 'PUT',
        url: '/api/date',
        qs: {
            date
        }
    });
});

When('I refresh the page', () => {
    cy.reload();
});

When('I click on the {string} button', (buttonName: string) => {
    cy.findByRole('button', { name: new RegExp(buttonName, 'i') }).click();
    cy.findByRole('progressbar').should('not.exist');
});

When('I click on the {string} button in the table {string} for the row with', (buttonName: string, tableName: string, dataTable: DataTable) => {
    if (dataTable.hashes().length !== 1) {
        throw new Error('Datatable should have one element');
    }
    cy.findByRole('table', { name: tableName }).findRow(dataTable.hashes()[0]).findByRole('button', { name: new RegExp(buttonName, 'i') }).click();
});

When('I click on the {string} button in the {string} section', (buttonName: string, sectionName: string) => {
    cy.findByRole('region', { name: new RegExp(sectionName, 'i') }).findByRole('button', { name: new RegExp(buttonName, 'i') }).click();
});

When('I click on the row in the table {string} with', (tableName: string, dataTable: DataTable) => {
    if (dataTable.hashes().length !== 1) {
        throw new Error('Datatable should have one element');
    }
    cy.findByRole('table', { name: tableName }).findRow(dataTable.hashes()[0]).findAllByRole('cell').first().click();
});

When('I write the following information', (dataTable: DataTable) => {
    dataTable.hashes().forEach(row => {
        Object.keys(row).forEach(key => {
            cy.findByRole('form').findByLabelText(new RegExp(`^${key}$`, 'i')).clear();
            cy.findByRole('form').findByLabelText(new RegExp(`^${key}$`, 'i')).type(row[key]);
        });
    });
});

When('I select {string} on the {string} field', (value: string, selectName: string) => {
    cy.findByRole('form').findByLabelText(new RegExp(`^${selectName}$`, 'i')).selectInMatSelect(value);
});

Then('the {string} button is disabled', (buttonName: string) => {
    cy.findByRole('button', { name: new RegExp(buttonName, 'i')}).should('be.disabled');
});

Then('I see items in the table {string} with the following information', (tableName: string, dataTable: DataTable) => {
    dataTable.hashes().forEach(hash => {
        console.log(cy.findByRole('table', { name: tableName }).findRow(hash));
        cy.findByRole('table', { name: tableName }).findRow(hash).should('exist');
    });
});

Then('I see only items in the table {string} with the following information', (tableName: string, dataTable: DataTable) => {
    dataTable.hashes().forEach(hash => {
        cy.findByRole('table', { name: tableName }).findRow(hash).should('exist');
    });
    cy.findByRole('table', { name: tableName }).findAllByRole('row').should('have.length', dataTable.hashes().length + 1);
});

Then('I see items in the table {string} with the following information, in order', (tableName: string, dataTable: DataTable) => {
    cy.findByRole('table', { name: tableName }).getTable().should(data => {
        dataTable.hashes().forEach((hash, i) => {
            Object.entries(hash).forEach(([key, value]) => {
                expect(data[i][key]).to.equal(value);
            });
        });
    });
});

Then('I see no items in the table {string}', (tableName: string) => {
    cy.findByRole('table', { name: tableName }).getTable().should('be.empty');
});

Then('I do not see the following items in the table {string}', (tableName: string, dataTable: DataTable) => {
    dataTable.hashes().forEach(hash => {
        cy.findByRole('table', { name: tableName }).findRow(hash).should('not.exist');
    });
});

Then('the items in the table {string} are sorted by {string}', (tableName: string, sortColumn: string) => {
    cy.findByRole('table', { name: tableName }).getTable()
        .then(tableData => tableData.map(row => row[sortColumn]))
        .should(columnData => {
            expect(columnData).to.deep.eq([...columnData].sort());
        });
});

Then('I see form fields with the following information', (dataTable: DataTable) => {
    dataTable.hashes().forEach(hash => {
        cy.findByRole('form').findByLabelText(new RegExp(`^${hash['Field name']}`, 'i')).should('have.value', hash['Content']);
    });
});

Then('I see fields with the following information', (dataTable: DataTable) => {
    dataTable.hashes().forEach(hash => {
        cy.findByLabelText(hash['Field name']).should('include.text', hash['Content']);
    });
});

Then('I get redirected to {string}', (path: string) => {
    cy.url().should('eq', Cypress.config('baseUrl') + path);
});

Then('I see the title {string}', (title: string) => {
    cy.findAllByRole('heading', { level: 1 }).should('contain.text', title);
});

Then('I see the text {string}', (text: string) => {
    cy.findByText(text, { exact: false }).should('exist');
});