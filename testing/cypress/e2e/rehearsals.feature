Feature: Rehearsals

  Background:
    Given a musician with name "Wolfgang Amadeus", family name "Mozart" and instrument "tenor"
    And a musician with name "Johann Sebastian", family name "Bach" and instrument "bass"
    And a project with name "Carmina Burana" for the year "2022/2023" conducted by "Carl Orff" with the following participants
      | familyName |
      | Bach |
      | Mozart |
    And a project with name "Music of the 80s" for the year "2022/2023" conducted by "Bjorn Ulvaeus" with the following participants
      | familyName |
      | Bach |
      | Mozart |
    And a rehearsal on "2022-12-24" for the project "Music of the 80s" lasting 2 hours
    And a rehearsal on "2022-12-24" for the project "Carmina Burana" lasting 5 hours
    And I open the application at "/project"
    And I click on the "details" button in the table "projects" for the row with
      | Name |
      | Carmina Burana |

  Scenario: Displaying existing rehearsals
    Then I see items in the table "rehearsals" with the following information
      | Date | Duration |
      | 2022-12-24 | 5h |

  Scenario: Creating a rehearsal
    When I click on the "Create" button in the "rehearsals" section
    And I write the following information
      | Date | Duration |
      | 2022-10-01 | 10h 11m |
    And I click on the "Submit" button
    Then I see items in the table "rehearsals" with the following information
      | Date | Duration |
      | 2022-10-01 | 10h 11m |

  Scenario: Editing a rehearsal
    When I click on the "edit" button in the table "rehearsals" for the row with
      | Date |
      | 2022-12-24 |
    And I write the following information
      | Date | Duration |
      | 2022-12-01 | 4 |
    And I click on the "submit" button
    Then I see items in the table "rehearsals" with the following information
      | Date | Duration |
      | 2022-12-01 | 4h |

  Scenario: Deleting a rehearsal
    When I click on the "delete" button in the table "rehearsals" for the row with
      | Date |
      | 2022-12-24 |
    And I click on the "delete" button
    Then I see no items in the table "rehearsals"

  Scenario: Recording attendance
    When I click on the "record attendance" button in the table "rehearsals" for the row with
      | Date |
      | 2022-12-24 |
    And I select the following musicians
      | Family name |
      | Bach |
    And I click on the "submit" button
    Then I see items in the table "rehearsals" with the following information
      | Date | Duration |
      | 2022-12-24 | 5h |