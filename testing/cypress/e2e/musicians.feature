Feature: Musicians page

  Background:
    Given a location named "Vienna" at a travelling distance of 10
    And a location named "Berlin" at a travelling distance of 200
    And a musician with name "Ludwig", family name "van Beethoven" and instrument "baritone" from "Vienna"
    And a musician with name "Wolfgang Amadeus", family name "Mozart" and instrument "tenor" from "Vienna"
    And a musician with name "Leopold", family name "Mozart" and instrument "tenor" from "Vienna"
    And a musician with name "Florence", family name "Foster" and instrument "soprano" from "Berlin"
    And I open the application at "/musician"

  Scenario: Opening the musicians page
    Then I see items in the table "musicians" with the following information, in order
      | Name | Family name | Instrument | From |
      | Florence | Foster | Soprano | Berlin |
      | Leopold | Mozart | Tenor | Vienna |
      | Wolfgang Amadeus | Mozart | Tenor | Vienna |
      | Ludwig | van Beethoven | Baritone | Vienna |

  Scenario: Creating a musician
    When I click on the "create" button
    And I write the following information
      | Name | Family name | Account number |
      | Johann Sebastian | Bach | 123456 |
    And I select "Bass" on the "Instrument" field
    And I select "Berlin" on the "From" field
    And I click on the "submit" button
    And I refresh the page
    Then I see items in the table "musicians" with the following information
      | Name | Family name | Instrument | From |
      | Johann Sebastian | Bach | Bass | Berlin |

  Scenario: Editing a musician
    When I click on the "edit" button in the table "musicians" for the row with
      | Name |
      | Wolfgang Amadeus |
    And I write the following information
      | Name | Family name | Account number |
      | Wolfgang Amadeus | Mozart | 123456 |
    And I select "Countertenor" on the "Instrument" field
    And I select "Berlin" on the "From" field
    And I click on the "submit" button
    And I refresh the page
    Then I see items in the table "musicians" with the following information
      | Name | Family name | Instrument | From |
      | Wolfgang Amadeus | Mozart | Countertenor | Berlin |
  
  Scenario: Deleting a musician
    When I click on the "delete" button in the table "musicians" for the row with
      | Name |
      | Wolfgang Amadeus |
    And I click on the "delete" button
    And I refresh the page
    Then I do not see the following items in the table "musicians"
      | Name | Family name | Instrument | From |
      | Wolfgang Amadeus | Mozart | Tenor | Vienna |
  
  Scenario: Cancelling modifications
    When I click on the "create" button
    And I write the following information
      | Name | Family name |
      | Johann Sebastian | Bach |
    And I click on the "cancel" button
    And I click on the "edit" button in the table "musicians" for the row with
      | Name |
      | Wolfgang Amadeus |
    And I select "Countertenor" on the "Instrument" field
    And I click on the "cancel" button
    And I click on the "delete" button in the table "musicians" for the row with
      | Name |
      | Wolfgang Amadeus |
    And I click on the "cancel" button
    Then I see items in the table "musicians" with the following information
      | Name | Family name | Instrument |
      | Wolfgang Amadeus | Mozart | Tenor |