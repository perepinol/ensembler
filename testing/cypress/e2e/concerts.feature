Feature: Concerts

  Background:
    Given a musician with name "Wolfgang Amadeus", family name "Mozart" and instrument "tenor"
    And a musician with name "Johann Sebastian", family name "Bach" and instrument "bass"
    And a project with name "Carmina Burana" for the year "2022/2023" conducted by "Carl Orff" with the following participants
      | familyName |
      | Bach |
      | Mozart |
    And a project with name "Music of the 80s" for the year "2022/2023" conducted by "Bjorn Ulvaeus" with the following participants
      | familyName |
      | Bach |
      | Mozart |
    And a concert on "2022-12-24" for the project "Music of the 80s" in "Vienna"
    And a concert on "2022-12-24" for the project "Carmina Burana" in "Paris"
    And I open the application at "/project"
    And I click on the "details" button in the table "projects" for the row with
      | Name |
      | Carmina Burana |

  Scenario: Displaying existing concerts
    Then I see items in the table "concerts" with the following information
      | Date | Location |
      | 2022-12-24 | Paris |

  Scenario: Creating a concert
    When I click on the "Create" button in the "concerts" section
    And I write the following information
      | Date | Location |
      | 2022-10-01 | Berlin |
    And I click on the "Submit" button
    Then I see items in the table "concerts" with the following information
      | Date | Location |
      | 2022-10-01 | Berlin |

  Scenario: Editing a concert
    When I click on the "edit" button in the table "concerts" for the row with
      | Date |
      | 2022-12-24 |
    And I write the following information
      | Date | Location |
      | 2022-12-01 | Barcelona |
    And I click on the "submit" button
    Then I see items in the table "concerts" with the following information
      | Date | Location |
      | 2022-12-01 | Barcelona |

  Scenario: Deleting a concert
    When I click on the "delete" button in the table "concerts" for the row with
      | Date |
      | 2022-12-24 |
    And I click on the "delete" button
    Then I see no items in the table "concerts"

  Scenario: Recording attendance
    When I click on the "record attendance" button in the table "concerts" for the row with
      | Date |
      | 2022-12-24 |
    And I select the following musicians
      | Family name |
      | Bach |
    And I click on the "submit" button
    Then I see items in the table "concerts" with the following information
      | Date | Location |
      | 2022-12-24 | Paris |