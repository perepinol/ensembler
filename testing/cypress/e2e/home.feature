Feature: Home page

  Background:
    Given a musician with name "Wolfgang Amadeus", family name "Mozart" and instrument "tenor"
    And a musician with name "Johann Sebastian", family name "Bach" and instrument "bass"
    And a project with name "Carmina Burana" for the year "2022/2023" conducted by "Carl Orff" with the following participants
      | familyName |
      | Bach |
      | Mozart |
    And a project with name "Music of the 80s" for the year "2022/2023" conducted by "Bjorn Ulvaeus" with the following participants
      | familyName |
      | Bach |
      | Mozart |
    And a rehearsal on "2022-12-24" for the project "Music of the 80s" lasting 2 hours
    And a rehearsal on "2022-12-24" for the project "Carmina Burana" lasting 5 hours with attendees
      | familyName | duration |
      | Bach | 1h |
    And a concert on "2022-12-25" for the project "Carmina Burana" in "Madrid"
    And a rehearsal on "2023-01-01" for the project "Carmina Burana" lasting 15 hours
    And a concert on "2023-02-01" for the project "Carmina Burana" in "Barcelona"
    And today's date is "2023-01-15"
    And I open the application at "/"
  
  Scenario: Not found pages
    When I open the application at "/non-existing-route"
    Then I get redirected to "/"

  Scenario: Upcoming events
    Then I see only items in the table "pending events" with the following information
      | Type | Project | Date | Duration | Location |
      | Rehearsal | Music of the 80s | 2022-12-24 | 2h | - |
      | Concert | Carmina Burana | 2022-12-25 | - | Madrid |
      | Rehearsal | Carmina Burana | 2023-01-01 | 15h | - |
    And the items in the table "pending events" are sorted by "Date"

  Scenario: Recording attendance
    When I click on the "record attendance" button in the table "pending events" for the row with
      | Type | Date | Project |
      | Rehearsal | 2023-01-01 | Carmina Burana |
    And I select the following musicians
      | Family name |
      | Bach |
    And I click on the "submit" button
    Then I do not see the following items in the table "pending events"
      | Type | Date | Project |
      | Rehearsal | 2023-01-01 | Carmina Burana |