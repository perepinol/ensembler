Feature: Projects page

  Background:
    Given a musician with name "Luciano", family name "Pavarotti" and instrument "tenor"
    And a musician with name "Josep", family name "Carreras" and instrument "tenor"
    And a project with name "O Sole Mio" for the year "2022/2023" conducted by "Herbert von Karajan" with the following participants
      | givenName |
      | Luciano |
    And I open the application at "/project"

  Scenario: Opening the projects page
    Then I see items in the table "projects" with the following information
      | Name | Year | Conductor |
      | O Sole Mio | 2022/2023 | Herbert von Karajan |

  Scenario: Creating a project
    When I click on the "create" button
    And I write the following information
      | Name | Year | Conductor |
      | Requiem in D minor | 2019/2020 | Wolfgang Amadeus Mozart |
    And I select the following musicians
      | Name |
      | Luciano |
    And I click on the "submit" button
    And I refresh the page
    Then I see items in the table "projects" with the following information
      | Name | Year | Conductor |
      | Requiem in D minor | 2019/2020 | Wolfgang Amadeus Mozart |

  Scenario: Duplicating a project
    When I click on the "duplicate" button in the table "projects" for the row with
      | Name |
      | O Sole Mio |
    Then I see the title "Create project"
    And I see form fields with the following information
      | Field name | Content |
      | Name | O Sole Mio | 
      | Year | 2022/2023 | 
      | Conductor | Herbert von Karajan |
    And I see the following checked participants
      | Name |
      | Luciano |

  Scenario: Editing a project
    When I click on the "details" button in the table "projects" for the row with
      | Name |
      | O Sole Mio |
    And I click on the "edit" button
    And I write the following information
      | Name | Year | Conductor |
      | Slavonic Dances | 2023/2024 | Jordi Nus |
    And I select the following musicians
      | Family name |
      | Carreras |
    And I click on the "submit" button
    And I refresh the page
    Then I see fields with the following information
      | Field name | Content |
      | Name | Slavonic Dances | 
      | Year | 2023/2024 |
      | Conductor | Jordi Nus |
    And I see the following participants
      | Family name |
      | Carreras |
      | Pavarotti |

  Scenario: Deleting a project
    When I click on the "delete" button in the table "projects" for the row with
      | Name |
      | O Sole Mio |
    And I click on the "delete" button
    And I refresh the page
    Then I see no items in the table "projects"

  Scenario: Cancelling modifications
    When I click on the "create" button
    And I write the following information
      | Name | Year | Conductor |
      | Test project | 2023/2024 | Test conductor |
    And I click on the "cancel" button
    And I click on the "delete" button in the table "projects" for the row with
      | Name |
      | O Sole Mio |
    And I click on the "cancel" button
    And I click on the "details" button in the table "projects" for the row with
      | Name |
      | O Sole Mio |
    And I click on the "edit" button
    And I write the following information
      | Conductor |
      | Carl Orff |
    And I click on the "cancel" button
    And I open the application at "/project"
    Then I see items in the table "projects" with the following information
      | Name | Year | Conductor |
      | O Sole Mio | 2022/2023 | Herbert von Karajan |

  Scenario: Invalid year
    When I open the application at "/project"
    And I click on the "create" button
    And I write the following information
      | Name | Year | Conductor |
      | Test project | 2022/2025 | Me |
    Then the "submit" button is disabled
    And I see the text "Invalid format"