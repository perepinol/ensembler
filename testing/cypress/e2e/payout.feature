Feature: Project payout

  Background:
    Given the following rehearsal prices
      | Start date | Value |
      | 2022-01-01 | 0.55 |
      | 2023-01-02 | 2.3 |
      | 2024-01-05 | 5 |
    And the following concert prices
      | Start date | Value |
      | 2021-01-01 | 2.505 |
      | 2022-02-15 | 3.1 |
      | 2023-06-30 | 4 |
    And the following mileage prices
      | Start date | Value |
      | 2021-01-01 | 0.3333333 |
      | 2022-02-01 | 0.5 |
      | 2023-03-01 | 0.12 |

    And a location named "Vienna" at a travelling distance of 10
    And a location named "Berlin" at a travelling distance of 200

    And a musician with name "Luciano", family name "Pavarotti" from "Vienna" with account number "601"
    And a musician with name "Josep", family name "Carreras" from "Berlin" with account number "602"
    And a musician with name "Ludwig", family name "van Beethoven" from "Vienna" with account number "603"
    
    And a project with name "O Sole Mio" for the year "2022/2023" conducted by "Herbert von Karajan" with the following participants
      | givenName |
      | Luciano |
      | Josep |
      | Ludwig |
    
    And a rehearsal on "2022-12-24" for the project "O Sole Mio" lasting 2 hours with attendees
      | givenName | duration |
      | Luciano | 2h |
    And a rehearsal on "2023-01-04" for the project "O Sole Mio" lasting 3 hours with attendees
      | givenName | duration |
      | Josep | 3h |
      | Luciano | 1h |
    And a rehearsal on "2023-02-01" for the project "O Sole Mio" lasting 4 hours with attendees
      | givenName | duration |
      | Josep | 4h |
      | Luciano | 4h |
    
    And a concert on "2023-01-15" for the project "O Sole Mio" in "Vienna" with attendees
      | givenName |
      | Josep |
    And a concert on "2023-02-10" for the project "O Sole Mio" in "Madrid" with attendees
      | givenName |
      | Luciano |
    
    And I open the application at "/project"
    And I click on the "details" button in the table "projects" for the row with
      | Name |
      | O Sole Mio |
    And I click on the "pay out" button

  Scenario: Displaying the payout overview
    Then I see a grand total of 249.90€
    And I see items in the table "payouts" with the following information
      | Name | Family name | Payout |
      | Luciano | Pavarotti | 30.70€ |
      | Josep | Carreras | 219.20€ |
      | Ludwig | van Beethoven | 0€ |

  Scenario: Displaying the details for a musician
    When I click on the row in the table "payouts" with
      | Name |
      | Josep |
    Then I see only items in the table "details" with the following information
      | Date | Rehearsal | Mileage | Concert |
      | 2022-12-24 | 0€ | 0€ | |
      | 2023-01-04 | 6.90€ | 100€ | |
      | 2023-01-15 | | | 3.10€ |
      | 2023-02-01 | 9.20€ | 100€ | |
      | 2023-02-10 | | | 0€ |
      | TOTALS | 16.10€ | 200€ | 3.10€ |
    And the items in the table "details" are sorted by "Date"

  Scenario: Exporting payout
    Given today's date is "2023-05-15"
    When I click on the "export" button
    Then a file named "o_sole_mio_payouts.csv" should be downloaded
    And the file "o_sole_mio_payouts.csv" contains the following entries
      | FECHA | CONCEPTO | CUENTA | DEBE | HABER | REGISTRO |
      | 15/05/2023 | O Sole Mio | | | | 1 |
      | | | 601 | 30.70 | | 1 |
      | | | 602 | 219.20 | | 1 |
      | | | 4100020 | | 249.90 | 1 |
