Feature: Locations page

  Background:
    Given a location named "Vienna" at a travelling distance of 0
    And a location named "Berlin" at a travelling distance of 150
    And I open the application at "/location"

  Scenario: Opening the locations page
    Then I see items in the table "locations" with the following information
      | Name | Distance |
      | Vienna | 0 |
      | Berlin | 150 |
  
  Scenario: Creating a location
    When I click on the "create" button
    And I write the following information
      | Name | Distance |
      | Paris | 20 |
    And I click on the "submit" button
    Then I see items in the table "locations" with the following information
      | Name | Distance |
      | Paris | 20 |
  
  Scenario: Editing a location
    When I click on the "edit" button in the table "locations" for the row with
      | Name |
      | Vienna |
    And I write the following information
      | Name | Distance |
      | Hamburg | 50 |
    And I click on the "submit" button
    And I refresh the page
    Then I see items in the table "locations" with the following information
      | Name | Distance |
      | Hamburg | 50 |
  
  Scenario: Deleting a location
    When I click on the "delete" button in the table "locations" for the row with
      | Name |
      | Vienna |
    And I click on the "delete" button
    And I refresh the page
    Then I see only items in the table "locations" with the following information
      | Name |
      | Berlin |

  Scenario: Cancelling modifications
    When I click on the "create" button
    And I write the following information
      | Name | Distance |
      | Paris | 200 |
    And I click on the "cancel" button
    And I click on the "edit" button in the table "locations" for the row with
      | Name |
      | Vienna |
    And I write the following information
      | Distance |
      | 300 |
    And I click on the "cancel" button
    And I click on the "delete" button in the table "locations" for the row with
      | Name |
      | Vienna |
    And I click on the "cancel" button
    Then I see items in the table "locations" with the following information
      | Name | Distance |
      | Vienna | 0 |
