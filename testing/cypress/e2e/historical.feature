Feature: Historical values

  Background:
    Given the following rehearsal prices
      | Start date | Value |
      | 2021-01-01 | 0.55 |
      | 2021-01-02 | 2.3 |
      | 2022-01-05 | 5 |
    And the following concert prices
      | Start date | Value |
      | 2021-01-01 | 2.505 |
      | 2021-02-15 | 3.1 |
      | 2021-06-30 | 4 |
    And the following mileage prices
      | Start date | Value |
      | 2021-01-01 | 0.3333333 |
      | 2021-02-01 | 0.5 |
      | 2021-03-01 | 0.12 |
    And I open the application at "/historical"

  Scenario: Displaying historical values
    Then I see items in the table "rehearsal prices" with the following information, in order
      | Start | End | Value (€/h) |
      | 2022-01-05 | - | 5 |
      | 2021-01-02 | 2022-01-05 | 2.3 |
      | 2021-01-01 | 2021-01-02 | 0.55 |
    And I see items in the table "concert prices" with the following information, in order
      | Start | End | Value (€) |
      | 2021-06-30 | - | 4 |
      | 2021-02-15 | 2021-06-30 | 3.1 |
      | 2021-01-01 | 2021-02-15 | 2.51 |
    And I see items in the table "mileage prices" with the following information, in order
      | Start | End | Value (€/km) |
      | 2021-03-01 | - | 0.12 |
      | 2021-02-01 | 2021-03-01 | 0.5 |
      | 2021-01-01 | 2021-02-01 | 0.33 |

  Scenario: Adding historical values
    When I add a value of 6 starting on "2022-01-10" in "rehearsal prices"
    And I add a value of 5 starting on "2021-08-15" in "concert prices"
    And I add a value of 0.1 starting on "2021-04-01" in "mileage prices"
    Then I see items in the table "rehearsal prices" with the following information, in order
      | Start | End | Value (€/h) |
      | 2022-01-10 | - | 6 |
      | 2022-01-05 | 2022-01-10 | 5 |
    And I see items in the table "concert prices" with the following information, in order
      | Start | End | Value (€) |
      | 2021-08-15 | - | 5 |
      | 2021-06-30 | 2021-08-15 | 4 |
      | 2021-02-15 | 2021-06-30 | 3.1 |
      | 2021-01-01 | 2021-02-15 | 2.51 |
    And I see items in the table "mileage prices" with the following information, in order
      | Start | End | Value (€/km) |
      | 2021-04-01 | - | 0.1 |
      | 2021-03-01 | 2021-04-01 | 0.12 |
      | 2021-02-01 | 2021-03-01 | 0.5 |
      | 2021-01-01 | 2021-02-01 | 0.33 |

  Scenario Outline: Deleting historical values
    When I click on the "delete" button in the "<section>" section
    Then I do not see the following items in the table "<section>"
      | Start |
      | <deleted start> |

    Examples:
      | section | deleted start |
      | rehearsal prices | 2022-01-05 |
      | concert prices | 2021-06-30 |
      | mileage prices | 2021-03-01 |

