const rowToObject = (headers: string[], row: Element) => {
    let cells = row.querySelectorAll('[role="cell"]');
    if (cells.length === 0) cells = row.querySelectorAll('[role="columnheader"]');
    const cellEntries = Array.from(cells)
        .map(cell => cell.children.length === 0 ? cell.textContent ?? '' : undefined)
        .reduce((accumulator, content, i) => content !== undefined ? { ...accumulator, [headers[i]]: content } : accumulator, {} as Record<string, string>);
    return cellEntries;
};

const findRow = (subject: JQuery<HTMLElement>, expectedRowData: Record<string, unknown>): Element | undefined => {
    const table = subject.get()[0];
    const headers = Array.from(table.querySelectorAll('[role="columnheader"]')).map(header => header.innerHTML);
    return Array.from(table.querySelectorAll('[role="row"]'))
        .slice(1)
        .find(row => {
            const rowData = rowToObject(headers, row);
            return Object.entries(expectedRowData).every(([key, value]) => rowData[key] === value);
        });
};

Cypress.Commands.addQuery('getTable', function () {
    return (subject: JQuery<HTMLElement>) => {
        const table = subject.get()[0];
        const headers = Array.from(table.querySelectorAll('[role="columnheader"]')).map(header => header.innerHTML);
        const rowCells = Array.from(table.querySelectorAll('[role="row"]'))
            .filter(row => row.querySelectorAll('[role="columnheader"]').length === 0)
            .map(row => rowToObject(headers, row));
        return rowCells;
    };
});

Cypress.Commands.addQuery('findRow', function (expectedRowData: Record<string, unknown>) {
    return (subject: JQuery<HTMLElement>) => findRow(subject, expectedRowData) ?? cy.$$('');
});

Cypress.Commands.addQuery('findCell', function (expectedRowData: Record<string, unknown>, columnRegex: string) {
    return (subject: JQuery<HTMLElement>) => {
        const table = subject.get()[0];
        const headerIndex = Array.from(table.querySelectorAll('[role="columnheader"]')).map(header => header.innerHTML).map(text => !!text.match(columnRegex)).indexOf(true);
        const cell = findRow(subject, expectedRowData)?.querySelectorAll('[role="cell"]')[headerIndex]
            ?? findRow(subject, expectedRowData)?.querySelectorAll('[role="columnheader"]')[headerIndex]
            ?? cy.$$('');
        return cell;
    };
});