export const allDefined = <T>(items: (T | undefined)[]): items is T[] => {
    return items.every(item => item !== undefined);;
};

export const apiRequest = <T>(request: () => Promise<T>): Cypress.Chainable<T> => {
    return cy.wrap(null).then(async () => {
        const result = await request();
        return result;
    });
};