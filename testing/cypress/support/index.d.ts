declare global {
    namespace Cypress {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        interface Chainable<Subject> {
            getTable(): Chainable<Record<string, string>[]>;
            findRow(data: Record<string, unknown>): Chainable<JQuery<HTMLElement>>;
            findCell(data: Record<string, unknown>, columnRegex: string): Chainable<JQuery<HTMLElement>>;
            selectInMatSelect(valueText: string): void;
        }
    }
}

export {};