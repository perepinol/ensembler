// ***********************************************************
// This example support/e2e.ts is processed and
// loaded automatically before your test files.
//
// This is a great place to put global configuration and
// behavior that modifies Cypress.
//
// You can change the location of this file or turn off
// automatically serving support files with the
// 'supportFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/configuration
// ***********************************************************

// Import commands.js using ES2015 syntax:
import './commands';

let error: ReturnType<typeof cy.spy>;
let warn: ReturnType<typeof cy.spy>;

Cypress.on('window:before:load', (window) => {
    error = cy.spy(window.console, 'error');
    warn = cy.spy(window.console, 'warn');

    window.addEventListener('unhandledrejection', (event) => {
        throw event.reason;
    });
});

beforeEach(() => {
    cy.request('DELETE', '/api/database');
    cy.task('deleteFolder', Cypress.config('downloadsFolder'));
});

afterEach(() => {
    cy.window().then(() => {
        if (!error || !warn) {
            console.warn('Window not defined!');
            return;
        }
        expect(error).to.have.callCount(0);
        expect(warn).to.have.callCount(0);
    });
});