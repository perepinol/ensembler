import { MusicianService } from '../../../front/src/app/services/musician.service';
import { InstrumentService } from '../../../front/src/app/services/instrument.service';
import { ProjectService } from '../../../front/src/app/services/project.service';
import { RehearsalService } from '../../../front/src/app/services/rehearsal.service';
import { ConcertService } from '../../../front/src/app/services/concert.service';
import { LocationService } from '../../../front/src/app/services/location.service';
import { HistoricalValueService } from '../../../front/src/app/services/historical-value.service';

export const api = {
    musicianService: new MusicianService(),
    instrumentService: new InstrumentService(),
    projectService: new ProjectService(),
    rehearsalService: new RehearsalService(),
    concertService: new ConcertService(),
    locationService: new LocationService(),
    historicalValueService: new HistoricalValueService()
};