FROM node:20 as build-frontend

WORKDIR /usr/src/app

COPY front/ ./
RUN npm ci

RUN npm run build

FROM maven:3.9.0-eclipse-temurin-19-alpine as build-backend

WORKDIR /usr/src/app

COPY src/ src/
COPY pom.xml ./
COPY --from=build-frontend /usr/src/app/dist/ src/main/resources/static/

RUN mvn clean package

FROM eclipse-temurin:19

WORKDIR /opt/app

COPY --from=build-backend /usr/src/app/target/*.jar app.jar
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "/opt/app/app.jar"]
