@Echo off
:healthTestLoop

cscript /b healthCheck.js %1%
if %ERRORLEVEL% NEQ 0 goto :healthTestLoop

start "" %1%
@Echo on