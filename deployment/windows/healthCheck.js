var location = WScript.arguments(0);

var request = new ActiveXObject("WinHttp.WinHttpRequest.5.1");
request.open("GET", location, false);

try {
    request.send();
} catch (e) {
    WScript.Echo("Error performing query: " + e.message);
    WScript.Quit(1);
}

WScript.Echo("Code " + request.Status);
WScript.Quit(request.Status === 200 ? 0 : 1);