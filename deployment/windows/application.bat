@Echo off
start "app" cmd /C "java -jar app.jar"
start "browserHealthCheck" cmd /C "startBrowserWhenHealthy.bat http://localhost:8080"
@Echo on